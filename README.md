Koura is a Facebook Messenger chat bot that makes it easy for businesses to send out online invoices to clients.

## Inspiration
I have a friend who works on a lot of side projects and he wanted an app that could easily log time spent working for a client 

## What it does
Koura is a Messenger bot that makes it simple for businesses to send invoices to clients. When a client receives an invoice in their inbox, they can go online to make a payment using a credit card. Once the payment goes through, Koura will let the business owner know that the invoice has been paid.

## How I built it
### Frontend
+ Facebook Messenger is the bot's user interface

### Backend
+ The backend is written in Python 3 and uses Django 1.11
+ I used [Zappa](https://github.com/Miserlou/Zappa) to package my Django application into a Lambda-compatible ZIP archive
+ All payment processing is done through [Stripe](https://stripe.com/)

### Infrastructure
I used [Terraform](https://www.terraform.io/) to deploy all the infrastructure for Koura on AWS. Terraform automatically provisions:
+ S3
+ Lambda
+ RDS (Postgresql)
+ ElastiCache (Redis)
+ VPC
+ CloudFront
+ Route 53
+ CloudWatch
+ IAM Roles
+ API Gateway
+ SQS

## Challenges I ran into
+ The library that I used to package my Django application, [Zappa](https://github.com/Miserlou/Zappa), does not have support for handling Lex events so I had to fork the repository and add my own solution.
+ Initially I used Lex's web interface to configure my bot, but that quickly became a bottleneck. I ended up writing a Python script that takes my bot's configuration in YAML format and automatically creates/updates the bot along with its intents and custom slot types.

## Accomplishments that I'm proud of
+ It was tricky at first, but I was able to design a FormIntent class that makes it fairly simple to turn standard Django Form objects into Lex intents
+ Built a parser that creates Lex bots from a YAML file
+ I used FIFO queues (via SQS) to feed messages from Facebook into Lex
+ First time using Terraform
+ First time using Lambda

## What I learned
+ Terraform makes provisioning infrastructure **much** easier. All my infrastructure is stored in source control, and I don't get slowed down by a GUI if I ever need to make a lot of changes
+ Lambda is amazingly cheap and versatile. I was surprised at how relatively easy it was for me to package a full Django app within a Lambda function
+ Lex is pretty darn smart. Especially when it comes to determining a user's intended actions. 

## What's next for Koura
There are a **ton** of features I would love to add to this bot. Here are the top ones:
+ Switch from using Facebook Messenger to a mobile application built with React Native
+ Add charts for hours worked, cash flow, outstanding payments, etc.
+ Give businesses the option to set up recurring payments

## Testing
Koura is a [Facebook Messenger bot](https://www.facebook.com/Koura-1970538366498422/) that is currently not available to the public. I have added a tester role for stef.devpost.1. You can also use the Facebook account I created for demonstration:

+ Email Address: kouratester@protonmail.com
+ Password: getkoura123

Koura uses Stripe to process payments. Currently, Koura is connected to Stripe's sandbox so if you attempt to use real credit cards or bank account numbers you will get an error. Use the values below to test out Koura:

+ Credit card number: 4242 4242 4242 4242
+ Expiration date: 08/23
+ CVC: 123
+ Tax ID: 123456789
+ Bank account number: 000123456789
+ Routing number: 110000000
+ SSN Last 4: 1234

You can use any valid email account to test sending/receiving invoices. You can also use the [ProtonMail](https://protonmail.com/) account I created for testing:

+ Username: kouratester
+ Password: getkoura123