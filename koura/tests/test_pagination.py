from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from unittest import skip

from django.core.urlresolvers import reverse
from django_webtest import WebTest
from rest_framework import status

from koura.enums import View as ViewName
from django.test import TestCase
from koura.pagination import Paginator
from django.contrib.auth import get_user_model
from koura.users.factories import UserFactory
from functools import reduce
import json

User = get_user_model()


class PaginatorTestCase(TestCase):
    def setUp(self):
        for i in range(25):
            UserFactory()

    def tearDown(self):
        User.objects.all().delete()

    def test_page_token(self):
        queryset = User.objects.all().order_by('full_name')
        paginator = Paginator(queryset, 5)
        next_page_token = paginator.get_page_token(2, 'koura.users.models.User')
        self.assertIsNotNone(next_page_token)
        paginator, page = Paginator.from_page_token(next_page_token)
        users = page.object_list
        for i, user in enumerate(users):
            if i == 0:
                continue
            self.assertLessEqual(users[i - 1].full_name, user.full_name)

    def test_no_token(self):
        queryset = User.objects.all().order_by('full_name')[:1]
        paginator = Paginator(queryset, 5)
        self.assertIsNone(paginator.get_page_token(2, 'koura.users.models.User'))

    def test_token_size(self):
        queryset = User.objects.all().order_by('full_name')
        paginator = Paginator(queryset, 5)
        next_page_token = paginator.get_page_token(2, 'koura.users.models.User')
        payload = {
            'type': 'TYPE 1232342413213',
            'payload': {
                'next_page_token': next_page_token
            }
        }
        self.assertLessEqual(len(json.dumps(payload)), 1000)
