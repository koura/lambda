from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.views.generic import TemplateView


class TermsOfServiceView(TemplateView):
    template_name = 'terms_of_service.html'


class PrivacyPolicyView(TemplateView):
    template_name = 'privacy_policy.html'


class IndexView(TemplateView):
    template_name = 'index.html'
