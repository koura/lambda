from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from uuid import uuid4

import factory
import random
from faker import Faker

from koura.users.models import User

DEFAULT_PASSWORD = 'over9000'


class UserFactory(factory.django.DjangoModelFactory):
    screen_name = factory.LazyAttribute(lambda a: '%s%d' % (a.full_name.lower().replace(' ', ''), random.randint(0, 9000)))
    full_name = factory.Faker('name')
    nickname = factory.Faker('first_name')
    password = factory.PostGenerationMethodCall('set_password', DEFAULT_PASSWORD)

    @factory.lazy_attribute
    def messenger_user_id(self):
        return uuid4().hex

    @factory.lazy_attribute
    def email_address(self):
        faker = Faker()
        email_address = None
        while email_address is None or User.objects.filter(email_address=email_address).exists():
            email_address = faker.email()
        return email_address

    class Meta:
        model = User
