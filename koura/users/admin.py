from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

User = get_user_model()


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    readonly_fields = ('date_joined',)
    fieldsets = (
        (None, {'fields': ('email_address', 'screen_name', 'password')}),
        (_('Personal info'), {'fields': ('full_name', 'nickname')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    list_display = ('email_address', 'screen_name', 'is_staff')
    search_fields = ('email_address', 'screen_name', 'full_name')
    ordering = ('date_joined',)
