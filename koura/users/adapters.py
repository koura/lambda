from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid

from allauth.account.adapter import DefaultAccountAdapter
from allauth.account.utils import user_username, user_email, user_field
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.utils import valid_email_or_none

from koura.context_processors import constants

try:
    from django.utils.encoding import force_text
except ImportError:
    from django.utils.encoding import force_unicode as force_text


class SocialAccountAdapter(DefaultSocialAccountAdapter):
    def populate_user(self,
                      request,
                      sociallogin,
                      data):
        nickname = data.get('nickname', 'Friend')
        username = data.get('username')
        full_name = data.get('name')
        email = data.get('email')
        user = sociallogin.user
        guid = uuid.uuid4().hex
        user_username(user, username or '%s' % guid)
        user_email(user, valid_email_or_none(email) or '%s@koura.com' % guid)
        user_field(user, 'full_name', full_name)
        user_field(user, 'nickname', nickname)
        return user


class UserAdapter(DefaultAccountAdapter):
    def send_mail(self, template_prefix, email, context):
        context.update(constants(None))
        return super(UserAdapter, self).send_mail(template_prefix, email, context)

    def format_email_subject(self, subject):
        return force_text(subject)

    def unstash_verified_email(self, request):
        if request:
            return super(UserAdapter, self).unstash_verified_email(request)
        return None

    def is_email_verified(self, request, email):
        if request:
            return super(UserAdapter, self).is_email_verified(request, email)
        return False

    def add_message(self, request, level, message_template,
                    message_context=None, extra_tags=''):
        if request:
            super(UserAdapter, self).add_message(request, level, message_template, message_context, extra_tags)

    def stash_user(self, request, user):
        if request:
            super(UserAdapter, self).stash_user(request, user)

    def login(self, request, user):
        if request:
            super(UserAdapter, self).login(request, user)

    # def get_login_redirect_url(self, request):
    #     if request:
    #         return super(UserAdapter, self).get_login_redirect_url(request)
    #     return '/'

    def save_user(self, request, user, form, commit=True):
        data = form.cleaned_data
        nickname = data.get('nickname', 'Friend')
        full_name = data.get('full_name')
        email_address = data.get('email_address')
        screen_name = data.get('screen_name')

        user_email(user, email_address)
        user_username(user, screen_name)
        if full_name:
            user_field(user, 'full_name', full_name)
        if nickname:
            user_field(user, 'nickname', nickname)
        if 'password1' in data:
            user.set_password(data['password1'])
        elif 'password' in data:
            user.set_password(data['password'])
        else:
            user.set_unusable_password()
        self.populate_username(request, user)

        if commit:
            # Ability not to commit makes it easier to derive from
            # this adapter by adding
            user.save()
        return user
