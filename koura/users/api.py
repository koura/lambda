from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from rest_auth.registration.views import SocialLoginView
from rest_framework import mixins
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response

from koura.users.serializers import ValidateEmailAddressSerializer
from koura.users.serializers import ValidateScreenNameSerializer


class UserViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    @list_route(methods=['post'])
    def validate_screen_name(self, request):
        serializer = ValidateScreenNameSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({}, status=status.HTTP_200_OK)

    @list_route(methods=['post'])
    def validate_email_address(self, request):
        serializer = ValidateEmailAddressSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({}, status=status.HTTP_200_OK)


class FacebookLoginView(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter
