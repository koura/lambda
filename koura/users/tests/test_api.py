from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import unittest

import random
from django.conf import settings
from django.contrib.sites.models import Site
from django.core import mail
from django.core.urlresolvers import reverse
from rest_framework import status

from koura.api import VersionedApiTestCase
from koura.factories import SiteFactory
from koura.factories import SocialAppFactory
from koura.users.factories import DEFAULT_PASSWORD
from koura.users.factories import UserFactory
from koura.users.models import User

DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')


class ValidateEmailAddressApiTestCase(VersionedApiTestCase):
    def test_valid_email_address(self):
        data = {
            'email_address': 'fjiijwa@gmail.com'
        }
        url = reverse('api_users:user_validate_email_address')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_duplicate_email_address(self):
        user = UserFactory()
        data = {
            'email_address': user.email_address
        }
        url = reverse('api_users:user_validate_email_address')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class ValidateScreenNameApiTestCase(VersionedApiTestCase):
    def test_valid_screen_name(self):
        data = {
            'screen_name': 'dougface'
        }
        url = reverse('api_users:user_validate_screen_name')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_characters_in_screen_name(self):
        data = {
            'screen_name': '!@#$%^&*()'
        }
        url = reverse('api_users:user_validate_screen_name')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_short_screen_name(self):
        data = {
            'screen_name': ''
        }
        url = reverse('api_users:user_validate_screen_name')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_duplicate_screen_name(self):
        user = UserFactory()
        data = {
            'screen_name': user.screen_name
        }
        url = reverse('api_users:user_validate_screen_name')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


@unittest.skip('These tests fail when the access tokens expire')
class FacebookLoginApiTestCase(VersionedApiTestCase):
    @classmethod
    def setUpClass(cls):
        super(FacebookLoginApiTestCase, cls).setUpClass()
        Site.objects.all().delete()
        SocialAppFactory(sites=(SiteFactory(),))

    @classmethod
    def tearDownClass(cls):
        super(FacebookLoginApiTestCase, cls).tearDownClass()

    def setUp(self):
        self.data = {
            'access_token': 'EAAVX7DL9w9UBAO07QOjiuMiJnz7uwlbuiX80jZCRDWZC6r4NY9kWnqpr4jsxgHJ3iUZBuYV1AEsxeKSdZAJTrX0cRRKVvgD3iTVZBFzTUuIRy8BkTbZAdZAnAUtZCDwmZBDBK2RVzXESxOEEyZAf73R4ZAvhwu7urWZC891wPAXbDYVqkcWE4lz2OecT',
        }
        self.assertEqual(User.objects.all().count(), 0)

    def tearDown(self):
        User.objects.all().delete()

    def test_login(self):
        data = self.data.copy()
        self._login(data)

    def test_multiple_logins(self):
        access_tokens = [
            'EAAVX7DL9w9UBAO07QOjiuMiJnz7uwlbuiX80jZCRDWZC6r4NY9kWnqpr4jsxgHJ3iUZBuYV1AEsxeKSdZAJTrX0cRRKVvgD3iTVZBFzTUuIRy8BkTbZAdZAnAUtZCDwmZBDBK2RVzXESxOEEyZAf73R4ZAvhwu7urWZC891wPAXbDYVqkcWE4lz2OecT',
            'EAAVX7DL9w9UBAI1lCXrKFvFwtZALAMSZA9HfZCaZADgNO51mdrnX6nOFKZAzqtDy6n3g0tmIPLuyCtGK8dzFIPbpR2eXiJnDZBADZCZBkgA9EmRmVJSGDaCUZC3XQ4Kaujsh8eug0xig40kYZC769rUqZBvbfyZC1UGZAZBJx1C2WnOq4XKob65A4IxQ6c'
        ]
        for i in range(10):
            data = {
                'access_token': random.choice(access_tokens)
            }
            response = self._login(data, False)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def _login(self, data, do_assert=True):
        url = reverse('api_users:facebook_login')
        response = self.client.post(url, data, format='json')
        if do_assert:
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTrue('key' in response.data)
            self.assertEqual(User.objects.all().count(), 1)
            user = User.objects.all()[0]
            self.assertEqual(user.full_name, 'Harry Aladhhgchhfdg Carrierosen')
            self.assertGreater(len(user.email_address), 0)
            self.assertGreater(len(user.screen_name), 0)
        return response


class PasswordResetApiTestCase(VersionedApiTestCase):
    @classmethod
    def setUpClass(cls):
        super(PasswordResetApiTestCase, cls).setUpClass()
        UserFactory.create_batch(1)

    @classmethod
    def tearDownClass(cls):
        super(PasswordResetApiTestCase, cls).tearDownClass()
        User.objects.all().delete()

    def setUp(self):
        self.user = User.objects.all()[0]
        self.data = {
            'email_address': self.user.email_address,
        }

    def _reset_password(self, data):
        url = reverse('api_users:rest_password_reset')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('detail' in response.data)
        return response

    def test_password_reset(self):
        data = self.data.copy()
        self._reset_password(data)

    def test_password_reset_email(self):
        data = self.data.copy()
        self._reset_password(data)
        self.assertEqual(len(mail.outbox), 1)
        password_reset_mail = mail.outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, password_reset_mail.from_email)
        self.assertEqual([self.user.email_address], password_reset_mail.to)


class LoginApiTestCase(VersionedApiTestCase):
    @classmethod
    def setUpClass(cls):
        super(LoginApiTestCase, cls).setUpClass()
        UserFactory.create_batch(1)

    @classmethod
    def tearDownClass(cls):
        super(LoginApiTestCase, cls).tearDownClass()
        User.objects.all().delete()

    def setUp(self):
        self.user = User.objects.all()[0]
        self.data = {
            'email_address': self.user.email_address,
            'password': DEFAULT_PASSWORD
        }

    def test_login(self):
        data = self.data.copy()
        url = reverse('api_users:rest_login')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('key' in response.data)


class UserRegistrationApiTestCase(VersionedApiTestCase):
    @classmethod
    def setUpClass(cls):
        super(UserRegistrationApiTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(UserRegistrationApiTestCase, cls).tearDownClass()
        User.objects.all().delete()

    def setUp(self):
        self.data = {
            'email_address': 'sterlingarcher@gmail.com',
            'screen_name': 'dangerzone123',
            'password1': 'password123',
            'password2': 'password123'
        }

    def _register_user(self, data, do_assert = True):
        url = reverse('api_users:rest_register')
        response = self.client.post(url, data, format='json')
        if do_assert:
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertTrue('key' in response.data)
            self.assertIsNotNone(response.data.get('key'))
            self.assertEqual(User.objects.all().count(), 1)
        return response

    def test_registration(self):
        self._register_user(self.data.copy())

    def test_verification_email(self):
        data = self.data.copy()
        self._register_user(data)
        self.assertEqual(len(mail.outbox), 1)
        verification_mail = mail.outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, verification_mail.from_email)
        self.assertEqual([data.get('email_address')], verification_mail.to)

    def test_invalid_screen_name(self):
        data = self.data.copy()
        data['screen_name'] = 'DANGERZONE!?!?!?'
        response = self._register_user(data, False)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue('screen_name' in response.data)

    def test_duplicate_email_address(self):
        data = self.data.copy()
        self._register_user(data)

        data['screen_name'] = 'LANA123'
        response = self._register_user(data, False)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue('email_address' in response.data)
        self.assertEqual(User.objects.all().count(), 1)

    def test_duplicate_screen_name(self):
        data = self.data.copy()
        self._register_user(data)

        data['email_address'] = 'lana@gmail.com'
        response = self._register_user(data, False)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue('screen_name' in response.data)
        self.assertEqual(User.objects.all().count(), 1)
