from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from django.core import mail
from django.test import TestCase

from koura.users.forms import UserForm

DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')

User = get_user_model()


class UserFormTestCase(TestCase):
    def setUp(self):
        self.data = {
            'email_address': 'sterlingarcher@gmail.com',
            'screen_name': 'Archie1234',
            'password': 'qefuh24yfhq2348wefof2eip2349-924',
            'nickname': 'Archie',
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_init(self):
        form = UserForm()
        self.assertIsNotNone(form)

    def test_valid_data(self):
        form = UserForm(data=self.data.copy())
        self.assertTrue(form.is_valid())

    def test_create_user(self):
        data = self.data.copy()
        form = UserForm(data=data)
        self.assertTrue(form.is_valid())
        user = form.save()
        self.assertIsNotNone(user)
        self.assertEqual(User.objects.all().count(), 1)
        self.assertIsNotNone(authenticate(
            username=data.get('email_address'),
            password=data.get('password')
        ))

    def test_invalid_screen_name(self):
        data = self.data.copy()
        data.update({
            'screen_name': '!?!?!?!?'
        })
        form = UserForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('screen_name' in form.errors)

    def test_long_screen_name(self):
        data = self.data.copy()
        data.update({
            'screen_name': 'aaaaaaaaaffhfhfhfhfhfawfhawidskfasdklfjasdlfdsafsdfasdfasdfsfasfasdfsd'
        })
        form = UserForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('screen_name' in form.errors)

    def test_invalid_nickname(self):
        data = self.data.copy()
        data.update({
            'nickname': 'Emek!@#'
        })
        form = UserForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('nickname' in form.errors)

    def test_clean_nickname(self):
        data = self.data.copy()
        data.update({
            'nickname': '   Archie    '
        })
        form = UserForm(data=data)
        self.assertTrue(form.is_valid())
        user = form.save()
        self.assertEqual(data.get('nickname').strip(), user.nickname)

    def test_invalid_email_address(self):
        data = self.data.copy()
        data.update({
            'email_address': 'myemail'
        })
        form = UserForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('email_address' in form.errors)

    def test_duplicate_email_address(self):
        data = self.data.copy()
        form = UserForm(data=data)
        self.assertTrue(form.is_valid())
        form.save()

        # Try the same email address
        data = self.data.copy()
        data['screen_name'] = 'fwafe89'
        form = UserForm(data=data)
        self.assertFalse(form.is_valid())

    def test_duplicate_screen_name(self):
        data = self.data.copy()
        form = UserForm(data=data)
        self.assertTrue(form.is_valid())
        form.save()

        # Try the same email address
        data = self.data.copy()
        data['email_address'] = 'fwafe89@gmail.com'
        form = UserForm(data=data)
        self.assertFalse(form.is_valid())

    def test_short_password(self):
        data = self.data.copy()
        data['password'] = '1234'
        form = UserForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('password' in form.errors)

    def test_weak_password(self):
        data = self.data.copy()
        data['password'] = '123456789'
        form = UserForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('password' in form.errors)

    def test_blank_data(self):
        data = {}
        form = UserForm(data=data)
        form.is_valid()
        self.assertFalse(form.is_valid())

    def test_verification_email(self):
        data = self.data.copy()
        form = UserForm(data=data)
        form.is_valid()
        form.save()
        self.assertEqual(len(mail.outbox), 1)
        verification_mail = mail.outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, verification_mail.from_email)
        self.assertEqual([data.get('email_address')], verification_mail.to)

