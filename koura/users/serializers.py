from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from allauth.account import app_settings as allauth_settings
from allauth.account.adapter import get_adapter
from allauth.account.forms import ResetPasswordForm
from allauth.account.utils import setup_user_email
from allauth.utils import (email_address_exists, get_username_max_length)
from django.conf import settings
from django.contrib.auth import get_user_model, authenticate
from django.core import exceptions
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers, exceptions

from koura.users.models import SCREEN_NAME_VALIDATORS

# Get the UserModel
UserModel = get_user_model()


class ValidateEmailAddressSerializer(serializers.Serializer):
    email_address = serializers.EmailField()

    def validate_email_address(self, email_address):
        if UserModel.objects.filter(email_address=email_address).exists():
            raise serializers.ValidationError('This email address is already being used.')
        return email_address


class ValidateScreenNameSerializer(serializers.Serializer):
    screen_name = serializers.CharField(min_length=1)

    def validate_screen_name(self, screen_name):
        if UserModel.objects.filter(screen_name=screen_name).exists():
            raise serializers.ValidationError('This screen name is already being used.')
        for validator in SCREEN_NAME_VALIDATORS:
            validator(screen_name)
            # try:
            #     validator(screen_name)
            # except exceptions.ValidationError:
            #     raise serializers.ValidationError('Invalid screen name.')
        return screen_name


class LoginSerializer(serializers.Serializer):
    screen_name = serializers.CharField(required=False, allow_blank=True)
    email_address = serializers.EmailField(required=False, allow_blank=True)
    password = serializers.CharField(style={'input_type': 'password'})

    def _validate_email_address(self, email_address, password):
        user = None

        if email_address and password:
            user = authenticate(email_address=email_address, password=password)
        else:
            msg = _('Must include "email address" and "password".')
            raise exceptions.ValidationError(msg)

        return user

    def _validate_screen_name(self, screen_name, password):
        user = None

        if screen_name and password:
            user = authenticate(screen_name=screen_name, password=password)
        else:
            msg = _('Must include "screen_name" and "password".')
            raise exceptions.ValidationError(msg)

        return user

    def _validate_screen_name_email_address(self, screen_name, email_address, password):
        user = None

        if email_address and password:
            user = authenticate(email_address=email_address, password=password)
        elif screen_name and password:
            user = authenticate(screen_name=screen_name, password=password)
        else:
            msg = _('Must include either "screen_name" or "email_address" and "password".')
            raise exceptions.ValidationError(msg)

        return user

    def validate(self, attrs):
        screen_name = attrs.get('screen_name')
        email_address = attrs.get('email_address')
        password = attrs.get('password')

        user = None

        if 'allauth' in settings.INSTALLED_APPS:
            from allauth.account import app_settings

            # Authentication through email_address
            if app_settings.AUTHENTICATION_METHOD == app_settings.AuthenticationMethod.EMAIL:
                user = self._validate_email_address(email_address, password)

            # Authentication through screen_name
            elif app_settings.AUTHENTICATION_METHOD == app_settings.AuthenticationMethod.USERNAME:
                user = self._validate_screen_name(screen_name, password)

            # Authentication through either screen_name or email_address
            else:
                user = self._validate_screen_name_email_address(screen_name, email_address, password)

        else:
            # Authentication without using allauth
            if email_address:
                try:
                    screen_name = UserModel.objects.get(email_address__iexact=email_address).get_screen_name()
                except UserModel.DoesNotExist:
                    pass

            if screen_name:
                user = self._validate_screen_name_email_address(screen_name, '', password)

        # Did we get back an active user?
        if user:
            if not user.is_active:
                msg = _('User account is disabled.')
                raise exceptions.ValidationError(msg)
        else:
            msg = _('Unable to log in with provided credentials.')
            raise exceptions.ValidationError(msg)

        # If required, is the email_address verified?
        if 'rest_auth.registration' in settings.INSTALLED_APPS:
            from allauth.account import app_settings
            if app_settings.EMAIL_VERIFICATION == app_settings.EmailVerificationMethod.MANDATORY:
                email_address_address = user.email_addressaddress_set.get(email_address=user.email_address)
                if not email_address_address.verified:
                    raise serializers.ValidationError(_('E-mail is not verified.'))

        attrs['user'] = user
        return attrs


class RegistrationSerializer(serializers.Serializer):
    screen_name = serializers.CharField(
        max_length=get_username_max_length(),
        min_length=allauth_settings.USERNAME_MIN_LENGTH,
        required=allauth_settings.USERNAME_REQUIRED
    )
    email_address = serializers.EmailField(required=allauth_settings.EMAIL_REQUIRED)
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)

    def get_cleaned_data(self):
        return {
            'screen_name': self.validated_data.get('screen_name', ''),
            'password1': self.validated_data.get('password1', ''),
            'email_address': self.validated_data.get('email_address', '')
        }

    def validate_screen_name(self, screen_name):
        screen_name = get_adapter().clean_username(screen_name)
        return screen_name

    def validate_email_address(self, email_address):
        email_address = get_adapter().clean_email(email_address)
        if allauth_settings.UNIQUE_EMAIL:
            if email_address and email_address_exists(email_address):
                raise serializers.ValidationError(_('Another account is using %s.' % email_address))
        return email_address

    def validate_password1(self, password):
        return get_adapter().clean_password(password)

    def validate(self, data):
        if data['password1'] != data['password2']:
            raise serializers.ValidationError(_("The two password fields didn't match."))
        return data

    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        self.cleaned_data = self.get_cleaned_data()
        adapter.save_user(request, user, self)
        setup_user_email(request, user, [])
        return user


class PasswordResetSerializer(serializers.Serializer):
    email_address = serializers.EmailField()

    password_reset_form_class = ResetPasswordForm

    def get_email_options(self):
        """Override this method to change default e-mail options"""
        return {}

    def validate_email_address(self, value):
        # Create PasswordResetForm with the serializer
        data = {
            'email': self.initial_data.get('email_address')
        }
        self.reset_form = self.password_reset_form_class(data=data)
        if not self.reset_form.is_valid():
            raise serializers.ValidationError(self.reset_form.errors)

        return value

    def save(self):
        request = self.context.get('request')
        # Set some values to trigger the send_email method.
        opts = {
            'use_https': request.is_secure(),
            'from_email': getattr(settings, 'DEFAULT_FROM_EMAIL'),
            'request': request,
        }

        opts.update(self.get_email_options())
        self.reset_form.save(**opts)
