from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from allauth.account import app_settings as allauth_settings, signals as allauth_signals
from allauth.account.adapter import get_adapter
from allauth.account.models import EmailAddress
from allauth.account.utils import send_email_confirmation
from allauth.account.utils import setup_user_email
from django import forms
from django.contrib.auth import forms as auth_forms
from django.contrib.auth import get_user_model

User = get_user_model()


class AuthenticationForm(auth_forms.AuthenticationForm):
    username = forms.EmailField()

    def add_error(self, field, error):
        if field == 'username':
            error = forms.ValidationError('That doesn\'t look like a valid email address. Please try entering another one.')
        super(AuthenticationForm, self).add_error(field, error)


class UserForm(forms.ModelForm):
    def clean_screen_name(self):
        screen_name = self.cleaned_data['screen_name']
        return screen_name.lower()

    def save(self, commit=True):
        adapter = get_adapter()
        user = adapter.new_user(None)
        adapter.save_user(None, user, self)
        # complete_signup(None, user, allauth_settings.EMAIL_VERIFICATION, None)
        allauth_signals.user_signed_up.send(
            sender=user.__class__,
            request=None,
            user=user
        )

        # For allauth
        setup_user_email(None, user, [])

        # Email via allauth
        request = None
        has_verified_email = EmailAddress.objects.filter(user=user, verified=True).exists()
        if allauth_settings.EMAIL_VERIFICATION == allauth_settings.EmailVerificationMethod.NONE:
            pass
        elif allauth_settings.EMAIL_VERIFICATION == allauth_settings.EmailVerificationMethod.OPTIONAL:
            # In case of OPTIONAL verification: send on signup.
            if not has_verified_email:
                send_email_confirmation(request, user, signup=False)
        elif allauth_settings.EMAIL_VERIFICATION == allauth_settings.EmailVerificationMethod.MANDATORY:
            if not has_verified_email:
                send_email_confirmation(request, user, signup=False)
                return adapter.respond_email_verification_sent(request, user)

        return user

    class Meta:
        model = User
        fields = [
            'email_address',
            'screen_name',
            'password',
            'nickname'
        ]