from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from rest_framework.test import APIClient
from rest_framework.test import APITestCase


class VersionedApiClient(APIClient):
    def request(self, **kwargs):
        if 'HTTP_ACCEPT' not in kwargs:
            kwargs.update({'HTTP_ACCEPT': 'application/json; version=1.0.0'})
        return super(VersionedApiClient, self).request(**kwargs)


class VersionedApiTestCase(APITestCase):
    client_class = VersionedApiClient
