from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import factory
from allauth.socialaccount.models import SocialApp
from django.conf import settings
from django.contrib.sites.models import Site

SITE_ID = getattr(settings, 'SITE_ID')
FACEBOOK_PUBLIC_KEY = getattr(settings, 'FACEBOOK_PUBLIC_KEY')
FACEBOOK_SECRET_KEY = getattr(settings, 'FACEBOOK_SECRET_KEY')


class SiteFactory(factory.django.DjangoModelFactory):
    domain = 'test.koura.com'
    name = 'Errbody'
    id = SITE_ID

    class Meta:
        model = Site


class SocialAppFactory(factory.django.DjangoModelFactory):
    provider = 'facebook'
    name = 'TEST'
    client_id = FACEBOOK_PUBLIC_KEY
    secret = FACEBOOK_SECRET_KEY

    @factory.post_generation
    def sites(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for site in extracted:
                self.sites.add(site)

    class Meta:
        model = SocialApp
