from __future__ import absolute_import

import logging
import urllib
from html.parser import HTMLParser
from urllib.parse import urljoin

from django.conf import settings
from django.contrib.sites.models import Site
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.core.urlresolvers import reverse
from django.utils import six
from django.utils.functional import lazy

logger = logging.getLogger(__name__)


SCHEME = 'https://'


def get_absolute_static_url(path):
    return urljoin(settings.STATIC_URL, static(path))


def get_absolute_url(view_name=None, url=None, **kwargs):
    if view_name:
        url = reverse(view_name, **kwargs)
    site = Site.objects.get_current()
    return urljoin('%s%s' % (SCHEME, site.domain), url)


def reverse_with_query(view_name, query_params, do_escape=True, **kwargs):
    url = reverse(view_name, **kwargs)
    if query_params:
        url += '?'
        if do_escape:
            url += urllib.parse.urlencode(query_params)
        else:
            url += HTMLParser().unescape(urllib.parse.urlencode(query_params)).replace('%2F', '/')
    return url

get_absolute_url_lazy = lazy(get_absolute_url, six.text_type)
