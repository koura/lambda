from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from django.conf import settings
from django.utils.deconstruct import deconstructible
from storages.backends.s3boto3 import S3Boto3Storage

STATICFILES_LOCATION = getattr(settings, 'STATICFILES_LOCATION', 'static')
MEDIAFILES_LOCATION = getattr(settings, 'MEDIAFILES_LOCATION', 'media')
logger = logging.getLogger(__name__)


class S3StaticStorage(S3Boto3Storage):
    location = STATICFILES_LOCATION

    def __init__(self, acl=None, bucket=None, **settings):
        super(S3StaticStorage, self).__init__(acl=acl, bucket=bucket, **settings)
        self.security_token = self._get_security_token()
        logger.info('%s %s %s', self.access_key, self.secret_key, self.security_token)


@deconstructible
class S3MediaStorage(S3Boto3Storage):
    location = MEDIAFILES_LOCATION

    def __init__(self, acl=None, bucket=None, **settings):
        super(S3MediaStorage, self).__init__(acl=acl, bucket=bucket, **settings)
        self.security_token = self._get_security_token()
        logger.info('%s %s %s', self.access_key, self.secret_key, self.security_token)

