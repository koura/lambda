from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from enum import Enum


class QueueName(Enum):
    MESSENGER_WEBHOOKS = 'KouraMessengerWebhooks.fifo'
    STRIPE_WEBHOOKS = 'KouraStripeWebhooks.fifo'
