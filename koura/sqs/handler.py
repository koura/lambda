from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import logging
from importlib import import_module
from inspect import unwrap

import django
import os
from django.apps import apps
from django.conf import settings

# Configure Django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "koura.settings")
if not apps.ready and not settings.configured:
    django.setup()

logger = logging.getLogger(__name__)

import boto3
from django.conf import settings


class SqsConsumer(object):
    def __init__(self, event):
        self.event = event
        self.sqs_client = boto3.client('sqs')
        response = self.sqs_client.get_queue_url(
            QueueName=self.event.get('queue_name')
        )
        self.queue_url = response.get('QueueUrl')

    def run(self, context):
        while True:
            response = self.sqs_client.receive_message(
                QueueUrl=self.queue_url,
            )
            if 'Messages' not in response:
                break
            if len(response.get('Messages')) <= 0:
                break
            for message in response.get('Messages'):  # Should be one at a time
                logger.debug('Processing message from SQS\n%s', json.dumps(message, indent=2))
                payload = json.loads(message.get('Body'))
                logger.debug('Payload is\n%s', json.dumps(payload, indent=2))

                # Execute code
                module_name = payload.get('module')
                function_name = payload.get('function')
                args = payload.get('args', [])
                kwargs = payload.get('kwargs', {})
                func = unwrap(getattr(import_module(module_name), function_name))
                try:
                    func(*args, **kwargs)
                except Exception:
                    logger.exception('Failed to execute message from queue (%s)\n%s' % (self.event.get('queue_name'), json.dumps(message, indent=2)) )

                # Tell SQS message was received
                self.sqs_client.delete_message(
                    QueueUrl=self.queue_url,
                    ReceiptHandle=message.get('ReceiptHandle')
                )

            if context.get_remaining_time_in_millis() < 1000:
                # Kick it of again because there might still be messages
                client = boto3.client('lambda')
                payload = {
                    'queue_name': self.event.get('queue_name')
                }
                client.invoke(
                    FunctionName=settings.AWS_LAMBDA_FUNCTION_NAME,
                    InvocationType='Event',
                    Payload=json.dumps(payload)
                )
                logger.debug('Exit while loop with %d ms left' % context.get_remaining_time_in_millis())
                break


def handler(event, context):
    logger.debug('We received an invocation to check SQS queue!\n%s' % (json.dumps(event, indent=2, sort_keys=True), ))
    sqs_consumer = SqsConsumer(event)
    sqs_consumer.run(context)
