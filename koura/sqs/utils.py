from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import logging

import boto3
from django.conf import settings
from functools import wraps

logger = logging.getLogger(__name__)


class sqs_message(object):
    def __init__(self, queue_name, get_deduplication_id, get_group_id):
        self.queue_name = queue_name
        self.get_deduplication_id = get_deduplication_id
        self.get_group_id = get_group_id
        self.client = None
        self.queue_url = None
        if settings.AWS_SQS_IS_ENABLED:
            self.client = boto3.client('sqs')
            response = self.client.create_queue(
                QueueName=self.queue_name,
                Attributes={
                    'FifoQueue': 'true',
                    'ContentBasedDeduplication': 'false'
                }
            )
            self.queue_url = response.get('QueueUrl')

    def send_message(self, method, *args, **kwargs):
        payload = {
            'module': method.__module__,
            'function': method.__name__,
            'args': args,
            'kwargs': kwargs
        }
        self.client.send_message(**{
            'QueueUrl': self.queue_url,
            'MessageBody': json.dumps(payload),
            'MessageDeduplicationId': self.get_deduplication_id(*args, **kwargs),
            'MessageGroupId': self.get_group_id(*args, **kwargs),
        })

    def __call__(self, method):
        @wraps(method)
        def wrapper(*args, **kwargs):
            if settings.AWS_SQS_IS_ENABLED:
                self.send_message(method, *args, **kwargs)
            else:
                method(*args, **kwargs)
        return wrapper
