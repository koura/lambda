# Create your views here.
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import CreateView

from koura.leads.forms import LeadForm
from koura.leads.models import Lead


@method_decorator(cache_page(60 * 60 * 24), name='dispatch')
@method_decorator(csrf_exempt, name='dispatch')
class LeadCreationView(CreateView):
    model = Lead
    form_class = LeadForm
    template_name = 'leads/lead_creation_form.html'
    success_url = reverse_lazy('leads:create_lead_success')

@method_decorator(cache_page(60 * 60 * 24), name='dispatch')
class LeadCreationSuccessView(LeadCreationView):
    def get_context_data(self, **kwargs):
        context_data = super(LeadCreationSuccessView, self).get_context_data(**kwargs)
        context_data['is_success'] = True
        return context_data
