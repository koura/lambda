from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.test import TestCase

from koura.leads.models import Lead


class LeadCase(TestCase):
    def setUp(self):
        self.data = {
            'email_address': 'sterlingarcher@gmail.com',
        }
    def tearDown(self):
        Lead.objects.all().delete()

    def test_create_lead(self):
        lead = Lead.objects.create(**self.data)
        self.assertIsNotNone(lead)
