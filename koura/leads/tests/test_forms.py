from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.test import TestCase

from koura.leads.forms import LeadForm
from koura.leads.models import Lead


class LeadFormTestCase(TestCase):
    def setUp(self):
        self.data = {
            'email_address': 'sterlingarcher@gmail.com',
        }

    def tearDown(self):
        Lead.objects.all().delete()

    def test_create_lead(self):
        self._create_lead()

    def _create_lead(self):
        form = LeadForm(data=self.data)
        self.assertTrue(form.is_valid())
        lead = form.save()
        self.assertIsNotNone(lead)
        self.assertEqual(Lead.objects.all().count(), 1)

    def test_existing_email_address(self):
        self._create_lead()
        form = LeadForm(data=self.data)
        self.assertFalse(form.is_valid())
        self.assertTrue('email_address' in form.errors.as_data())
        self.assertEqual(Lead.objects.all().count(), 1)
