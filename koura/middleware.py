from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

import re
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.middleware import common
from django.urls import is_valid_path

WSGI_SCRIPT_ALIAS = getattr(settings, 'WSGI_SCRIPT_ALIAS')
DO_REMOVE_WSGI_SCRIPT_ALIAS = getattr(settings, 'DO_REMOVE_WSGI_SCRIPT_ALIAS')
logger = logging.getLogger(__name__)


class CommonMiddleware(common.CommonMiddleware):
    def process_request(self, request):
        """
        Check for denied User-Agents and rewrite the URL based on
        settings.APPEND_SLASH and settings.PREPEND_WWW
        """

        # Check for denied User-Agents
        if 'HTTP_USER_AGENT' in request.META:
            for user_agent_regex in settings.DISALLOWED_USER_AGENTS:
                if user_agent_regex.search(request.META['HTTP_USER_AGENT']):
                    raise PermissionDenied('Forbidden user agent')

        # Check for a redirect based on settings.PREPEND_WWW
        host = request.get_host()
        must_prepend = settings.PREPEND_WWW and host and not host.startswith('www.')
        redirect_url = ('%s://www.%s' % (request.scheme, host)) if must_prepend else ''

        # Check if a slash should be appended
        if self.should_redirect_with_slash(request):
            path = self.get_full_path_with_slash(request)
        elif self.should_redirect_without_alias(request):
            path = self.get_full_path_without_alias(request)
        else:
            path = request.get_full_path()

        # Return a redirect if necessary
        if redirect_url or path != request.get_full_path():
            redirect_url += path
            return self.response_redirect_class(redirect_url)

    def should_redirect_without_alias(self, request):
        if DO_REMOVE_WSGI_SCRIPT_ALIAS and re.match('^/%s' % WSGI_SCRIPT_ALIAS, request.path_info):
            urlconf = getattr(request, 'urlconf', None)
            return not is_valid_path(request.path_info, urlconf) and is_valid_path(re.sub('^/%s' % WSGI_SCRIPT_ALIAS, '', request.path_info), urlconf)
        return False

    def get_full_path_without_alias(self, request):
        path = super(CommonMiddleware, self).get_full_path_with_slash(request)
        if DO_REMOVE_WSGI_SCRIPT_ALIAS:
            path = re.sub('^/%s' % WSGI_SCRIPT_ALIAS, '', path)
        return path
