from __future__ import absolute_import

from django.conf.urls import url
from django.views.decorators.cache import cache_page
from django.views.decorators.cache import never_cache
from rest_framework.routers import DynamicDetailRoute
from rest_framework.routers import DynamicListRoute
from rest_framework.routers import Route
from rest_framework.routers import SimpleRouter

from koura.decorators import cache_on_auth


class Router(SimpleRouter):
    routes = [
        # List route.
        Route(
            url=r'^{prefix}{trailing_slash}$',
            mapping={
                'get': 'list',
                'post': 'create'
            },
            name='{basename}_list',
            initkwargs={'suffix': 'List'}
        ),
        # Dynamically generated list routes.
        # Generated using @list_route decorator
        # on methods of the viewset.
        DynamicListRoute(
            url=r'^{prefix}/{methodnamehyphen}{trailing_slash}$',
            name='{basename}_{methodname}',
            initkwargs={}
        ),
        # Detail route.
        Route(
            url=r'^{prefix}/{lookup}{trailing_slash}$',
            mapping={
                'get': 'retrieve',
                'put': 'update',
                'patch': 'partial_update',
                'delete': 'destroy'
            },
            name='{basename}_detail',
            initkwargs={'suffix': 'Instance'}
        ),
        # Dynamically generated detail routes.
        # Generated using @detail_route decorator on methods of the viewset.
        DynamicDetailRoute(
            url=r'^{prefix}/{lookup}/{methodname}{trailing_slash}$',
            name='{basename}_{methodnamehyphen}',
            initkwargs={}
        ),
    ]

    def __init__(self, trailing_slash=True, cache_timeout=0, is_login_required=False, do_never_cache=False):
        self.cache_timeout = cache_timeout
        self.do_never_cache = do_never_cache
        self.is_login_required = is_login_required
        super(Router, self).__init__(trailing_slash)

    def get_urls(self):
        """
        Use the registered viewsets to generate a list of URL patterns.
        """
        ret = []

        for prefix, viewset, basename in self.registry:
            lookup = self.get_lookup_regex(viewset)
            routes = self.get_routes(viewset)

            for route in routes:

                # Only actions which actually exist on the viewset will be bound
                mapping = self.get_method_map(viewset, route.mapping)
                if not mapping:
                    continue

                # Build the url pattern
                regex = route.url.format(
                    prefix=prefix,
                    lookup=lookup,
                    trailing_slash=self.trailing_slash
                )
                view = viewset.as_view(mapping, **route.initkwargs)
                name = route.name.format(basename=basename)

                # Wrap it up
                if self.do_never_cache:
                    view = never_cache(view)
                else:
                    if self.is_login_required:
                        view = cache_on_auth(self.cache_timeout)(view)
                    else:
                        view = cache_page(self.cache_timeout)(view)

                ret.append(url(regex, view, name=name))

        return ret
