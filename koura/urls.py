import django
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.http import HttpResponse
from django.views import View
from rest_auth.registration.urls import urlpatterns as rest_auth_registration_urls
from rest_auth.urls import urlpatterns as rest_auth_urls

from koura.businesses.api import StripeWebhookView
from koura.businesses.enums import View as BusinessViewName
from koura.businesses.views import ConfirmInvoiceView, ChargeInvoiceView, ChargeInvoiceSuccessView
from koura.enums import Namespace
from koura.enums import View as ViewName
from koura.lex.api import MessengerWebhookView
from koura.routers import Router
from koura.users.api import FacebookLoginView
from koura.users.api import UserViewSet
from koura.views import TermsOfServiceView, PrivacyPolicyView
from django.views.decorators.cache import cache_page
from django.views.generic.base import RedirectView

urlpatterns = list()

# 94uTnuR7yBJw6El0PthrXjqpzXbVq0icvZXqROVc7nhwbR5aMS8cGiIO7b1kB038


class HelloWorldView(View):
    def get(self, request):
        return HttpResponse('Hello World! My name is Koura!')

        
class IndexView(RedirectView):
    url = 'https://www.facebook.com/Koura-1970538366498422/'

    
# Businesses API
business_api_urls = [
    url(r'^94uTnuR7yBJw6El0PthrXjqpzXbVq0icvZXqROVc7nhwbR5aMS8cGiIO7b1kB038/$', StripeWebhookView.as_view(),
        name=BusinessViewName.STRIPE_WEBHOOK.value)
]
urlpatterns.extend([
    url(r'^api/businesses/', include((business_api_urls, None), namespace=Namespace.BUSINESSES_API.value))
])

# Messengers
messenger_urls = [
    url(r'^JTwH7M4l2gzfTBXODA9NJpIYwsL74z1oRTFUxdxWEScGq0HzF4qJL3Sm28iHC7S3/$', MessengerWebhookView.as_view(),
        name=ViewName.MESSENGER_WEBHOOK.value)
]
urlpatterns.extend([
    url(r'^api/lex/', include((messenger_urls, None), namespace=Namespace.LEX_API.value))
])

# Users api
user_router = Router(is_login_required=False)
user_router.register('user', UserViewSet, base_name='user')
user_urls = [
    url(r'^facebook/$', FacebookLoginView.as_view(), name='facebook_login')
]
user_urls.extend(user_router.urls)
urlpatterns.extend([
    url(r'^api/users/', include((rest_auth_urls + rest_auth_registration_urls + user_urls, None), namespace='api_users'))
])
urlpatterns.append(
    url(r'^users/', include('allauth.urls'))
)

# Static stuff
if settings.DEBUG:
    urlpatterns.append(
        url(r'^media/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.MEDIA_ROOT})
    )
    urlpatterns.append(
        url(r'^static/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.STATIC_ROOT})
    )

# Admin
urlpatterns.extend([
    url(r'^fbd0f2fc/', include(admin.site.urls)),
    url(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
])

# Flat
urlpatterns.extend([
    url(r'^invoices/(?P<token>[0-9a-zA-Z:\-_]+)/confirm/$', ConfirmInvoiceView.as_view(), name=BusinessViewName.CONFIRM_INVOICE.value),
    url(r'^invoices/(?P<token>[0-9a-zA-Z:\-_]+)/checkout/$', ChargeInvoiceView.as_view(), name=BusinessViewName.CHARGE_INVOICE.value),
    url(r'^invoices/(?P<token>[0-9a-zA-Z:\-_]+)/checkout/success/$', ChargeInvoiceSuccessView.as_view(), name=BusinessViewName.CHARGE_INVOICE_SUCCESS.value),
    url(r'^terms/$', cache_page(60 * 60 * 24)(TermsOfServiceView.as_view()), name=ViewName.TERMS_OF_SERVICE.value),
    url(r'^privacy/$', cache_page(60 * 60 * 24)(PrivacyPolicyView.as_view()), name=ViewName.PRIVACY_POLICY.value),
    url(r'^$', IndexView.as_view())
])
