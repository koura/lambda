from __future__ import absolute_import

from django.conf import settings


def api_keys(request):
    return {
        'facebook_public_key': getattr(settings, 'FACEBOOK_PUBLIC_KEY', None),
        'stripe_public_key': getattr(settings, 'STRIPE_PUBLIC_KEY', None),
    }


def constants(request):
    return {
        'site_display_name': getattr(settings, 'SITE_DISPLAY_NAME', None),
        'login_url': getattr(settings, 'LOGIN_URL', None),
        'email_closing': getattr(settings, 'EMAIL_CLOSING', None),
        'support_email': getattr(settings, 'SUPPORT_EMAIL', None),
    }
