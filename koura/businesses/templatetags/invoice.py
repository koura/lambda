from django import template

register = template.Library()


@register.filter
def amount(x):
    return '$%.2f' % (x / 100)
