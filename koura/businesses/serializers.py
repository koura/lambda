from __future__ import absolute_import

import logging

from rest_framework import serializers

from koura.businesses.models import LineItem, TimeSheet

logger = logging.getLogger(__name__)


class TimeSheetSerializer(serializers.ModelSerializer):
    def to_internal_value(self, data):
        internal_value = super().to_internal_value(data)
        if 'rounded_hours' in data:
            internal_value['rounded_hours'] = data.get('rounded_hours')
        return internal_value
      
    class Meta:
        model = TimeSheet
        fields = ('hourly_rate', 'rounded_hours')


class LineItemSerializer(serializers.ModelSerializer):
    time_sheet = TimeSheetSerializer(required=False, allow_null=True)

    class Meta:
        model = LineItem
        exclude = ('invoice', )
