from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from django import forms
from django.core import signing
from django.core.exceptions import SuspiciousOperation
from django.urls import reverse, reverse_lazy
from django.views.generic import FormView
from django.views.generic import TemplateView
from functools import reduce

from koura.businesses.enums import View
from koura.businesses.forms import ChargeInvoiceForm
from koura.businesses.models import Business, Client, Invoice
from koura.businesses.serializers import LineItemSerializer

logger = logging.getLogger(__name__)


class ChargeInvoiceSuccessView(TemplateView):
    template_name = 'businesses/charge_invoice_success.html'

    def get(self, request, *args, **kwargs):
        self.invoice_token = kwargs['token']
        if not Invoice.objects.is_token_valid(self.invoice_token):
            raise SuspiciousOperation
        return super(ChargeInvoiceSuccessView, self).get(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(ChargeInvoiceSuccessView, self).get_context_data(**kwargs)
        context['invoice'] = Invoice.objects.from_token(self.invoice_token)
        context['line_items'] = context['invoice'].line_items.all().order_by('order')
        return context


class ChargeInvoiceView(FormView):
    form_class = ChargeInvoiceForm
    template_name = 'businesses/charge_invoice.html'
    invoice_token = None

    def get_success_url(self):
        return reverse(View.CHARGE_INVOICE_SUCCESS.value, kwargs={'token': self.invoice_token})

    def form_valid(self, form):
        form.submit()
        return super(ChargeInvoiceView, self).form_valid(form)

    def get_initial(self):
        return {
            'invoice': self.invoice_token
        }

    def get_form_kwargs(self):
        form_kwargs = super(ChargeInvoiceView, self).get_form_kwargs()
        if self.request.method in ('POST', 'PUT'):
            if 'stripeToken' in self.request.POST:
                data = self.request.POST.copy()
                data['stripe_token'] = self.request.POST.get('stripeToken')
                form_kwargs['data'] = data
        return form_kwargs

    def get(self, request, *args, **kwargs):
        self.invoice_token = kwargs['token']
        form = self.form_class(data={'invoice': self.invoice_token})
        try:
            form.is_valid()
        except forms.ValidationError:
            pass
        if 'invoice' in form.errors:
            raise SuspiciousOperation
        return super(ChargeInvoiceView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.invoice_token = kwargs['token']
        form = self.form_class(data={'invoice': self.invoice_token})
        try:
            form.is_valid()
        except forms.ValidationError:
            pass
        if 'invoice' in form.errors:
            raise SuspiciousOperation
        return super(ChargeInvoiceView, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ChargeInvoiceView, self).get_context_data(**kwargs)
        form = self.form_class(data={'invoice': self.invoice_token})
        try:
            form.is_valid()
        except forms.ValidationError:
            pass
        context['invoice'] = form.cleaned_data.get('invoice')
        context['line_items'] = context['invoice'].line_items.all().order_by('order')
        context['is_read_only'] = False
        return context

    @staticmethod
    def get_url(invoice):
        token = invoice.token
        return reverse(View.CHARGE_INVOICE.value, kwargs={'token': token})


class ConfirmInvoiceView(TemplateView):
    template_name = 'businesses/charge_invoice.html'

    @staticmethod
    def get_url(data):
        token = signing.dumps(data, salt=ConfirmInvoiceView.get_salt())
        return reverse(View.CONFIRM_INVOICE.value, kwargs={'token': token})
    
    @staticmethod
    def get_salt():
        return 'VLetli0KTUiUCr1kFU1m4s1yD3J8xHQP0tqvjIBJvI4MMSqnsczLJELcNFRVsFMo'
    
    def get_context_data(self, **kwargs):
        context = super(ConfirmInvoiceView, self).get_context_data(**kwargs)
        token = kwargs['token']
        try:
            data = signing.loads(token, salt=ConfirmInvoiceView.get_salt())
        except signing.BadSignature:
            raise SuspiciousOperation

        data['business'] = Business.objects.get(id=data['business'])
        data['client'] = Client.objects.get(id=data['client'])
        serializer = LineItemSerializer(data=data['line_items'], many=True)
        serializer.is_valid()
        context['line_items'] = sorted(serializer.validated_data, key=lambda x: x.get('order'))
        data['amount'] = reduce(lambda x, y: x + y.get('amount'), context['line_items'], 0)
        context['invoice'] = data
        context['is_read_only'] = True
        return context
