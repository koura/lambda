from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from importlib import import_module

from django.conf import settings
from django.utils import timezone
from django.contrib import admin
from django.contrib.auth import get_user_model
import random

from koura.businesses.models import Business, Invoice, Client, TimeSheet, TimeSheetEntry, LineItem
from django.utils import timezone
from psycopg2.extras import DateTimeTZRange
from uuid import uuid4
from datetime import timedelta
from freezegun import freeze_time

SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
User = get_user_model()
FULL_NAMES = [
    'Mandi Oakey',
    'Braulio Tzul',
    'Herb Dominey',
    'Dayton Newbauer',
    'Ayanna Tiehen',
    'Alonso Larive',
    'Jon Reidel',
    'Bonifacio Bjarnason',
    'Keion Kyu',
    'Brenna Cumbaa',
    'Tamia Lybert',
    'Delilah Knaus',
    'Jasmine Fritchman',
    'Kasey Faia',
    'Benita Nocito',
    'Evette Kahraman',
    'Mitzi Skoglund',
    'Dina Merical',
    'Pascal Farless',
    'Misty Kassa',
    'Eston Dykins',
    'Ivery Forger',
    'Kristyn Toohill',
    'Blong Dufner',
    'Tommie Lorg',
    'Ben Kropf',
    'Willie Masica',
    'Tayler Sad',
    'Chaya Shadron',
    'Kasey Corkery',
    'Nathalie Caballo',
    'Fredy Kofmehl',
    'Ladarius Fencik',
    'Meredith Talon',
    'Carissa Kather',
    'Tamika Stortzum',
    'Kathi Richtmyer',
    'Carsen Obuch',
    'Abran Slachter',
    'Stella Balkus',
    'Beatriz Jakovich',
    'Trae Sachdev',
    'Cadarius Bundt',
    'Ashanti Obney',
    'Jaylin Wiedemeier',
    'Drake Hillaker',
    'Oren Infusino',
    'Kara Oei',
    'Teague Andorfer',
    'Deja Mogel',
    'Bailey Metoyer',
    'Donato Huenefeld',
    'Jazmin Minn',
    'Ivy Stats',
    'Sharlene Menchaca',
    'Tawanna Tarrow',
    'Channon Blewett',
    'Hashim Lodwig',
    'Devonta Rivadeneira',
    'Georgette Giebler',
    'Kimo Langmeyer',
    'Arlene Pontsler',
    'Edd Dentel',
    'Friedrich Haycraft',
    'Shawna Swantek',
    'Darvell Magid',
    'Haley Gerde',
    'Misti Billi',
    'Kenna Villarroel',
    'Arianna Schippers',
    'Evelyn Seidel',
    'Christiana Hanigan',
    'Thelma Musngi',
    'Rodolfo Oporta',
    'Jose Towler',
    'Tu Pellon',
    'Nickalus Halouska',
    'Michael Jrjohnson',
    'Chace Frauendorfer',
    'Eddrick Overberg',
    'Rueben Holzerland',
    'Victoria Lebeuf',
    'Delores Macias',
    'Jeffrie Cavaleri',
    'Karrie Lusk',
    'Angie Uher',
    'Elisa Andrzejewski',
    'Corey Usera',
    'Thurman Yamamura',
    'Karriem Uetrecht',
    'Franklin Butryn',
    'Domonick Hucek',
    'Shon Deboer',
    'Rosalinda Hoeflein',
    'Wendy Osterhouse',
    'Faith Wrona',
    'Rajan Chiarappa',
    'Rock Mcclymonds',
    'Abby Ledonne',
    'Lanell Escarcega',
    'Worth Mcfeaters',
    'Damarcus Lucien',
    'Sky Columbia',
    'Mae Benway',
    'Amelia Steinagel',
    'Darnelle Lamansky',
    'Alisa Blachowski',
    'Kalyn Murfitt',
    'Marc Hickson',
    'Reba Ralli',
    'Babak Damaske',
    'Tristan Raho',
    'Abelino Terranova',
    'Kaylie Voccola',
    'Kelly Salm',
    'Essence Krzyston',
    'Jamone Ritzel',
    'Stacey Earhart',
    'Ladale Klepach',
    'Yvonne Bierce',
    'Shanna Vedral',
    'Trina Krumsiek',
    'Rhonda Oringderff',
    'Sunny Levato',
    'Jan Miara',
    'Yesenia Simecek',
    'Julia Keenan',
    'Tyvon Vitalo',
    'Latoya Romagnolo',
    'Darci Loudy',
    'Sharon Palmero',
    'Nikole Layng',
    'Jami Acain',
    'Talia Moehrke',
    'Bernice Baronowski',
    'Colette Dagley',
    'Laurie Brem',
    'Kailee Bridenbaugh'
]
SERVICES = [
    'Air Duct Cleaning',
    'Alarms',
    'Animal Removal',
    'Antiques',
    'Apartments',
    'Appliance Refinishing',
    'Appliance Sales',
    'Appraisals',
    'Architects & Building Design',
    'Artwork/Murals',
    'Asbestos Removal',
    'Auction Services',
    'Awnings',
    'Basement Waterproofing',
    'Basketball Goals',
    'Bathtub Refinishing & Liners',
    'Billiard Table Sales',
    'Biohazard Remediation',
    'Blind Cleaning',
    'Buffing & Polishing',
    'Cabinet Making',
    'Cabinet Refacing/Restoration',
    'Carpet Cleaning',
    'Carpet Sales/Installation/Repair',
    'Ceiling Fans',
    'Central Vacuum Cleaning',
    'Ceramic Tile',
    'Chimney Caps',
    'Chimney Repair',
    'Chimney Sweep',
    'Cleaning',
    'Closets',
    'Countertops',
    'Deck Maintenance',
    'Decks & Porches',
    'Doors',
    'Drapery Cleaning',
    'Dryer Vent Cleaning',
    'Drywall',
    'Electrical',
    'Energy Efficiency Auditing',
    'Epoxy Flooring',
    'Excavating',
    'Fencing',
    'Fencing & Driveway Gates',
    'Fireplaces',
    'Firewood',
    'Floor Cleaning/Polishing/Waxing',
    'Flooring Sales/Installation/Repair',
    'Foundation Repair',
    'Fountains',
    'Framing',
    'Garage Doors',
    'Gas Leak Repair',
    'Gas Logs',
    'Generator',
    'Glass & Mirrors',
    'Glass Block',
    'Greenhouses/Nurseries',
    'Gutter Cleaning',
    'Gutter Repair & Replacement',
    'Handymen',
    'Heating & A/C',
    'Heating Oil',
    'Home Automation',
    'Home Inspection',
    'Home Staging',
    'Housecleaning',
    'Insulation',
    'Interior Design & Decorating',
    'Land Surveying',
    'Landscaping',
    'Landscaping & Lighting',
    'Lawn & Yard Work',
    'Lawn Fertilization & Treatment',
    'Lawn Irrigation',
    'Lawn Mower & Power Tool Repair',
    'Lead Testing & Removal',
    'Leaf Removal',
    'Lighting',
    'Locksmiths',
    'Marble & Granite',
    'Masonry',
    'Mattresses',
    'Misting Systems',
    'Mold Testing & Remediation',
    'Mortgage Companies',
    'Moving',
    'Mulch & Topsoil',
    'Oriental Rug Cleaning',
    'Painting',
    'Pest Control/Exterminating',
    'Piano Moving',
    'Piano Tuning',
    'Plastering',
    'Playground Equipment',
    'Plumbing',
    'Drain Cleaning',
    'Pool & Spa Service',
    'Pressure Washing',
    'Propane Sales/Services',
    'Property Management',
    'Radon Detection & Reduction',
    'Real Estate Agents',
    'Basements',
    'Remodeling',
    'Roof Cleaning',
    'Roofing',
    'Roto-Tilling',
    'Screen Repair',
    'Septic Tank',
    'Sewer Cleaning',
    'Siding',
    'Skylights',
    'Snow Removal',
    'Solar Panels',
    'Stone & Gravel',
    'Storage Facilities',
    'Structural Engineering',
    'Stucco',
    'Tablepads',
    'Title Companies',
    'Tree Service',
    'Upholstering',
    'Upholstery Cleaning',
    'Vacuum Cleaners',
    'Wallpaper Removal',
    'Wallpapering',
    'Water & Smoke Damage',
    'Water Heaters',
    'Water Softeners',
    'Wells & Pumps',
    'Window Cleaning',
    'Window Tinting',
    'Window Treatments',
    'Windows',
    'Wrought Iron',
]

@admin.register(Business)
class BusinessAdmin(admin.ModelAdmin):
    readonly_fields = ('date_created',)
    fieldsets = (
        (None, {'fields': ('user', 'stripe_account_id', 'name', 'logo', 'date_created')}),
    )
    list_display = ('name', 'user', 'logo', 'date_created')
    actions = [
        'create_fixtures',
    ]

    def create_fixtures(self, request, queryset):
        for business in queryset:
            for i in range(50):
                full_name = random.choice(FULL_NAMES)
                email_address = 'kouratester+%s@protonmail.com' % uuid4().hex[:5]
                Client.objects.create(
                    business=business,
                    full_name=full_name,
                    email_address=email_address
                )
            for i in range(7):
                with freeze_time(timezone.now() - timedelta(seconds=random.randint(1, 60 * 60 * 24 * 5))):
                    invoice = Invoice.objects.create(
                        business=business,
                        client=random.choice(Client.objects.filter(business=business)),
                    )
                for i in range(random.randint(1, 5)):
                    line_item = LineItem.objects.create(
                        description=random.choice(SERVICES),
                        amount=random.randint(10 * 100, 500 * 100),
                        order=i,
                        invoice=invoice
                    )
                invoice.application_fee = Invoice.get_application_fee(amount=invoice.amount)
                if not Invoice.objects.filter(stripe_charge_id='').count() <= 2:
                    invoice.stripe_charge_id = uuid4().hex
                invoice.save()
            for i in range(7):
                time_sheet = TimeSheet.objects.create(
                    business=business,
                    client=random.choice(Client.objects.filter(business=business)),
                    hourly_rate=random.randint(10 * 100, 100 * 100),
                    description=random.choice(SERVICES)
                )
                for i in range(random.randint(1, 10)):
                    lower = timezone.now() - timedelta(hours=24 * random.randint(0, 90))
                    upper = lower + timedelta(hours=random.randint(1, 5))
                    TimeSheetEntry.objects.create(
                        time_sheet=time_sheet,
                        time_range=DateTimeTZRange(lower=lower, upper=upper)
                    )
        self.message_user(request, '%d businesses were populated.' % queryset.count())
    create_fixtures.short_description = 'Populate businesses with fake data'

    
@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    readonly_fields = ('date_created', 'amount',)
    fieldsets = (
        (None, {'fields': ('business', 'client', 'date_created')}),
        (None, {'fields': ('stripe_charge_id', 'amount', 'application_fee', 'currency')}),
    )
    list_display = ('business', 'client', 'amount', 'application_fee', 'stripe_charge_id', 'date_created')

    
@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    readonly_fields = ('date_created',)
    fieldsets = (
        (None, {'fields': ('full_name', 'email_address', 'business', 'date_created')}),
    )
    list_display = ('full_name', 'email_address', 'business', 'date_created')


@admin.register(TimeSheet)
class TimeSheetAdmin(admin.ModelAdmin):
    readonly_fields = ('date_created', 'rounded_hours', 'amount', 'is_timing')
    fieldsets = (
        (None, {'fields': ('business', 'client', 'hourly_rate')}),
    )
    list_display = ('business', 'client', 'hourly_rate', 'rounded_hours', 'amount', 'is_timing', 'date_created')
