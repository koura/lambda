from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from datetime import timedelta

import stripe
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.gis.db import models
from django.contrib.postgres.fields import DateTimeRangeField
from django.core import signing
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import RegexValidator
from django.db.models import Sum
from django.utils import timezone
from psycopg2.extras import DateTimeTZRange

from koura.addresses.models import Address

User = get_user_model()
stripe.api_key = settings.STRIPE_SECRET_KEY


class AbstractModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Business(AbstractModel):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=False,
    )
    stripe_account_id = models.CharField(max_length=512)
    name = models.CharField(max_length=512, blank=False)
    logo = models.ImageField(upload_to='businesses/businesses/logos/', null=True)
    address = models.ForeignKey(Address)

    def __str__(self):
        return self.name

    def send_facebook_message(self, messages):
        self.user.send_facebook_message(messages)

    @property
    def has_external_accounts(self):
        if not self.stripe_account_id:
            return False
        stripe_account = stripe.Account.retrieve(self.stripe_account_id)
        if 'external_accounts' not in stripe_account:
            return False
        if 'data' not in stripe_account.get('external_accounts'):
            return False
        if len(stripe_account.get('external_accounts').get('data')) == 0:
            return False
        return True
    

class Client(AbstractModel):
    full_name = models.CharField(max_length=512, blank=False, validators=[RegexValidator('^[A-Za-z\s\.]+$')])
    email_address = models.EmailField(blank=False)
    business = models.ForeignKey('Business', related_name='clients')

    def __str__(self):
        return self.full_name

    @property
    def first_name(self):
        return self.full_name.split(' ')[0]
    
    class Meta:
        unique_together = ('email_address', 'business')


class InvoiceManager(models.Manager):
    def is_token_valid(self, token):
        return bool(self.from_token(token))

    def from_token(self, token):
        try:
            data = signing.loads(token, salt=self.model.get_salt())
        except signing.BadSignature:
            return None
        try:
            invoice = self.get(id=data.get('id'))
        except ObjectDoesNotExist:
            return None
        return invoice


class Invoice(AbstractModel):
    business = models.ForeignKey('Business', related_name='invoices')
    client = models.ForeignKey('Client', related_name='invoices')
    application_fee = models.IntegerField(null=True)
    currency = models.CharField(max_length=16, default='usd')
    stripe_charge_id = models.CharField(max_length=64, default='', blank=True)
    objects = InvoiceManager()

    @staticmethod
    def get_salt():
        return 'UM4k3anArwh6E3LvKSfBHt2yLbvPigZFzLN56hy7JJREQGKST6tGkc1eO5ueU3ti'

    @staticmethod
    def get_application_fee(amount):
        # Stripe fee is 2.9% + $0.30
        # Let's make our fee 3.25% + $0.30
        return round(amount * .0325) + 30

    @property
    def token(self):
        data = {
            'id': self.id
        }
        return signing.dumps(data, salt=self.get_salt())

    @property
    def amount(self):
        amount = self.line_items.all().aggregate(Sum('amount')).get('amount__sum')
        return amount if amount else 0
        
    @property
    def payout(self):
        assert self.amount
        assert self.application_fee
        return self.amount - self.application_fee

    @property
    def absolute_charge_url(self):
        from koura.urlresolvers import get_absolute_url
        from koura.businesses.enums import View
        return get_absolute_url(View.CHARGE_INVOICE.value, kwargs={'token': self.token})

    @property
    def has_time_sheet(self):
        return self.line_items.filter(time_sheet__isnull=False).exists()


class LineItem(AbstractModel):
    description = models.TextField(blank=False)
    amount = models.IntegerField()
    currency = models.CharField(max_length=16, default='usd')
    order = models.IntegerField()
    invoice = models.ForeignKey('Invoice', related_name='line_items')

    
class TimeSheet(AbstractModel):
    description = models.TextField(blank=False)
    hourly_rate = models.IntegerField()
    client = models.ForeignKey('Client', related_name='time_sheets')
    business = models.ForeignKey('Business', related_name='time_sheets')
    line_item = models.OneToOneField('LineItem', related_name='time_sheet', null=True)
    
    def start_timer(self):
        TimeSheetEntry.objects.create(
            time_sheet=self,
            time_range=DateTimeTZRange(lower=timezone.now())
        )
        self.save()
    
    def stop_timer(self):
        entry = self.latest_entry
        assert entry
        assert entry.time_range.lower
        assert not entry.time_range.upper
        entry.time_range = DateTimeTZRange(
            lower=entry.time_range.lower,
            upper=timezone.now()
        )
        entry.save()
        self.save()

    @property
    def rounded_hours(self):
        return round(self.timedelta.total_seconds() / 3600, 1)
        
    @property
    def timedelta(self):
        delta = timedelta(seconds=0)
        for entry in self.entries.all():
            x = entry.timedelta
            if x: 
                delta += x
        return delta    
    
    @property
    def is_invoiceable(self):
        if self.is_timing:
            return False
        # for entry in self.entries.all():
        #     if not entry.time_range.upper:
        #         return False
        return self.entries.all().count() > 0
    
    @property
    def latest_entry(self):
        entries = self.entries.all().order_by('-time_range')
        if not entries.exists():
            return None
        return entries[0]
    
    @property
    def is_timing(self):
        entry = self.latest_entry
        return bool(entry) and not bool(entry.time_range.upper)

    @property
    def amount(self):
        return self.hourly_rate * self.rounded_hours


class TimeSheetEntry(AbstractModel):
    time_range = DateTimeRangeField()
    time_sheet = models.ForeignKey('TimeSheet', related_name='entries')

    @property
    def timedelta(self):
        if not self.time_range.lower:
            return None
        if not self.time_range.upper:
            return timezone.now() - self.time_range.lower
        return self.time_range.upper - self.time_range.lower