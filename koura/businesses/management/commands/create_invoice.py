from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.urls import reverse
from faker import Faker

from koura.businesses.enums import View
from koura.businesses.factories import InvoiceFactory
from koura.businesses.forms import CreateBusinessForm
from koura.users.factories import UserFactory

fake = Faker()
User = get_user_model()


class Command(BaseCommand):
    def handle(self, *args, **options):
        user = UserFactory()
        form = CreateBusinessForm(data={
            'user': user.id,
            'name': fake.company(),
            'has_logo': 'Yes',
            'logo_url': 'https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png',
            'do_accept_terms_of_service': 'Yes',

            'street_address': fake.street_address(),
            'locality': fake.city(),
            'region': fake.state(),
            'postal_code': fake.zipcode(),
            'country': 'US',
        })
        form.is_valid()
        business = form.save()
        invoice = InvoiceFactory(business=business)
        self.stdout.write(reverse(View.CHARGE_INVOICE.value, kwargs={'token': invoice.token}))
