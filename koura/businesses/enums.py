from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from enum import Enum


class View(Enum):
    STRIPE_WEBHOOK = 'stripe_webhook'
    CONFIRM_INVOICE = 'confirm_invoice'
    CHARGE_INVOICE = 'charge_invoice'
    CHARGE_INVOICE_SUCCESS = 'charge_invoice_success'


class StripeWebhookType(Enum):
    CHARGE_SUCCEEDED = 'charge.succeeded'
