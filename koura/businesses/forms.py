from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
from datetime import date
from datetime import datetime
from datetime import timedelta
from uuid import uuid4

from botocore.exceptions import ClientError
import pytz
import re
import requests
import stripe
from django import forms
from django.conf import settings
from django.core import signing
from django.core.cache import cache
from django.core.files.base import ContentFile
from django.core.mail import EmailMultiAlternatives
from django.core.validators import RegexValidator
from django.db.models import Q
from django.template import loader
from django.utils.timezone import make_aware
from io import BytesIO
from rest_framework import status

from koura.addresses.forms import AddressForm
from koura.businesses.models import Business, Client, Invoice, LineItem, TimeSheet
from koura.forms import IntentFormMixin, YesNoField, StripeAmountField, ModelTokenField
from koura.pagination import Paginator

stripe.api_key = settings.STRIPE_SECRET_KEY
logger = logging.getLogger(__name__)


class LineItemsField(forms.Field):
    def clean(self, value):
        if not value:
            return []
        line_items = []
        for i, line in enumerate(value.strip().split('\n')):
            pattern = '^(.*) (\$?(?:(?:[1-9]\d{0,2}(?:,\d{3})*)|(?:(?:[1-9]\d*)?\d))(?:\.\d\d)?)$'
            match = re.fullmatch(pattern, line)
            if not match:
                raise forms.ValidationError('Invalid line item')
            description = match.group(1)
            amount = StripeAmountField().clean(match.group(2))
            line_items.append(LineItem(description=description, amount=amount, order=i))
        return line_items


class InvoiceFilterField(forms.TypedChoiceField):
    unpaid_choices = [
        'unpaid',
        'outstanding',
        'overdue',
        'past due',
        'over due'
    ]

    def coerce(self, x):
        return 'unpaid' if x in self.unpaid_choices else x

    def to_python(self, value):
        return super(InvoiceFilterField, self).to_python(value).lower()

    def __init__(self, *args, **kwargs):      
        choices = [
            ('all', 'all'), 
            ('paid', 'paid')
        ]
        [choices.append((c, c)) for c in self.unpaid_choices]
        super(InvoiceFilterField, self).__init__(choices=choices, coerce=self.coerce, *args, **kwargs)
    

class ChargeInvoiceForm(forms.Form):
    stripe_token = forms.CharField(widget=forms.HiddenInput)
    invoice = ModelTokenField(queryset=Invoice.objects.filter(stripe_charge_id__exact=''), salt=Invoice.get_salt())

    def submit(self):
        assert self.is_valid()
        invoice = self.cleaned_data.get('invoice')
        stripe_token = self.cleaned_data.get('stripe_token')
        stripe_charge = stripe.Charge.create(
            amount=invoice.amount,
            currency=invoice.currency,
            source=stripe_token,
            destination={
                'amount': invoice.payout,
                'account': invoice.business.stripe_account_id
            }
        )
        invoice.stripe_charge_id = stripe_charge.stripe_id
        invoice.save()
        
        # Send receipt
        context = {
            'invoice': invoice,
            'card_last_4': stripe_charge.get('source').get('last4'),
            'card_brand': stripe_charge.get('source').get('brand'),
            'charge_date': make_aware(datetime.fromtimestamp(stripe_charge.get('created')), timezone=pytz.utc)
        }
        subject = loader.render_to_string('businesses/emails/receipt_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('businesses/emails/receipt_body.txt', context)
        html_body = loader.render_to_string('businesses/emails/receipt_body.html', context)
        email_message = EmailMultiAlternatives(
            subject=subject, 
            body=body, 
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[invoice.client.email_address],
            bcc=[settings.SERVER_EMAIL]
        )
        email_message.attach_alternative(html_body, "text/html")
        email_message.send()
        
        return invoice

 
class RetrieveTimeSheetsForm(IntentFormMixin, forms.Form):
    business = forms.ModelChoiceField(queryset=Business.objects.all())

    @staticmethod
    def get_next_page_token(paginator, page_number, page_size, model_class, filter_kwargs=None, exclude_kwargs=None, ordering_args=None):
        page = paginator.page(page_number)
        if not page.has_next():
            return None
        payload = {
            'page_number': page.next_page_number(),
            'page_size': page_size,
            'model_class': model_class,
            'filter_kwargs': filter_kwargs,
            'exclude_kwargs': exclude_kwargs,
            'ordering_args': ordering_args
        }
        return signing.dumps(payload)

    def __init__(self, page_size, *args, **kwargs):
        self.page_size = page_size
        super(RetrieveTimeSheetsForm, self).__init__(*args, **kwargs)

    def retrieve(self):
        assert self.is_valid()
        business = self.cleaned_data.get('business')
        queryset = TimeSheet.objects.filter(business=business.id, line_item__isnull=True).order_by('-date_modified')
        paginator = Paginator(queryset, self.page_size)
        page = paginator.page(1)
        next_page_token = self.get_next_page_token(
            paginator=paginator,
            page_number=1,
            page_size=self.page_size,
            model_class='koura.businesses.models.TimeSheet',
            filter_kwargs={
                'business': business.id,
                'line_item__isnull': True
            },
            ordering_args=[
                '-date_modified'
            ]
        )
        return page.object_list, next_page_token
    
    def get_field_to_elicit(self):
        fields = []
        for field in fields:
            if field in self.errors:
                return field
        return None

        
class RetrieveInvoicesForm(IntentFormMixin, forms.Form):
    filter = InvoiceFilterField()
    business = forms.ModelChoiceField(queryset=Business.objects.all())
    
    def __init__(self, page_size, *args, **kwargs):
        self.page_size = page_size
        super(RetrieveInvoicesForm, self).__init__(*args, **kwargs)
    
    def retrieve(self):
        assert self.is_valid()
        business = self.cleaned_data.get('business')

        # Queryset
        queryset = Invoice.objects.filter(business=business.id).order_by('-date_created')
        if self.cleaned_data.get('filter') == 'paid':
            queryset = queryset.exclude(stripe_charge_id='')
        elif self.cleaned_data.get('filter') == 'unpaid':
            queryset = queryset.filter(stripe_charge_id='')

        # Paginate
        paginator = Paginator(queryset, self.page_size)
        page = paginator.page(1)
        next_page_token = paginator.get_page_token(2, 'koura.businesses.models.Invoice')
        logger.debug('queryset.count() = %d' % queryset.count())
        logger.debug('next_page_token = %s' % next_page_token)
        return page.object_list, next_page_token
    
    def is_fulfilled(self):
        return self.is_valid()

    def get_field_to_elicit(self):
        fields = [
            'filter',
        ]
        for field in fields:
            if field in self.errors:
                return field
        return None


class CreateBankPayoutForm(IntentFormMixin, forms.Form):
    date_of_birth = forms.DateField()
    tax_id = forms.CharField(validators=[RegexValidator('^(?:\d{2}-\d{7})|(?:\d{9})|(?:\d{3}-\d{2}-\d{4})$')]) # Could be EIN, SSN, etc.
    account_number = forms.CharField()
    routing_number = forms.CharField(validators=[RegexValidator('^\d{9}$')])
    ssn_last_four = forms.CharField(validators=[RegexValidator('^\d{4}$')])
    business = forms.ModelChoiceField(queryset=Business.objects.all())
    
    def clean_date_of_birth(self):
        date_of_birth = self.cleaned_data.get('date_of_birth')
        if not date_of_birth:
            return date_of_birth
        if date_of_birth > (date.today() - timedelta(days=18 * 365)):
            raise forms.ValidationError('You must be at least 18 years old')
        elif date_of_birth < (date.today() - timedelta(days=120 * 365)):
            raise forms.ValidationError('Invalid date')
        return date_of_birth
    
    def submit(self):
        assert self.is_valid()
        
        # User
        user = self.cleaned_data.get('business').user
        user.date_of_birth = self.cleaned_data.get('date_of_birth')
        user.save()
        
        # Stripe
        stripe_account = stripe.Account.retrieve(self.cleaned_data.get('business').stripe_account_id)
        stripe_account.legal_entity.dob = {
            'day': self.cleaned_data.get('date_of_birth').day,
            'month': self.cleaned_data.get('date_of_birth').month,
            'year': self.cleaned_data.get('date_of_birth').year
        }
        stripe_account.legal_entity.ssn_last_4 = self.cleaned_data.get('ssn_last_four')
        stripe_account.save()
        stripe_account.external_accounts.create(external_account={
             'object': 'bank_account',
             'account_number': self.cleaned_data.get('account_number'),
             'routing_number': self.cleaned_data.get('routing_number'),
             'country': 'US',
             'currency': 'usd',
        })

        return self.cleaned_data.get('business')

    def is_fulfilled(self):
        return self.is_valid()

    def get_field_to_elicit(self):
        fields = [
            'date_of_birth',
            'tax_id',
            'account_number',
            'routing_number',
            'ssn_last_four',
        ]
        for field in fields:
            if field in self.errors:
                return field
        return None


class CreateTimeSheetForm(IntentFormMixin, forms.ModelForm):
    hourly_rate = StripeAmountField()
    query = forms.CharField()

    def clean(self):
        cleaned_data = super(CreateTimeSheetForm, self).clean()
        query = cleaned_data.get('query')
        business = cleaned_data.get('business')
        client = cleaned_data.get('client')

        # Let's see if the query matches any clients
        if query and business:
            queryset = Client.objects.filter(business=business).filter(
                Q(email_address__icontains=query) |
                Q(full_name__icontains=query)
            )
            if not queryset.exists():
                raise forms.ValidationError({
                    'query': ['No clients found']
                })
        
        # Make sure business owns this client:
        if client and business:
            if client.business != business:
                raise forms.ValidationError({
                    'client': ['Invalid client']
                })

        return cleaned_data

    def get_field_to_elicit(self):
        fields = [
            'query',
            'client',
            'description',
            'hourly_rate',
        ]
        for field in fields:
            if field in self.errors:
                return field
        return None

    def is_fulfilled(self):
        return self.is_valid()

    class Meta:
        model = TimeSheet
        fields = [
            'business',
            'client',
            'hourly_rate',
            'description'
        ]

        
class CreateInvoiceForm(IntentFormMixin, forms.ModelForm):
    query = forms.CharField(required=False)
    do_confirm = YesNoField()
    do_elicit_line_items = YesNoField()
    line_items = LineItemsField(required=False)
    time_sheet = forms.ModelChoiceField(required=False, queryset=TimeSheet.objects.all())

    def clean(self):
        cleaned_data = super(CreateInvoiceForm, self).clean()
        query = cleaned_data.get('query')
        business = cleaned_data.get('business')
        client = cleaned_data.get('client')
        time_sheet = cleaned_data.get('time_sheet')
        line_items = cleaned_data.get('line_items')
        do_elicit_line_items = cleaned_data.get('do_elicit_line_items')

        # Let's see if the query matches any clients
        if query and business:
            queryset = Client.objects.filter(business=business).filter(
                Q(email_address__icontains=query) |
                Q(full_name__icontains=query)
            )
            if not queryset.exists():
                raise forms.ValidationError({
                    'query': ['No clients found']
                })
        
        # Query
        if not query and not time_sheet:
            raise forms.ValidationError({
                'query': ['This field is required']
            })

        # Make sure business owns this client:
        if client and business:
            if client.business != business:
                raise forms.ValidationError({
                    'client': ['Invalid client']
                })

        # Line items
        if not line_items and do_elicit_line_items:
            raise forms.ValidationError({
                'line_items': ['This field is required']
            })

        # Check time sheet
        if business and time_sheet:
            if business != time_sheet.business:
                raise forms.ValidationError({
                    'time_sheet': ['Invalid time sheet']
                })

        return cleaned_data

    def save(self, commit=True):
        # Create invoice
        invoice = super(CreateInvoiceForm, self).save(commit=True)

        # Time sheet
        order = 0
        time_sheet = self.cleaned_data.get('time_sheet')
        if time_sheet:
            line_item = LineItem.objects.create(
                description=time_sheet.description,
                amount=time_sheet.amount,
                order=order,
                invoice=invoice
            )
            time_sheet.line_item = line_item
            time_sheet.save()
            order += 1

        # Line items
        line_items = self.cleaned_data.get('line_items')
        for line_item in line_items:
            line_item.invoice = invoice
            line_item.order = order
            line_item.save()
            order += 1

        # Calculate application fee
        invoice.application_fee = Invoice.get_application_fee(amount=invoice.amount)
        invoice.save()
        
        # Send mail
        context = {
            'invoice': invoice
        }
        subject = loader.render_to_string('businesses/emails/invoice_creation_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('businesses/emails/invoice_creation_body.txt', context)
        html_body = loader.render_to_string('businesses/emails/invoice_creation_body.html', context)
        email_message = EmailMultiAlternatives(
            subject=subject, 
            body=body, 
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[invoice.client.email_address],
            bcc=[settings.SERVER_EMAIL]
        )
        email_message.attach_alternative(html_body, "text/html")
        email_message.send()

        return invoice

    def get_field_to_elicit(self):
        self.is_valid()
        time_sheet = self.cleaned_data['time_sheet']
        fields = [
            'query',
            'client',
            'do_elicit_line_items',
            'line_items',
            'do_confirm',
        ]
        for field in fields:
            if field in self.errors:
                if field in ['query', 'client'] and time_sheet:
                    continue
                return field
        return None

    def is_fulfilled(self):
        return self.is_valid() and self.cleaned_data.get('do_confirm', False)

    class Meta:
        model = Invoice
        fields = [
            'business',
            'client',
        ]


class CreateClientForm(IntentFormMixin, forms.ModelForm):
    def clean(self):
        cleaned_data = super(CreateClientForm, self).clean()
        if cleaned_data.get('email_address') and cleaned_data.get('business'):
            if Client.objects.filter(email_address=cleaned_data.get('email_address'), business=cleaned_data.get('business')).exists():
                raise forms.ValidationError({
                    'email_address': ['A client with this email address already exists.']
                })
        return cleaned_data

    def get_field_to_elicit(self):
        fields = [
            'email_address',
            'full_name'
        ]
        for field in fields:
            if field in self.errors:
                return field
        return None

    def is_fulfilled(self):
        return self.is_valid()

    class Meta:
        model = Client
        fields = [
            'full_name',
            'email_address',
            'business'
        ]


class CreateBusinessForm(IntentFormMixin, forms.ModelForm):
    has_logo = YesNoField(required=True)
    logo_url = forms.URLField(required=False)
    do_accept_terms_of_service = YesNoField(required=True)

    street_address = forms.CharField()
    locality = forms.CharField()
    region = forms.CharField()
    postal_code = forms.CharField()
    country = forms.CharField()

    _image_data_key = ''
    _image_type_key = ''

    def clean_logo_url(self):
        logo_url = self.cleaned_data.get('logo_url')
        if not logo_url:
            return logo_url

        validation_error = forms.ValidationError('Unfortunately, that is not a valid business logo.')
        response = requests.get(logo_url)

        # Check response code
        if response.status_code != status.HTTP_200_OK:
            raise validation_error

        self._image_data_key = '%s.%s.data' % (__name__, logo_url)
        self._image_type_key = '%s.%s.type' % (__name__, logo_url)

        # Verify this is an image
        image_data = cache.get(self._image_data_key)
        image_type = cache.get(self._image_type_key)
        if not image_data or not image_type:
            from PIL import Image

            image_data = response.content
            try:
                # load() could spot a truncated JPEG, but it loads the entire
                # image in memory, which is a DoS vector. See #3848 and #18520.
                image = Image.open(BytesIO(image_data))
                # verify() must be called immediately after the constructor.
                image.verify()
                image_type = Image.MIME.get(image.format)
            except Exception:
                # Pillow doesn't recognize it as an image.
                raise validation_error

            # What if image has no type...?
            if not image_type:
                raise validation_error

            cache.set(self._image_data_key, image_data, 300)
            cache.set(self._image_type_key, image_type, 300)

        return image_data

    def clean(self):
        cleaned_data = super(CreateBusinessForm, self).clean()

        # Check address
        cleaned_data['street_address_1'] = ''
        cleaned_data['street_address_2'] = ''
        if cleaned_data.get('street_address'):
            tokens = cleaned_data.get('street_address').split('\n')
            if len(tokens) > 2:
                raise forms.ValidationError({
                    'street_address': ['Invalid']
                })
            cleaned_data['street_address_1'] = tokens[0]
            if len(tokens) > 1:
                cleaned_data['street_address_2'] = tokens[1]
        address_form = AddressForm(data={
            'street_address_1': cleaned_data.get('street_address_1'),
            'street_address_2': cleaned_data.get('street_address_2'),
            'locality': cleaned_data.get('locality'),
            'region': cleaned_data.get('region'),
            'postal_code': cleaned_data.get('postal_code'),
            'country': cleaned_data.get('country'),
        })
        if address_form.errors:
            payload = {}
            for key, value in address_form.errors.as_data().items():
                key = '%s%s' % ('', key)
                payload[key] = [value[0].message]
            payload['street_address'] = payload.pop('street_address_1', []) + payload.pop('street_address_2', [])
            if not payload['street_address']:
                payload.pop('street_address')
            raise forms.ValidationError(payload)

        if cleaned_data.get('has_logo') and not cleaned_data.get('logo_url'):
            raise forms.ValidationError({
                'logo_url': ['Unfortunately, that is not a valid business logo.']
            })
        return cleaned_data

    def save(self, commit=True):
        business = super(CreateBusinessForm, self).save(commit=False)

        # Address
        address_form = AddressForm(data={
            'street_address_1': self.cleaned_data.get('street_address_1'),
            'street_address_2': self.cleaned_data.get('street_address_2'),
            'locality': self.cleaned_data.get('locality'),
            'region': self.cleaned_data.get('region'),
            'postal_code': self.cleaned_data.get('postal_code'),
            'country': self.cleaned_data.get('country'),
        })
        address_form.is_valid()
        address = address_form.save()
        business.address = address
        business.save()

        # Image
        if self.cleaned_data.get('logo_url'):
            image_type = cache.get(self._image_type_key)
            if image_type:
                tokens = image_type.split('/')
                image_type = tokens[-1]
            image_name = '%s.%s' % (uuid4().hex, image_type)
            try:
                business.logo.save(image_name, ContentFile(self.cleaned_data.get('logo_url')))
            except ClientError:
                logger.exception('Failed to connect to S3')

            business.save()

        # Stripe
        stripe_account = stripe.Account.create(
            type="custom",
            country="US",
            business_name=business.name,
        )
        business.stripe_account_id = stripe_account.id
        business.save()

        return business

    def get_field_to_elicit(self):
        fields = [
            'name',
            'street_address',
            'locality',
            'region',
            'postal_code',
            'country',
            'has_logo',
            'logo_url',
            'do_accept_terms_of_service'
        ]
        for field in fields:
            if field in self.errors:
                if field == 'logo_url' and not self.cleaned_data['has_logo']:
                    continue
                return field
        return None

    def is_fulfilled(self):
        return self.is_valid() and self.cleaned_data.get('do_accept_terms_of_service', False)

    class Meta:
        model = Business
        fields = [
            'user',
            'name'
        ]
