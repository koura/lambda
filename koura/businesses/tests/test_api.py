from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
from importlib import import_module
from unittest import mock
from uuid import uuid4

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from koura.businesses.enums import View
from koura.enums import Namespace

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


class StripeWebhookViewTestCase(APITestCase):
    view_name = '%s:%s' % (Namespace.BUSINESSES_API.value, View.STRIPE_WEBHOOK.value)

    @classmethod
    def setUpClass(cls):
        super(StripeWebhookViewTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(StripeWebhookViewTestCase, cls).tearDownClass()

    def setUp(self):
        self.patchers = [
            mock.patch('requests.post', mock.MagicMock(return_value={}))
        ]
        [p.start() for p in self.patchers]

        self.messenger_user_id = uuid4().hex
        payload = """
{
  "created": 1326853478,
  "livemode": false,
  "id": "evt_00000000000000",
  "type": "charge.succeeded",
  "object": "event",
  "request": null,
  "pending_webhooks": 1,
  "api_version": "2017-06-05",
  "data": {
    "object": {
      "id": "ch_00000000000000",
      "object": "charge",
      "amount": 93513,
      "amount_refunded": 0,
      "application": null,
      "application_fee": null,
      "balance_transaction": "txn_00000000000000",
      "captured": true,
      "created": 1498439673,
      "currency": "usd",
      "client": null,
      "description": null,
      "destination": "acct_1AYloCDyJMyAAxUs",
      "dispute": null,
      "failure_code": null,
      "failure_message": null,
      "fraud_details": {
      },
      "invoice": null,
      "livemode": false,
      "metadata": {
      },
      "on_behalf_of": "acct_1AYloCDyJMyAAxUs",
      "order": null,
      "outcome": {
        "network_status": "approved_by_network",
        "reason": null,
        "risk_level": "normal",
        "seller_message": "Payment complete.",
        "type": "authorized"
      },
      "paid": true,
      "receipt_email": null,
      "receipt_number": null,
      "refunded": false,
      "refunds": {
        "object": "list",
        "data": [
        ],
        "has_more": false,
        "total_count": 0,
        "url": "/v1/charges/ch_1AYloDDjTLW8eSgmyzuTyhgb/refunds"
      },
      "review": null,
      "shipping": null,
      "source": {
        "id": "card_00000000000000",
        "object": "card",
        "address_city": null,
        "address_country": null,
        "address_line1": null,
        "address_line1_check": null,
        "address_line2": null,
        "address_state": null,
        "address_zip": null,
        "address_zip_check": null,
        "brand": "Visa",
        "country": "US",
        "client": null,
        "cvc_check": null,
        "dynamic_last4": null,
        "exp_month": 8,
        "exp_year": 2019,
        "fingerprint": "hEvOPTNlgSr7Mdga",
        "funding": "credit",
        "last4": "4242",
        "metadata": {
        },
        "name": null,
        "tokenization_method": null
      },
      "source_transfer": null,
      "statement_descriptor": null,
      "status": "succeeded",
      "transfer": "tr_00000000000000",
      "transfer_group": "group_ch_1AYloDDjTLW8eSgmyzuTyhgb"
    }
  }
}
""".strip()
        self.data = json.loads(payload)

    def tearDown(self):
        [p.stop() for p in self.patchers]

    def test_webhook(self):
        data = self.data.copy()
        url = reverse(self.view_name)
        print(url)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
