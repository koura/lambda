from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from datetime import timedelta

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone
from psycopg2.extras import DateTimeTZRange

from koura.businesses.factories import TimeSheetFactory, TimeSheetEntryFactory, InvoiceFactory, LineItemFactory
from koura.businesses.models import Invoice, TimeSheetEntry, LineItem
from koura.businesses.serializers import LineItemSerializer

User = get_user_model()


class LineItemSerializerTestCase(TestCase):
    def tearDown(self):
        User.objects.all().delete()

    def test_time_sheet(self):
        invoice = InvoiceFactory()
        for line_item in invoice.line_items.all():
            line_item.order += 1
            line_item.save()
        business = invoice.business
        client = invoice.client
        time_sheet = TimeSheetFactory(business=business, client=client)
        [TimeSheetEntryFactory(time_sheet=time_sheet) for i in range(10)]
        line_item = LineItem(
            invoice=invoice,
            time_sheet=time_sheet,
            description=time_sheet.description,
            amount=time_sheet.amount,
            order=0
        )
        data = LineItemSerializer(line_item).data
        self.assertTrue('time_sheet' in data)
        self.assertTrue('rounded_hours' in data.get('time_sheet'))
        self.assertTrue('hourly_rate' in data.get('time_sheet'))
        
        serializer = LineItemSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        line_item = serializer.validated_data
        self.assertTrue('time_sheet' in line_item)
        self.assertTrue('hourly_rate' in line_item.get('time_sheet'))
        self.assertTrue('rounded_hours' in line_item.get('time_sheet'))
