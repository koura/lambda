from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from importlib import import_module
from string import Template

import boto3
import copy
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sessions.models import Session
from django.test import TestCase, override_settings

UNAUTHENTICATED_ASSUME_ROLE_POLICY = Template("""
{
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Federated": "cognito-identity.amazonaws.com"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "cognito-identity.amazonaws.com:aud": "$identity_pool_id"
        },
        "ForAnyValue:StringLike": {
          "cognito-identity.amazonaws.com:amr": "unauthenticated"
        }
      }
    }
  ]
}
""".strip())

AUTHENTICATED_ASSUME_ROLE_POLICY = Template("""
{
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Federated": "cognito-identity.amazonaws.com"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "cognito-identity.amazonaws.com:aud": "$identity_pool_id"
        },
        "ForAnyValue:StringLike": {
          "cognito-identity.amazonaws.com:amr": "authenticated"
        }
      }
    }
  ]
}
""".strip())

ROLE_POLICY = """
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": [
        "cognito-sync:*",
        "cognito-identity:*"
      ],
      "Resource": "*"
    }
  ]
}
""".strip()

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


@override_settings(
    AWS_COGNITO_IDENTITY_POOL_NAME='test' + uuid.uuid4().hex,
    AWS_COGNITO_DEVELOPER_PROVIDER_NAME='test.testbot.com',
)
class IntentTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(IntentTestCase, cls).setUpClass()
        cls._unauthenticated_role_name = 'test' + uuid.uuid4().hex
        cls._policy_name = 'test' + uuid.uuid4().hex
        cls._authenticated_role_name = 'test' + uuid.uuid4().hex
        cls._identity_pool_id = None

        cognito_client = boto3.client('cognito-identity')
        response = cognito_client.create_identity_pool(
            IdentityPoolName=settings.AWS_COGNITO_IDENTITY_POOL_NAME,
            AllowUnauthenticatedIdentities=True,
            DeveloperProviderName=settings.AWS_COGNITO_DEVELOPER_PROVIDER_NAME,
        )
        cls._identity_pool_id = response.get('IdentityPoolId')

        # Attach role policies
        iam_client = boto3.client('iam')
        unauthenticated_role = iam_client.create_role(
            RoleName=cls._unauthenticated_role_name,
            AssumeRolePolicyDocument=UNAUTHENTICATED_ASSUME_ROLE_POLICY.substitute({
                'identity_pool_id': cls._identity_pool_id
            })
        ).get('Role')
        authenticated_role = iam_client.create_role(
            RoleName=cls._authenticated_role_name,
            AssumeRolePolicyDocument=AUTHENTICATED_ASSUME_ROLE_POLICY.substitute({
                'identity_pool_id': cls._identity_pool_id
            })
        ).get('Role')
        iam_client.put_role_policy(
            RoleName=cls._unauthenticated_role_name,
            PolicyName=cls._policy_name,
            PolicyDocument=ROLE_POLICY,

        )
        iam_client.put_role_policy(
            RoleName=cls._authenticated_role_name,
            PolicyName=cls._policy_name,
            PolicyDocument=ROLE_POLICY,

        )

        cognito_client.set_identity_pool_roles(
            IdentityPoolId=cls._identity_pool_id,
            Roles={
                'unauthenticated': unauthenticated_role.get('Arn'),
                'authenticated': authenticated_role.get('Arn')
            }
        )

    @classmethod
    def tearDownClass(cls):
        super(IntentTestCase, cls).tearDownClass()
        cognito_client = boto3.client('cognito-identity')
        cognito_client.delete_identity_pool(
            IdentityPoolId=cls._identity_pool_id
        )
        iam_client = boto3.client('iam')
        iam_client.delete_role_policy(RoleName=cls._unauthenticated_role_name, PolicyName=cls._policy_name)
        iam_client.delete_role(RoleName=cls._unauthenticated_role_name)
        iam_client.delete_role_policy(RoleName=cls._authenticated_role_name, PolicyName=cls._policy_name)
        iam_client.delete_role(RoleName=cls._authenticated_role_name)

    def assertCognitoIdentityExists(self, user):
        client = boto3.client('cognito-identity')
        response = client.describe_identity(
            IdentityId=user.cognito_identity_id,
        )
        self.assertEqual(user.cognito_identity_id, response.get('IdentityId'))
        del response['CreationDate']
        del response['LastModifiedDate']

    def assertSessionAttributes(self, response):
        self.assertIsNotNone(response.get('sessionAttributes').get('key'))
        self.assertTrue(Session.objects.filter(session_key=response.get('sessionAttributes').get('key')).exists())

    def assertSessionAttributesContainAwsCredentials(self, response):
        self.assertIsNotNone(response.get('sessionAttributes'))
        self.assertIsNotNone(response.get('sessionAttributes').get('openIdToken'))

    def assertResponseEqual(self, actual, expected):
        x = copy.deepcopy(actual)
        y = copy.deepcopy(expected)
        try:
            del y['dialogAction']['message']
        except Exception:
            pass
        try:
            del y['sessionAttributes']
        except Exception:
            pass
        try:
            del x['dialogAction']['message']
        except Exception:
            pass
        try:
            del x['sessionAttributes']
        except Exception:
            pass
        self.assertEqual(x, y)
