from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from unittest import skip
from uuid import uuid4

import os
import random
import stripe
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import mail
from django.test import TestCase
from datetime import timedelta

from koura.businesses.factories import BusinessFactory, ClientFactory, InvoiceFactory, TimeSheetFactory, TimeSheetEntryFactory
from koura.businesses.forms import CreateBusinessForm, CreateClientForm, CreateInvoiceForm, CreateBankPayoutForm, \
    ChargeInvoiceForm, RetrieveInvoicesForm, CreateTimeSheetForm, RetrieveTimeSheetsForm
from koura.businesses.models import Business, Client, Invoice, TimeSheet
from koura.users.factories import UserFactory
from django.utils import timezone
from psycopg2.extras import DateTimeTZRange

stripe.api_key = settings.STRIPE_SECRET_KEY
DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')
FIXTURES_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'fixtures')

User = get_user_model()


class CreateTimeSheetFormTestCase(TestCase):
    def setUp(self):
        self.client = ClientFactory()
        self.business = self.client.business
        self.data = {
            'business': self.business.id,
            'client': self.client.id,
            'query': self.client.full_name,
            'description': 'I write elite codez',
            'hourly_rate': '$900'
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_init(self):
        form = CreateTimeSheetForm()
        self.assertIsNotNone(form)

    def test_create_timesheet(self):
        data = self.data.copy()
        form = CreateTimeSheetForm(data=data)
        self.assertTrue(form.is_valid())
        time_sheet = form.save()
        self.assertIsNotNone(time_sheet)
        self.assertEqual(TimeSheet.objects.all().count(), 1)
        self.assertTrue(TimeSheet.objects.filter(business=self.business, client=self.client).exists())
        self.assertEqual(time_sheet.hourly_rate, 900 * 100)
        self.assertEqual(time_sheet.description, data.get('description'))
    
    def test_elicit_query(self):
        data = {
            'business': self.business.id
        }
        form = CreateTimeSheetForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'query')

    def test_elicit_client(self):
        data = {
            'query': self.client.full_name,
            'business': self.business.id
        }
        form = CreateTimeSheetForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'client')
    
    def test_elicit_description(self):
        data = {
            'query': self.client.full_name,
            'business': self.business.id,
            'client': self.client.id
        }
        form = CreateTimeSheetForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'description')
    
    def test_elicit_hourly_rate(self):
        data = {
            'query': self.client.full_name,
            'business': self.business.id,
            'client': self.client.id,
            'description': 'feewaefwfewefw',
        }
        form = CreateTimeSheetForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'hourly_rate')

    def test_elicit_nothing(self):
        data = self.data.copy()
        form = CreateTimeSheetForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertIsNone(form.get_field_to_elicit())
    
    def test_invalid_client(self):
        business = BusinessFactory()
        client = ClientFactory(business=business)
        data = self.data.copy()
        data['client'] = client.id
        form = CreateTimeSheetForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('client' in form.errors)
        business.user.delete()

    def test_invalid_query(self):
        data = self.data.copy()
        data['query'] = 'fdafasdfwefaweawfjiawefji'
        form = CreateTimeSheetForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('query' in form.errors)

class RetrieveTimeSheetsFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(RetrieveTimeSheetsFormTestCase, cls).setUpClass()
        business = BusinessFactory()
        client = ClientFactory(business=business)
        for i in range(20):
            TimeSheetFactory(business=business, client=client)

    @classmethod
    def tearDownClass(cls):
        super(RetrieveTimeSheetsFormTestCase, cls).tearDownClass()
        User.objects.all().delete()

    def setUp(self):
        self.business = Business.objects.all()[0]
        self.client = Client.objects.all()[0]
        self.data = {
            'business': self.business.id,
        }

    def test_init(self):
        form = RetrieveTimeSheetsForm(page_size=9)
        self.assertIsNotNone(form)
    
    def test_retrieve_time_sheets(self):
        data = self.data.copy()
        form = RetrieveTimeSheetsForm(data=data, page_size=9)
        self.assertTrue(form.is_valid())
        time_sheets, next_page_token = form.retrieve()
        self.assertIsNotNone(time_sheets)
        self.assertLessEqual(len(time_sheets), 9)
        self.assertIsNotNone(next_page_token)

    def test_elicit_nothing(self):
        data = self.data.copy()
        form = RetrieveTimeSheetsForm(data=data, page_size=10)
        self.assertTrue(form.is_valid())
        self.assertIsNone(form.get_field_to_elicit())


class RetrieveInvoicesFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(RetrieveInvoicesFormTestCase, cls).setUpClass()
        business = BusinessFactory()
        client = ClientFactory(business=business)
        for i in range(50):
            InvoiceFactory(business=business, client=client, stripe_charge_id=random.choice(['', uuid4().hex]))

    @classmethod
    def tearDownClass(cls):
        super(RetrieveInvoicesFormTestCase, cls).tearDownClass()
        User.objects.all().delete()

    def setUp(self):
        self.business = Business.objects.all()[0]
        self.data = {
            'filter': 'all',
            'business': self.business.id,
        }

    def test_init(self):
        form = RetrieveInvoicesForm(page_size=10)
        self.assertIsNotNone(form)
    
    def test_retrieve_invoices(self):
        data = self.data.copy()
        form = RetrieveInvoicesForm(data=data, page_size=10)
        self.assertTrue(form.is_valid())
        invoices, next_page_token = form.retrieve()
        self.assertIsNotNone(invoices)
        self.assertIsNotNone(next_page_token)
        self.assertLessEqual(len(invoices), 10)

    def test_case_insensitive(self):
        self.assertRetrieveFilteredInvoices('PaiD')

    def test_retrieve_paid_invoices(self):
        self.assertRetrieveFilteredInvoices('paid')

    def test_retrieve_unpaid_invoices(self):
        self.assertRetrieveFilteredInvoices('unpaid')

    def test_retrieve_past_due_invoices(self):
        self.assertRetrieveFilteredInvoices('past due')

    def test_retrieve_overdue_invoices(self):
        self.assertRetrieveFilteredInvoices('overdue')
        self.assertRetrieveFilteredInvoices('over due')

    def test_retrieve_outstanding_invoices(self):
        self.assertRetrieveFilteredInvoices('outstanding')

    def test_elicit_filter(self):
        data = self.data.copy()
        data.pop('filter')
        form = RetrieveInvoicesForm(data=data, page_size=10)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'filter')
    
    def test_elicit_nothing(self):
        data = self.data.copy()
        data.pop('filter')
        form = RetrieveInvoicesForm(data=data, page_size=10)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'filter')

    def assertRetrieveFilteredInvoices(self, filter):
        data = self.data.copy()
        data['filter'] = filter
        form = RetrieveInvoicesForm(data=data, page_size=10)
        self.assertTrue(form.is_valid())
        invoices, next_page_token = form.retrieve()
        self.assertIsNotNone(invoices)
        if filter.lower() == 'paid':
            for invoice in invoices:
                self.assertNotEqual(invoice.stripe_charge_id, '')
        else:
            for invoice in invoices:
                self.assertEqual(invoice.stripe_charge_id, '')
    

class ChargeInvoiceFormTestCase(TestCase):
    def setUp(self):
        self.user = UserFactory()
        form = CreateBusinessForm(data={
            'user': self.user.id,
            'name': uuid4().hex,
            'has_logo': 'Yes',
            'logo_url': 'https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png',
            'do_accept_terms_of_service': 'Yes',

            'street_address': '221B Baker Street',
            'locality': 'London',
            'region': 'California',
            'postal_code': '20444',
            'country': 'US',
        })
        self.assertTrue(form.is_valid())
        self.business = form.save()
        self.invoice = InvoiceFactory(business=self.business)
        self.data = {
            'stripe_token': 'tok_visa',
            'invoice': self.invoice.token,
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_init(self):
        form = ChargeInvoiceForm()
        self.assertIsNotNone(form)

    def test_invalid_invoice(self):
        data = self.data.copy()
        data['invoice'] = 'fdsafasfdasfasdfasdafs'
        form = ChargeInvoiceForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('invoice' in form.errors)

    def test_invoice_already_paid(self):
        self.invoice.stripe_charge_id = '12345'
        self.invoice.save()
        data = self.data.copy()
        form = ChargeInvoiceForm(data=data)
        self.assertFalse(form.is_valid())

    def test_checkout(self):
        data = self.data.copy()
        form = ChargeInvoiceForm(data=data)
        self.assertTrue(form.is_valid())
        invoice = form.submit()
        self.assertTrue(invoice.stripe_charge_id)
        stripe_charge = stripe.Charge.retrieve(invoice.stripe_charge_id)
        self.assertEqual(invoice.amount, stripe_charge.amount)
        self.assertEqual(invoice.currency, stripe_charge.currency)
        self.assertEqual(invoice.business.stripe_account_id, stripe_charge.destination)
        stripe_transfer = stripe.Transfer.retrieve(stripe_charge.transfer)
        self.assertEqual(invoice.payout, stripe_transfer.amount)

    def test_receipt(self):
        data = self.data.copy()
        form = ChargeInvoiceForm(data=data)
        self.assertTrue(form.is_valid())
        invoice = form.submit()
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].from_email, DEFAULT_FROM_EMAIL)
        self.assertEqual(mail.outbox[0].to, [invoice.client.email_address])
        self.assertEqual(mail.outbox[0].bcc, [SERVER_EMAIL])

        
class CreateBankPayoutFormTestCase(TestCase):
    def setUp(self):
        self.user = UserFactory()
        form = CreateBusinessForm(data={
            'user': self.user.id,
            'name': uuid4().hex,
            'has_logo': 'Yes',
            'logo_url': 'https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png',
            'do_accept_terms_of_service': 'Yes',

            'street_address': '221B Baker Street',
            'locality': 'London',
            'region': 'California',
            'postal_code': '20444',
            'country': 'US',
        })
        self.assertTrue(form.is_valid())
        self.business = form.save()
        self.data = {
            'business': self.business.id,
            'date_of_birth': '8/23/86',
            'tax_id': '11-1111111',
            'account_number': '000123456789',
            'routing_number': '110000000',
            'ssn_last_four': '1453',
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_init(self):
        form = CreateBankPayoutForm()
        self.assertIsNotNone(form)

    def test_create_bank_payout(self):
        data = self.data.copy()
        form = CreateBankPayoutForm(data=data)
        self.assertTrue(form.is_valid())
        business = form.submit()
        
        # Stripe
        stripe_account = stripe.Account.retrieve(business.stripe_account_id)
        self.assertIsNotNone(stripe_account)
        self.assertEqual(form.cleaned_data.get('date_of_birth').day, stripe_account.legal_entity.dob.day)
        self.assertEqual(form.cleaned_data.get('date_of_birth').month, stripe_account.legal_entity.dob.month)
        self.assertEqual(form.cleaned_data.get('date_of_birth').year, stripe_account.legal_entity.dob.year)
        self.assertTrue(stripe_account.legal_entity.ssn_last_4_provided)
        self.assertEqual(form.cleaned_data.get('routing_number'), stripe_account.external_accounts.data[0].routing_number)

    def test_invalid_date_of_birth_1(self):
        data = self.data.copy()
        data['date_of_birth'] = '9/1/2005'
        form = CreateBankPayoutForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('date_of_birth' in form.errors)

    def test_invalid_date_of_birth_2(self):
        data = self.data.copy()
        data['date_of_birth'] = '9/1/2048'
        form = CreateBankPayoutForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('date_of_birth' in form.errors)

    def test_invalid_date_of_birth_3(self):
        data = self.data.copy()
        data['date_of_birth'] = '209'
        form = CreateBankPayoutForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('date_of_birth' in form.errors)

    def test_invalid_date_of_birth_4(self):
        data = self.data.copy()
        data['date_of_birth'] = '1986'
        form = CreateBankPayoutForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('date_of_birth' in form.errors)

    def test_invalid_tax_id(self):
        data = self.data.copy()
        data['tax_id'] = 'abc'
        form = CreateBankPayoutForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('tax_id' in form.errors)

    def test_invalid_routing_number(self):
        data = self.data.copy()
        data['routing_number'] = 'abc'
        form = CreateBankPayoutForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('routing_number' in form.errors)
        
    def test_invalid_ssn_last_four(self):
        data = self.data.copy()
        data['ssn_last_four'] = 'abc'
        form = CreateBankPayoutForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('ssn_last_four' in form.errors)

    def test_elicit_date_of_birth(self):
        data = self.data.copy()
        for k in ['date_of_birth', 'tax_id', 'account_number', 'routing_number', 'ssn_last_four']:
            data.pop(k)
        form = CreateBankPayoutForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'date_of_birth')

    def test_elicit_tax_id(self):
        data = self.data.copy()
        for k in ['tax_id', 'account_number', 'routing_number', 'ssn_last_four']:
            data.pop(k)
        form = CreateBankPayoutForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'tax_id')

    def test_elicit_account_number(self):
        data = self.data.copy()
        for k in ['account_number', 'routing_number', 'ssn_last_four']:
            data.pop(k)
        form = CreateBankPayoutForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'account_number')

    def test_elicit_routing_number(self):
        data = self.data.copy()
        for k in ['routing_number', 'ssn_last_four']:
            data.pop(k)
        form = CreateBankPayoutForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'routing_number')

    def test_elicit_ssn_last_four(self):
        data = self.data.copy()
        for k in ['ssn_last_four']:
            data.pop(k)
        form = CreateBankPayoutForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'ssn_last_four')

    def test_elicit_nothing(self):
        data = self.data.copy()
        form = CreateBankPayoutForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertIsNone(form.get_field_to_elicit())

        
class CreateInvoiceFormTestCase(TestCase):
    def setUp(self):
        self.client = ClientFactory()
        self.business = self.client.business
        self.data = {
            'business': self.business.id,
            'client': self.client.id,
            'query': self.client.full_name,
            'do_confirm': 'Yes',
            'do_elicit_line_items': 'Yes',
            'line_items': 'Housing $50\nParts $30\nA really juicy steak $400'
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_init(self):
        form = CreateInvoiceForm()
        self.assertIsNotNone(form)
    
    def test_invalid_client(self):
        business = BusinessFactory()
        client = ClientFactory(business=business)
        data = self.data.copy()
        data['client'] = client.id
        form = CreateInvoiceForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('client' in form.errors)
        business.user.delete()

    def test_create_invoice(self):
        data = self.data.copy()
        form = CreateInvoiceForm(data=data)
        self.assertTrue(form.is_valid())
        invoice = form.save()
        self.assertIsNotNone(invoice)
        self.assertEqual(Invoice.objects.all().count(), 1)
        self.assertTrue(Invoice.objects.filter(business=self.business, client=self.client).exists())
        self.assertEqual(invoice.amount, 48000)
        self.assertEqual(invoice.application_fee, 1590)
        self.assertEqual(invoice.line_items.all().count(), 3)

    def test_create_invoice_with_time_sheet(self):
        time_sheet = TimeSheetFactory(business=self.business, client=self.client, hourly_rate=50 * 100)
        for i in range(10):
            TimeSheetEntryFactory(
                time_sheet=time_sheet,
                time_range=DateTimeTZRange(lower=timezone.now() - timedelta(hours=i*24*30), upper=timezone.now() - timedelta(hours=i*24*30 - 5))
            )
        data = self.data.copy()
        data['do_elicit_line_items'] = 'yes'
        data['time_sheet'] = time_sheet.id
        form = CreateInvoiceForm(data=data)
        self.assertTrue(form.is_valid())
        invoice = form.save()
        self.assertIsNotNone(invoice)
        self.assertEqual(Invoice.objects.all().count(), 1)
        self.assertTrue(Invoice.objects.filter(business=self.business, client=self.client).exists())
        self.assertEqual(invoice.amount, 298000)
        self.assertEqual(invoice.application_fee, 9715)
        self.assertEqual(invoice.line_items.all().count(), 4)

    def test_create_invoice_with_time_sheet_only(self):
        time_sheet = TimeSheetFactory(business=self.business, client=self.client, hourly_rate=50 * 100)
        for i in range(10):
            lower = timezone.now() - timedelta(hours=i*24*30)
            upper = lower + timedelta(hours=1)
            TimeSheetEntryFactory(
                time_sheet=time_sheet,
                time_range=DateTimeTZRange(lower=lower, upper=upper)
            )
        data = self.data.copy()
        data.pop('line_items')
        data['do_elicit_line_items'] = 'no'
        data['time_sheet'] = time_sheet.id
        form = CreateInvoiceForm(data=data)
        self.assertTrue(form.is_valid())
        invoice = form.save()
        self.assertIsNotNone(invoice)
        self.assertEqual(Invoice.objects.all().count(), 1)
        self.assertTrue(Invoice.objects.filter(business=self.business, client=self.client).exists())
        self.assertEqual(invoice.amount, 50 * 10 * 100)
        self.assertEqual(invoice.application_fee, 50 * 10 * .0325 * 100 + 30)
        self.assertEqual(invoice.line_items.all().count(), 1)

    def test_invalid_time_sheet(self):
        time_sheet = TimeSheetFactory()
        for i in range(1, 10):
            TimeSheetEntryFactory(
                time_sheet=time_sheet,
                time_range=DateTimeTZRange(lower=timezone.now() - timedelta(hours=i * 24 * 30), upper=timezone.now() - timedelta(hours=i * 24 * 30 - 5))
            )
        data = self.data.copy()
        data['time_sheet'] = time_sheet.id
        form = CreateInvoiceForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('time_sheet' in form.errors)

    def test_invalid_line_items_1(self):
        data = self.data.copy()
        data['line_items'] = 'A really juicy steak\nA box of soap'
        form = CreateInvoiceForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('line_items' in form.errors)

    @skip('Meh')
    def test_invalid_line_items_2(self):
        data = self.data.copy()
        data['line_items'] = '5 Hours Labor $50\nParts $30\A really juicy steak $400'
        form = CreateInvoiceForm(data=data)
        self.assertFalse(form.is_valid())

    def test_invalid_query(self):
        data = self.data.copy()
        data['query'] = 'fdafasdfwefaweawfjiawefji'
        form = CreateInvoiceForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('query' in form.errors)

    def test_elicit_query(self):
        data = {
            'business': self.business.id
        }
        form = CreateInvoiceForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'query')

    def test_elicit_client(self):
        data = {
            'query': self.client.full_name,
            'business': self.business.id
        }
        form = CreateInvoiceForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'client')

    def test_elicit_line_items(self):
        data = {
            'query': self.client.full_name,
            'client': self.client.id,
            'business': self.business.id,
            'do_elicit_line_items': 'Yes'
        }
        form = CreateInvoiceForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'line_items')

    def test_elicit_do_confirm(self):
        data = {
            'query': self.client.full_name,
            'client': self.client.id,
            'business': self.business.id,
            'line_items': '5 Hours Labor $50\nParts $30\nEmotional stress $400',
            'do_elicit_line_items': 'Yes'
        }
        form = CreateInvoiceForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'do_confirm')

    def test_elicit_nothing(self):
        data = self.data.copy()
        form = CreateInvoiceForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), None)

    def test_elicit_do_elicit_line_items(self):
        time_sheet = TimeSheetFactory(business=self.business, client=self.client, hourly_rate=50 * 100)
        for i in range(10):
            TimeSheetEntryFactory(
                time_sheet=time_sheet,
                time_range=DateTimeTZRange(lower=timezone.now() - timedelta(hours=i*24*30), upper=timezone.now() - timedelta(hours=i*24*30 - 5))
            )
        data = self.data.copy()
        data['time_sheet'] = time_sheet.id
        [data.pop(k) for k in ['query', 'do_confirm', 'line_items', 'do_elicit_line_items']]
        form = CreateInvoiceForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'do_elicit_line_items')

    def test_elicit_line_items_1(self):
        time_sheet = TimeSheetFactory(business=self.business, client=self.client, hourly_rate=50 * 100)
        for i in range(10):
            TimeSheetEntryFactory(
                time_sheet=time_sheet,
                time_range=DateTimeTZRange(lower=timezone.now() - timedelta(hours=i*24*30), upper=timezone.now() - timedelta(hours=i*24*30 - 5))
            )
        data = self.data.copy()
        data['time_sheet'] = time_sheet.id
        data['do_elicit_line_items'] = 'No'
        [data.pop(k) for k in ['query', 'do_confirm', 'line_items']]
        form = CreateInvoiceForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'do_confirm')

    def test_do_not_elicit_line_items(self):
        time_sheet = TimeSheetFactory(business=self.business, client=self.client, hourly_rate=50 * 100)
        for i in range(10):
            TimeSheetEntryFactory(
                time_sheet=time_sheet,
                time_range=DateTimeTZRange(lower=timezone.now() - timedelta(hours=i*24*30), upper=timezone.now() - timedelta(hours=i*24*30 - 5))
            )
        data = self.data.copy()
        data['time_sheet'] = time_sheet.id
        data['do_elicit_line_items'] = 'No'
        [data.pop(k) for k in ['query', 'do_confirm', 'line_items']]
        form = CreateInvoiceForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'do_confirm')

    def test_is_fulfilled(self):
        data = self.data.copy()
        form = CreateInvoiceForm(data=data)
        self.assertTrue(form.is_fulfilled())

    def test_is_not_fulfilled(self):
        data = self.data.copy()
        data['do_confirm'] = 'No'
        form = CreateInvoiceForm(data=data)
        self.assertFalse(form.is_fulfilled())
    
    def test_email(self):
        data = self.data.copy()
        form = CreateInvoiceForm(data=data)
        self.assertTrue(form.is_valid())
        invoice = form.save()
        self.assertIsNotNone(invoice)
        self.assertEqual(Invoice.objects.all().count(), 1)
        self.assertTrue(Invoice.objects.filter(business=self.business, client=self.client).exists())
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].from_email, DEFAULT_FROM_EMAIL)
        self.assertEqual(mail.outbox[0].to, [invoice.client.email_address])
        self.assertEqual(mail.outbox[0].bcc, [SERVER_EMAIL])


class CreateClientFormTestCase(TestCase):
    def setUp(self):
        self.business = BusinessFactory()
        self.data = {
            'full_name': 'Chukwuemeka Ezekwe',
            'email_address': 'emeka@getkoura.com',
            'business': self.business.id
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_init(self):
        form = CreateClientForm()
        self.assertIsNotNone(form)

    def test_create_client(self):
        data = self.data.copy()
        form = CreateClientForm(data=data)
        self.assertTrue(form.is_valid())
        client = form.save()
        self.assertIsNotNone(client)
        self.assertEqual(Client.objects.all().count(), 1)
        self.assertTrue(Client.objects.filter(business=self.business, email_address=data.get('email_address')).exists())

    def test_duplicate_email_address(self):
        data = self.data.copy()
        form = CreateClientForm(data=data)
        self.assertTrue(form.is_valid())
        form.save()

        data = self.data.copy()
        form = CreateClientForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('email_address' in form.errors)

    def test_elicit_full_name(self):
        data = self.data.copy()
        del data['full_name']
        form = CreateClientForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'full_name')

    def test_elicit_email_address_for_duplicate(self):
        data = self.data.copy()
        form = CreateClientForm(data=data)
        self.assertTrue(form.is_valid())
        form.save()

        data = self.data.copy()
        form = CreateClientForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('email_address' in form.errors)
        self.assertEqual(form.get_field_to_elicit(), 'email_address')

    def test_invalid_full_name(self):
        data = self.data.copy()
        data['full_name'] = '24aef423aaw!@#@'
        form = CreateClientForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('full_name' in form.errors)

    def test_elicit_email_address_after_error(self):
        data = self.data.copy()
        data['email_address'] = '24aef423aaw!@#@'
        form = CreateClientForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('email_address' in form.errors)
        self.assertEqual(form.get_field_to_elicit(), 'email_address')


class CreateBusinessFormTestCase(TestCase):
    def setUp(self):
        self.user = UserFactory()
        self.data = {
            'user': self.user.id,
            'name': uuid4().hex,
            'has_logo': 'Yes',
            'logo_url': 'https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png',
            'do_accept_terms_of_service': 'Yes',

            'street_address': '221B Baker Street',
            'locality': 'London',
            'region': 'California',
            'postal_code': '20444',
            'country': 'US',
        }

    def tearDown(self):
        User.objects.all().delete()
        Business.objects.all().delete()
        Client.objects.all().delete()

    def test_init(self):
        form = CreateBusinessForm()
        self.assertIsNotNone(form)

    def test_elicit_name(self):
        data = {
            'user': self.user.id
        }
        form = CreateBusinessForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'name')

    def test_elicit_street_address(self):
        data = {
            'user': self.user.id,
            'name': uuid4().hex
        }
        form = CreateBusinessForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'street_address')

    def test_elicit_locality(self):
        data = {
            'user': self.user.id,
            'name': uuid4().hex,
            'street_address': uuid4().hex
        }
        form = CreateBusinessForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'locality')

    def test_elicit_region(self):
        data = {
            'user': self.user.id,
            'name': uuid4().hex,
            'street_address': uuid4().hex,
            'locality': uuid4().hex
        }
        form = CreateBusinessForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'region')

    def test_elicit_postal_code(self):
        data = {
            'user': self.user.id,
            'name': uuid4().hex,
            'street_address': uuid4().hex,
            'locality': uuid4().hex,
            'region': uuid4().hex,
        }
        form = CreateBusinessForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'postal_code')

    def test_elicit_country(self):
        data = {
            'user': self.user.id,
            'name': uuid4().hex,
            'street_address': uuid4().hex,
            'locality': uuid4().hex,
            'region': uuid4().hex,
            'postal_code': uuid4().hex,
        }
        form = CreateBusinessForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'country')

    def test_elicit_has_logo(self):
        data = {
            'user': self.user.id,
            'name': uuid4().hex,
            'street_address': uuid4().hex,
            'locality': uuid4().hex,
            'region': uuid4().hex,
            'postal_code': uuid4().hex,
            'country': uuid4().hex,
        }
        form = CreateBusinessForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'has_logo')

    def test_elicit_logo_url(self):
        data = {
            'user': self.user.id,
            'name': uuid4().hex,
            'street_address': uuid4().hex,
            'locality': uuid4().hex,
            'region': uuid4().hex,
            'postal_code': uuid4().hex,
            'country': uuid4().hex,
            'has_logo': 'Yes'
        }
        form = CreateBusinessForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'logo_url')

    def test_elicit_logo_url_when_invalid(self):
        data = {
            'user': self.user.id,
            'name': uuid4().hex,
            'street_address': uuid4().hex,
            'locality': uuid4().hex,
            'region': uuid4().hex,
            'postal_code': uuid4().hex,
            'country': uuid4().hex,
            'has_logo': 'Yes',
            'logo_url': 'notaurl'
        }
        form = CreateBusinessForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'logo_url')

    def test_elicit_nothing_when_done(self):
        data = self.data.copy()
        form = CreateBusinessForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertIsNone(form.get_field_to_elicit())

    def test_elicit_for_no_logo(self):
        data = {
            'user': self.user.id,
            'name': uuid4().hex,
            'street_address': uuid4().hex,
            'locality': uuid4().hex,
            'region': uuid4().hex,
            'postal_code': uuid4().hex,
            'country': uuid4().hex,
            'has_logo': 'No'
        }
        form = CreateBusinessForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'do_accept_terms_of_service')

    def test_blank_logo_url(self):
        data = self.data.copy()
        data['logo_url'] = None
        form = CreateBusinessForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('logo_url' in form.errors)

    def test_valid_data(self):
        form = CreateBusinessForm(data=self.data.copy())
        self.assertTrue(form.is_valid())

    def test_very_yes(self):
        data = self.data.copy()
        data['has_logo'] = 'YES'
        form = CreateBusinessForm(data=data)
        form.is_valid()
        self.assertTrue(form.is_valid())

    def test_very_no(self):
        data = self.data.copy()
        data['has_logo'] = 'NO'
        form = CreateBusinessForm(data=data)
        self.assertTrue(form.is_valid())

    def test_is_fulfilled(self):
        form = CreateBusinessForm(data=self.data.copy())
        self.assertTrue(form.is_valid())
        self.assertTrue(form.is_fulfilled())

    def test_is_failed(self):
        data = self.data.copy()
        data['do_accept_terms_of_service'] = 'No'
        form = CreateBusinessForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertFalse(form.is_fulfilled())

    def test_invalid_image_url(self):
        data = self.data.copy()
        data['logo_url'] = 'https://google.com'
        form = CreateBusinessForm(data=data)
        self.assertFalse(form.is_valid())

    def test_two_line_street_address(self):
        data = self.data.copy()
        data['street_address'] = '5719 Old Buggy Court\nSMC 2076'
        form = CreateBusinessForm(data=data)
        self.assertTrue(form.is_valid())
        business = form.save()
        self.assertEqual(business.address.street_address_1, '5719 Old Buggy Court')
        self.assertEqual(business.address.street_address_2, 'SMC 2076')

    def test_invalid_street_address(self):
        data = self.data.copy()
        data['street_address'] = '5719 Old Buggy Court\nSMC 2076\nSMC 2005'
        form = CreateBusinessForm(data=data)
        self.assertFalse(form.is_valid())

    def test_create_business(self):
        data = self.data.copy()
        form = CreateBusinessForm(data=data)
        self.assertTrue(form.is_valid())
        business = form.save()
        self.assertIsNotNone(business)
        self.assertEqual(Business.objects.all().count(), 1)
        self.assertTrue(Business.objects.filter(user=self.user).exists())

        # Image
        self.assertIsNotNone(Business.objects.get(user=self.user).logo.url)

        # Stripe
        stripe_account = stripe.Account.retrieve(business.stripe_account_id)
        self.assertIsNotNone(stripe_account)
