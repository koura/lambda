from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from datetime import timedelta

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone
from psycopg2.extras import DateTimeTZRange

from koura.businesses.factories import TimeSheetFactory, TimeSheetEntryFactory
from koura.businesses.models import Invoice, TimeSheetEntry
from freezegun import freeze_time

User = get_user_model()


class InvoiceTestCase(TestCase):
    def test_application_fee(self):
        amount = 20 * 100
        self.assertEqual(Invoice.get_application_fee(amount), amount * .0325 + 30)


class TimeSheetTestCase(TestCase):
    def setUp(self):
        self.time_sheet = TimeSheetFactory()
        for i in range(1, 10):
            TimeSheetEntryFactory(
                time_sheet=self.time_sheet,
                time_range=DateTimeTZRange(lower=timezone.now() - timedelta(hours=i*24*30), upper=timezone.now() - timedelta(hours=i*24*30 - 5))
            )
    
    def tearDown(self):
        User.objects.all().delete()
    
    def test_is_timing(self):
        TimeSheetEntryFactory(time_sheet=self.time_sheet, time_range=DateTimeTZRange(lower=timezone.now()))
        self.assertTrue(self.time_sheet.is_timing)
    
    def test_is_not_timing(self):
        self.assertFalse(self.time_sheet.is_timing)
    
    def test_start_timer(self):
        num_entries = self.time_sheet.entries.all().count()
        self.assertFalse(self.time_sheet.is_timing)
        self.time_sheet.start_timer()
        self.assertEqual(num_entries + 1, self.time_sheet.entries.all().count())
        self.assertTrue(self.time_sheet.is_timing)
    
    def test_stop_timer(self):
        TimeSheetEntryFactory(time_sheet=self.time_sheet, time_range=DateTimeTZRange(lower=timezone.now()))
        num_entries = self.time_sheet.entries.all().count()
        self.assertTrue(self.time_sheet.is_timing)
        self.time_sheet.stop_timer()
        self.assertEqual(num_entries, self.time_sheet.entries.all().count())
        self.assertFalse(self.time_sheet.is_timing)

    def test_is_invoiceable(self):
        self.assertTrue(self.time_sheet.is_invoiceable)
        
    def test_is_not_invoiceable_0(self):
        TimeSheetEntryFactory(time_sheet=self.time_sheet, time_range=DateTimeTZRange(lower=timezone.now()))
        self.assertFalse(self.time_sheet.is_invoiceable)
        
    def test_is_not_invoiceable_1(self):
        TimeSheetEntry.objects.all().delete()
        self.assertFalse(self.time_sheet.is_invoiceable)
        
    def test_timedelta(self):
        TimeSheetEntry.objects.all().delete()
        for i in range(10):
            now = timezone.now() - timedelta(seconds=3600*24*30)
            TimeSheetEntryFactory(
                time_sheet=self.time_sheet,
                time_range=DateTimeTZRange(lower=now - timedelta(hours=i*24*30), upper=now - timedelta(hours=i*24*30 - 1))
            )
        now = timezone.now()
        TimeSheetEntryFactory(time_sheet=self.time_sheet, time_range=DateTimeTZRange(lower=now))
        with freeze_time(now + timedelta(seconds=3600)):
            self.assertEqual(self.time_sheet.timedelta, timedelta(hours=11))
    
    def test_rounded_hours(self):
        TimeSheetEntry.objects.all().delete()
        for i in range(10):
            now = timezone.now() - timedelta(seconds=3600*24*30)
            TimeSheetEntryFactory(
                time_sheet=self.time_sheet,
                time_range=DateTimeTZRange(lower=now - timedelta(seconds=i*3600*24*30), upper=now - timedelta(seconds=i*3600*24*30 - 34 * 60))
            )
        
        now = timezone.now()
        TimeSheetEntryFactory(time_sheet=self.time_sheet, time_range=DateTimeTZRange(lower=now))
        with freeze_time(now + timedelta(seconds=3600)):
            self.assertEqual(self.time_sheet.timedelta, timedelta(seconds=60 * 34 * 10 + 3600))
            self.assertEqual(self.time_sheet.rounded_hours, 6.7)
