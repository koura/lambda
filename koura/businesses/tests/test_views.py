from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from uuid import uuid4

import random
from django.contrib.auth import get_user_model
from django.core import signing
from django.urls import reverse
from django_webtest import WebTest
from faker import Faker
from rest_framework import status

from koura.businesses.enums import View
from koura.businesses.factories import BusinessFactory, ClientFactory, InvoiceFactory, TimeSheetFactory, TimeSheetEntryFactory, LineItemFactory
from koura.businesses.forms import CreateBusinessForm
from koura.businesses.models import Invoice
from koura.businesses.views import ConfirmInvoiceView
from koura.users.factories import UserFactory

User = get_user_model()


class ChargeInvoiceSuccessViewTestCase(WebTest):
    view_name = View.CHARGE_INVOICE_SUCCESS.value
    
    def setUp(self):
        self.business = BusinessFactory()
        self.invoice = InvoiceFactory(business=self.business)

    def tearDown(self):
        User.objects.all().delete()
        
    def test_page_exists(self):
        url = reverse(self.view_name, kwargs={'token': self.invoice.token})
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'businesses/charge_invoice_success.html')

    def test_invalid_token(self):
        url = reverse(self.view_name, kwargs={'token': 'fdffsdfsddas'})
        response = self.app.get(url, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        
class ChargeInvoiceViewTestCase(WebTest):
    view_name = View.CHARGE_INVOICE.value

    def setUp(self):
        self.user = UserFactory()
        form = CreateBusinessForm(data={
            'user': self.user.id,
            'name': uuid4().hex,
            'has_logo': 'Yes',
            'logo_url': 'https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png',
            'do_accept_terms_of_service': 'Yes',

            'street_address': '221B Baker Street',
            'locality': 'London',
            'region': 'California',
            'postal_code': '20444',
            'country': 'US',
        })
        self.assertTrue(form.is_valid())
        self.business = form.save()
        self.invoice = InvoiceFactory(business=self.business)
        self.data = {
            'stripe_token': 'tok_visa',
            'invoice': self.invoice.token,
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_page_exists(self):
        url = reverse(self.view_name, kwargs={'token': self.invoice.token})
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'businesses/charge_invoice.html')

    def test_charge_invoice(self):
        data = self.data.copy()
        url = reverse(self.view_name, kwargs={'token': self.invoice.token})
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'businesses/charge_invoice.html')
        form = response.form
        for key, value in data.items():
            form[key] = value
        response = form.submit()
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        # self.assertRedirects(response, reverse(View.CHARGE_INVOICE_SUCCESS.value, kwargs={'token': self.invoice.token}))
        self.assertFalse(Invoice.objects.filter(id=self.invoice.id, stripe_charge_id='').exists())

    def test_invalid_token(self):
        url = reverse(self.view_name, kwargs={'token': 'fdffsdfsddas'})
        response = self.app.get(url, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invoice_already_paid(self):
        self.invoice.stripe_charge_id = '12345'
        self.invoice.save()
        url = reverse(self.view_name, kwargs={'token': self.invoice.token})
        response = self.app.get(url, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_time_sheet(self):
        # Update line items
        for line_item in self.invoice.line_items.all():
            line_item.order += 1
            line_item.save()

        # Make a time sheet
        business = self.invoice.business
        client = self.invoice.client
        time_sheet = TimeSheetFactory(business=business, client=client, hourly_rate=9200)
        [TimeSheetEntryFactory(time_sheet=time_sheet) for i in range(10)]
        line_item = LineItemFactory(
            invoice=self.invoice,
            description=time_sheet.description,
            amount=time_sheet.amount,
            order=0
        )
        time_sheet.line_item = line_item
        time_sheet.save()
        self.assertIsNotNone(line_item.time_sheet)
        self.assertTrue(self.invoice.has_time_sheet)
        
        url = reverse(self.view_name, kwargs={'token': self.invoice.token})
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'businesses/charge_invoice.html')
        self.assertTrue(b'$92.00' in response.content)


class ConfirmInvoiceViewTestCase(WebTest):
    view_name = View.CONFIRM_INVOICE.value
    
    def setUp(self):
        client = ClientFactory()
        business = client.business
        self.data = {
            'line_items': [
                {
                    'description': Faker().catch_phrase(),
                    'amount': random.randint(100 * 100, 1000 * 100),
                    'order': i,
                }
                for i in range(random.randint(5, 10))
            ],
            'business': business.id,
            'client': client.id,
        }
        self.token = signing.dumps(self.data, salt=ConfirmInvoiceView.get_salt())

    def tearDown(self):
        User.objects.all().delete()
        
    def test_page_exists(self):
        url = reverse(self.view_name, kwargs={'token': self.token})
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'businesses/charge_invoice.html')

    def test_time_sheet(self):
        data = self.data.copy()
        data['has_time_sheet'] = True
        for line_item in data.get('line_items'):
            line_item['order'] += 1
        data['line_items'].insert(0, {
            'description': Faker().catch_phrase(),
            'amount': random.randint(100 * 100, 1000 * 100),
            'order': 0,
            'time_sheet': {
                'rounded_hours': '55.5',
                'hourly_rate': '9200'
            }
        })
        token = signing.dumps(data, salt=ConfirmInvoiceView.get_salt())
        url = reverse(self.view_name, kwargs={'token': token})
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'businesses/charge_invoice.html')
        self.assertTrue(b'55.5' in response.content)
        self.assertTrue(b'$92.00' in response.content)
