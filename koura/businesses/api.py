from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import logging

import boto3
import stripe
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.renderers import JSONRenderer
from rest_framework.renderers import StaticHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from koura.businesses.enums import StripeWebhookType
from koura.businesses.models import Invoice
from koura.sqs.enums import QueueName
from koura.sqs.utils import sqs_message

stripe.api_key = settings.STRIPE_SECRET_KEY
logger = logging.getLogger(__name__)


def get_deduplication_id(event):
    return event.get('id')


def get_group_id(event):
    return 'stripe'


@sqs_message(
    queue_name=QueueName.STRIPE_WEBHOOKS.value,
    get_deduplication_id=get_deduplication_id,
    get_group_id=get_group_id
)
def process_event(event):
    if event.get('type') == StripeWebhookType.CHARGE_SUCCEEDED.value:
        logger.info('Processing %s' % StripeWebhookType.CHARGE_SUCCEEDED.value)
        stripe_charge_id = event.get('data').get('object').get('id')
        try:
            invoice = Invoice.objects.get(stripe_charge_id=stripe_charge_id)
        except ObjectDoesNotExist:
            logger.critical('Failed to find invoice with stripe charge id %s' % stripe_charge_id)
            return
        business = invoice.business
        client = invoice.client
        logger.info('Business "%s" just received a successful charge of $%.2f from "%s"!' % (business.name, invoice.amount / 100, client.full_name))
        if business.has_external_accounts:
            business.send_facebook_message([
                {'text': 'Good news!'},
                {'text': '%s just paid the invoice you sent for $%.2f 🤑' % (client.full_name, invoice.amount / 100)},
                {'text': 'I\'ll reach out to you the next time I make a payout to your bank account'},
            ])
        else:
            business.send_facebook_message([
                {'text': 'Good news!'},
                {'text': '%s just paid the invoice you sent for $%.2f 🤑' % (client.full_name, invoice.amount / 100)},
                {'text': 'It looks like you have not given me your payout information'},
                {'text': 'No worries though! I can hold onto the funds 😉'},
                {'text': 'But until you configure your payout I won\'t be able to transfer those funds to you'},
                {'text': 'Try saying something like "Configure payout details"'},
            ])


class StripeWebhookView(APIView):
    http_method_names = ['post']

    renderer_classes = (StaticHTMLRenderer, JSONRenderer)

    def post(self, request, format=None):
        logger.info('Processing Stripe webhook request\n%s' % json.dumps(request.data, indent=2))

        # TODO: Use security token to check event
        process_event(request.data)

        # Tell consumer to start up
        if settings.AWS_SQS_IS_ENABLED:
            client = boto3.client('lambda')
            payload = {
                'queue_name': QueueName.STRIPE_WEBHOOKS.value
            }
            client.invoke(
                FunctionName=settings.AWS_LAMBDA_FUNCTION_NAME,
                InvocationType='Event',
                Payload=json.dumps(payload)
            )

        return Response({})
