from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from datetime import timedelta

import factory
import random
from django.utils import timezone
from psycopg2.extras import DateTimeTZRange
from faker import Faker
from koura.businesses.models import Business, Client, Invoice, LineItem, TimeSheet, TimeSheetEntry


class TimeSheetEntryFactory(factory.DjangoModelFactory):
    time_sheet = factory.SubFactory('koura.businesses.factories.TimeSheetFactory')
    # time_range = DateTimeTZRange(lower=timezone.now() - timedelta(hours=1), upper=timezone.now())
    
    @factory.lazy_attribute
    def time_range(self):
        lower = timezone.now() - timedelta(hours=24 * random.randint(0, 90))
        upper = lower + timedelta(hours=random.randint(1, 5))
        return DateTimeTZRange(lower=lower, upper=upper)

    class Meta:
        model = TimeSheetEntry


class TimeSheetFactory(factory.DjangoModelFactory):
    business = factory.SubFactory('koura.businesses.factories.BusinessFactory')
    client = factory.SubFactory('koura.businesses.factories.ClientFactory')
    description = factory.Faker('catch_phrase')

    @factory.lazy_attribute
    def hourly_rate(self):
        return random.randint(10 * 100, 100 * 100)

    class Meta:
        model = TimeSheet


class LineItemFactory(factory.DjangoModelFactory):
    description = factory.Faker('catch_phrase')
    order = factory.Sequence(lambda n: n)
    invoice = factory.SubFactory('koura.businesses.factories.InvoiceFactory')

    @factory.lazy_attribute
    def amount(self):
        return random.randint(100 * 100, 1000 * 100)

    class Meta:
        model = LineItem


class InvoiceFactory(factory.DjangoModelFactory):
    business = factory.SubFactory('koura.businesses.factories.BusinessFactory')
    client = factory.SubFactory('koura.businesses.factories.ClientFactory')

    @factory.post_generation
    def post(self, create, extracted, **kwargs):
        for i in range(random.randint(1, 10)):
            LineItemFactory(invoice=self)
        self.application_fee = Invoice.get_application_fee(amount=self.amount)
        self.save()

    class Meta:
        model = Invoice


class ClientFactory(factory.DjangoModelFactory):
    full_name = factory.Faker('name')
    business = factory.SubFactory('koura.businesses.factories.BusinessFactory')

    @factory.lazy_attribute
    def email_address(self):
        faker = Faker()
        email_address = None
        while email_address is None or Client.objects.filter(email_address=email_address).exists():
            email_address = faker.email()
        return email_address
        
    class Meta:
        model = Client


class BusinessFactory(factory.DjangoModelFactory):
    user = factory.SubFactory('koura.users.factories.UserFactory')
    name = factory.Faker('company')
    address = factory.SubFactory('koura.addresses.factories.AddressFactory')

    class Meta:
        model = Business
