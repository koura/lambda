from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from django.conf import settings


class RequireIsTestModeFalse(logging.Filter):
    def filter(self, record):
        return not settings.IS_TEST_MODE
