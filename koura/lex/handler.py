from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import logging
from importlib import import_module

import django
import os
from django.apps import apps
from django.conf import settings

# Configure Django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "koura.settings")
if not apps.ready and not settings.configured:
    django.setup()

logger = logging.getLogger(__name__)


# Yuck...
class LexHandler(object):
    def __init__(self):
        self.session = None

    def process_event(self, event, context):
        from koura.lex.intents import Intent, CreateBusinessIntent, CreateClientIntent, CreateInvoiceIntent, CreateBankPayoutIntent, CancelIntent, HelpIntent, RetrieveInvoicesIntent, HelloIntent, CreateTimeSheetIntent, RetrieveTimeSheetsIntent, CreateInvoiceWithTimeSheetIntent, StopTimeSheetIntent
        from koura.lex.enums import Intent as IntentName
        SessionStore = import_module(settings.SESSION_ENGINE).SessionStore

        intent_name = event.get('currentIntent').get('name')

        # First grab the user's session or create one if it does not exist
        session_attributes = event.get('sessionAttributes')
        if not session_attributes:
            session_attributes = {}
        session_key = session_attributes.get('key')
        if session_key:
            self.session = SessionStore(session_key)
            if 'intents' not in self.session:
                self.session['intents'] = {}
            if intent_name not in self.session['intents']:
                self.session['intents'][intent_name] = {
                    'num_events': 0
                }
            if 'num_events' not in self.session['intents'][intent_name]:
                self.session['intents'][intent_name]['num_events'] = 0
            self.session['intents'][intent_name]['num_events'] += 1
            self.session.save()
            logger.debug('Found existing session for %s' % event.get('userId'))
        else:
            self.session = SessionStore()
            self.session['lex_user_id'] = event.get('userId')
            self.session['intents'] = {
                intent_name: {
                    'num_events': 1
                }
            }
            self.session.create()
            session_attributes['key'] = self.session.session_key
            logger.debug('Found existing session for %s' % event.get('userId'))
        event['sessionAttributes'] = session_attributes
        
        # Did we change intents?
        if not 'current_intent' in self.session:
            self.session['current_intent'] = intent_name
        elif intent_name != self.session['current_intent']:
            keys_to_remove = []
            for k in self.session['intents'].keys():
                if k != intent_name:
                    keys_to_remove.append(k)
            for k in keys_to_remove:
                self.session['intents'].pop(k)
        self.session['current_intent'] = intent_name
        self.session.save()
        
        # Get the intent
        intents = {
            IntentName.CREATE_BUSINESS.value: lambda: CreateBusinessIntent(session=self.session),
            IntentName.CREATE_CLIENT.value: lambda: CreateClientIntent(session=self.session),
            IntentName.CREATE_INVOICE.value: lambda: CreateInvoiceIntent(session=self.session),
            IntentName.CREATE_INVOICE_WITH_TIME_SHEET.value: lambda: CreateInvoiceWithTimeSheetIntent(session=self.session),
            IntentName.CREATE_BANK_PAYOUT.value: lambda: CreateBankPayoutIntent(session=self.session),
            IntentName.CREATE_TIME_SHEET.value: lambda: CreateTimeSheetIntent(session=self.session),
            IntentName.RETRIEVE_INVOICES.value: lambda: RetrieveInvoicesIntent(session=self.session),
            IntentName.RETRIEVE_TIME_SHEETS.value: lambda: RetrieveTimeSheetsIntent(session=self.session),
            IntentName.STOP_TIME_SHEET.value: lambda: StopTimeSheetIntent(session=self.session),
            IntentName.CANCEL.value: lambda: CancelIntent(session=self.session),
            IntentName.HELP.value: lambda: HelpIntent(session=self.session),
            IntentName.HELLO.value: lambda: HelloIntent(session=self.session)
        }
        if intent_name in intents:
            intent = intents.get(intent_name)()
            response = intent.process_event(event=event)
        else:
            response = Intent.create_elicit_intent_response(event=event, session=self.session)
        return response


def handler(event, context=None):
    logger.debug('We received an invocation from Lex!\n%s' % (json.dumps(event, indent=2, sort_keys=True), ))
    lex_handler = LexHandler()
    response = lex_handler.process_event(event, context)
    logger.debug('Our response is\n%s' % json.dumps(response, indent=2, sort_keys=True))
    return response
