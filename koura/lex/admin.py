from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
from importlib import import_module

from django.conf import settings
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse

SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
User = get_user_model()


@admin.register(SessionStore().model)
class SessionAdmin(admin.ModelAdmin):
    readonly_fields = ('decoded_session_data', 'user')
    fieldsets = (
        (None, {'fields': ('session_key', 'user', 'decoded_session_data', 'expire_date')}),
    )
    list_display = ('session_key', 'user', 'expire_date')

    def decoded_session_data(self, obj):
        return '<pre>%s</pre>' % json.dumps(obj.get_decoded(), indent=2)
    decoded_session_data.allow_tags = True

    def user(self, obj):
        session_data = obj.get_decoded()
        user = None
        if 'user_id' in session_data:
            try:
                user = User.objects.get(id=int(session_data['user_id']))
            except ObjectDoesNotExist:
                pass
        elif '_auth_user_id' in session_data:
            try:
                user = User.objects.get(id=int(session_data['_auth_user_id']))
            except ObjectDoesNotExist:
                pass

        if user:
            return '<a href="%s">%d</a>' % (reverse('admin:users_user_change', args=(user.id,)), user.id)
        return None
    user.allow_tags = True
