from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.contrib.sessions.backends.cached_db import SessionStore as CachedDBStore
from django.contrib.sessions.base_session import AbstractBaseSession


class Session(AbstractBaseSession):
    messenger_user_id = models.CharField(blank=False, default='', max_length=512, db_index=True)

    @classmethod
    def get_session_store_class(cls):
        return SessionStore


class SessionStore(CachedDBStore):
    cache_key_prefix = 'saffron.lex.models'

    @classmethod
    def get_model_class(cls):
        return Session

    def create_model_instance(self, data):
        obj = super(SessionStore, self).create_model_instance(data)
        if data.get('messenger_user_id', None):
            obj.messenger_user_id = data.get('messenger_user_id')
        return obj
