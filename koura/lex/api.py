from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import logging
from importlib import import_module
from time import sleep
from uuid import uuid4

import boto3
import random
import requests
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.renderers import JSONRenderer
from rest_framework.renderers import StaticHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from koura.pagination import Paginator

from koura.businesses.models import TimeSheet
from koura.businesses.forms import RetrieveTimeSheetsForm
from koura.lex.enums import ContentType, DialogActionType, Postback, Intent as IntentName
from koura.lex.intents import Intent, RetrieveTimeSheetsIntent, RetrieveInvoicesIntent
from koura.sqs.enums import QueueName
from koura.sqs.utils import sqs_message
from django.core import signing

MESSAGE_ENDPOINT = 'https://graph.facebook.com/v2.6/me/messages?access_token=%s' % settings.MESSENGER_PAGE_TOKEN

User = get_user_model()
logger = logging.getLogger(__name__)
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


def handle_lex_response(response, user, session):
    def send_typing(is_on):
        requests.post(MESSAGE_ENDPOINT, json={
            'recipient': {
                'id': user.messenger_user_id,
            },
            'sender_action': 'typing_on' if is_on else 'typing_off'
        })

    def pause():
        sleep(max([random.gauss(1.5, 0.5), 0]))

    # Lex won't let me ElicitIntents...?
    event = response.copy()
    del event['ResponseMetadata']
    if event.get('dialogState') == DialogActionType.ELICIT_INTENT.value:
        Intent.create_elicit_intent_response(event=event, session=session)  # Remember, this method dumps an outbox into Redis

    # Tell the user we read the message
    requests.post(MESSAGE_ENDPOINT, json={
        'recipient': {
            'id': user.messenger_user_id,
        },
        'sender_action': 'mark_seen'
    })

    # Send reply
    session = SessionStore(session_key=session.session_key)
    if 'outbox' not in session:
        logger.critical('Failed to find outbox in Lex response')
        return

    outbox = session['outbox'].get(ContentType.MESSENGER)
    if isinstance(outbox, list):
        for message in outbox:
            # Dramatic pause
            send_typing(True)
            pause()
            payload = {
                'recipient': {
                    'id': user.messenger_user_id
                },
                'message': message,
            }
            try:
                logger.info('Sending message to Messenger\n%s' % json.dumps(payload, indent=2))
            except TypeError:
                pass
            response = requests.post(MESSAGE_ENDPOINT, json=payload)
            try:
                logger.info('Messenger response is\n%s' % json.dumps(response.json(), indent=2))
            except (TypeError, AttributeError):
                pass
    else:
        send_typing(True)
        pause()
        payload = {
            'recipient': {
                'id': user.messenger_user_id
            },
            'message': outbox,
        }
        try:
            logger.info('Sending message to Messenger\n%s' % json.dumps(payload, indent=2))
        except TypeError:
            pass
        response = requests.post(MESSAGE_ENDPOINT, json=payload)
        try:
            logger.info('Messenger response is\n%s' % json.dumps(response.json(), indent=2))
        except (TypeError, AttributeError):
            pass

    # We are done typing...
    send_typing(False)

    # Remove messages from session
    del session['outbox']
    session.save()


def process_postback(postback, user, session):
    logger.info('Processing postback\n%s' % json.dumps(postback, indent=2))
    type = postback.get('type')
    payload = postback.get('payload')
    if type == Postback.START_TIMER.value:
        time_sheet = TimeSheet.objects.get(id=payload.get('time_sheet'), business__user=user)
        if time_sheet.is_timing:
            user.send_facebook_message([
                {'text': 'This timer is already running'},
            ])
        else:
            time_sheet.start_timer()
            user.send_facebook_message([
                {'text': 'I have started your timer'},
                {'text': 'Just let me know when you would like me to stop time tracking'},
                {'text': 'Try saying something like \'Stop time tracking\''},
            ])
    elif type == Postback.STOP_TIMER.value:
        time_sheet = TimeSheet.objects.get(id=payload.get('time_sheet'), business__user=user)
        if time_sheet.is_timing:
            time_sheet.stop_timer()
            user.send_facebook_message([
                {'text': 'I have stopped your timer'},
                {'text': 'So far you have %.1f hours logged for this time sheet' % time_sheet.rounded_hours},
                {
                    'attachment': {
                        'type': 'template',
                        'payload': {
                            'template_type': 'generic',
                            'elements': RetrieveTimeSheetsIntent.get_facebook_elements([time_sheet], False),
                        }
                    }
                }
            ])
        else:
            user.send_facebook_message([
                {'text': 'This timer has already been stopped'},
            ])
    elif type == Postback.CREATE_INVOICE.value:
        time_sheet = TimeSheet.objects.get(id=payload.get('time_sheet'), business__user=user)

        # Is this timer running lol
        if time_sheet.is_timing:
            user.send_facebook_message([
                {'text': 'Oops 😮'},
                {'text': 'You must stop the timer for this time sheet before you can create an invoice'},
                {
                    'attachment': {
                        'type': 'template',
                        'payload': {
                            'template_type': 'generic',
                            'elements': RetrieveTimeSheetsIntent.get_facebook_elements([time_sheet], False),
                        }
                    }
                }
            ])
            return

        client = time_sheet.client
        session['current_intent'] = IntentName.CREATE_INVOICE_WITH_TIME_SHEET.value
        session['intents'] = {
            IntentName.CREATE_INVOICE_WITH_TIME_SHEET.value: {
                'num_events': 0,
                'slots': {
                    'do_elicit_line_items': None,
                    'line_items': None,
                    'do_confirm': None,
                },
                'last_form_errors': {
                    'do_elicit_line_items': '',
                    'do_confirm': '',
                },
                'initial_data': {
                    'query': client.full_name,
                    'client': client.id,
                    'time_sheet': time_sheet.id,
                }
            }
        }
        session.save()
        
        # Contact Lex
        input_transcript = 'Create invoice with time sheet'
        lex_client = boto3.client('lex-runtime')
        response = lex_client.post_text(
            botName=settings.AWS_LEX_BOT_NAME,
            botAlias=settings.AWS_LEX_BOT_ALIAS,
            userId=str(user.messenger_user_id),
            inputText=input_transcript,
            sessionAttributes={
                'key': session.session_key
            }
        )
        try:
            logger.info('Lex response is\n%s' % json.dumps(response, indent=2))
        except TypeError:
            pass
        handle_lex_response(response, user, session)
    elif type == Postback.RETRIEVE_TIME_SHEET.value:
        time_sheet = TimeSheet.objects.get(id=payload.get('time_sheet'), business__user=user)
        elements = RetrieveTimeSheetsIntent.get_facebook_elements([time_sheet], False)
        reply_payload = {
            'template_type': 'generic',
            'elements': elements,
        }
        user.send_facebook_message([
            {
                'attachment': {
                    'type': 'template',
                    'payload': reply_payload
                }
            }
        ])
    elif type == Postback.RETRIEVE_TIME_SHEETS.value:
        data = signing.loads(payload.get('next_page_token'))
        time_sheets = TimeSheet.objects.all()
        if data.get('filter_kwargs'):
            time_sheets = time_sheets.filter(**data.get('filter_kwargs'))
        if data.get('exclude_kwargs'):
            time_sheets = time_sheets.exclude(**data.get('exclude_kwargs'))
        if data.get('ordering_args'):
            time_sheets = time_sheets.order_by(*data.get('ordering_args'))
        paginator = Paginator(time_sheets, data.get('page_size'))
        page = paginator.page(data.get('page_number'))
        next_page_token = RetrieveTimeSheetsForm.get_next_page_token(
            paginator=paginator,
            page_number=data.get('page_number'),
            page_size=data.get('page_size'),
            model_class=data.get('model_class'),
            filter_kwargs=data.get('filter_kwargs'),
            ordering_args=data.get('ordering_args')
        )
        is_list_view = bool(next_page_token) or len(page.object_list) >= 2
        elements = RetrieveTimeSheetsIntent.get_facebook_elements(page.object_list, is_list_view)
        reply_payload = {
            'template_type': 'list' if is_list_view else 'generic',
            'elements': elements,
        }
        if is_list_view:
            reply_payload['top_element_style'] = 'compact'
            if next_page_token:
                reply_payload['buttons'] = [{
                    'title': 'View More',
                    'type': 'postback',
                    'payload': json.dumps({
                        'type': Postback.RETRIEVE_TIME_SHEETS.value,
                        'payload': {
                            'next_page_token': next_page_token
                        }
                    })
                }]
        user.send_facebook_message([
            {
                'attachment': {
                    'type': 'template',
                    'payload': reply_payload
                }
            }
        ])
    elif type == Postback.RETRIEVE_INVOICES.value:
        paginator, page = Paginator.from_page_token(payload.get('next_page_token'))
        next_page_token = paginator.get_page_token(page.number + 1, 'koura.businesses.models.Invoice')
        is_list_view = bool(next_page_token) or len(page.object_list) >= 2
        elements = RetrieveInvoicesIntent.get_facebook_elements(page.object_list, is_list_view)
        reply_payload = {
            'template_type': 'list' if is_list_view else 'generic',
            'elements': elements,
        }
        if is_list_view:
            reply_payload['top_element_style'] = 'compact'
            if next_page_token:
                reply_payload['buttons'] = [{
                    'title': 'View More',
                    'type': 'postback',
                    'payload': json.dumps({
                        'type': Postback.RETRIEVE_INVOICES.value,
                        'payload': {
                            'next_page_token': next_page_token
                        }
                    })
                }]
        user.send_facebook_message([
            {
                'attachment': {
                    'type': 'template',
                    'payload': reply_payload
                }
            }
        ])
    else:
        raise Exception('Unhandled postback %s' % json.dumps(postback, indent=2))


def get_deduplication_id(message):
    return message.get('message', {}).get('mid', uuid4().hex)


def get_group_id(message):
    return message.get('sender').get('id')


@sqs_message(
    queue_name=QueueName.MESSENGER_WEBHOOKS.value,
    get_deduplication_id=get_deduplication_id,
    get_group_id=get_group_id
)
def process_message(message):
    logger.info('Processing message\n%s' % json.dumps(message, indent=2))
    messenger_user_id = message.get('sender').get('id')

    # Create user
    try:
        user = User.objects.get(messenger_user_id=messenger_user_id)
    except ObjectDoesNotExist:
        email_address = '%s@%s' % (uuid4().hex, 'simulator.amazonses.com')
        screen_name = '%s' % (uuid4().hex,)
        password = uuid4().hex
        user = User.objects.create_user(email_address, screen_name, password)
    user.messenger_user_id = messenger_user_id
    user.save()

    # Try to grab data from Facebook
    if not user.full_name:
        response = requests.get(
            'https://graph.facebook.com/v2.6/%s' % (user.messenger_user_id,),
            params={'access_token': settings.MESSENGER_PAGE_TOKEN},
            headers={'Content-Type': 'application/json'}
        )
        data = response.json()
        logger.debug('User API response is\n%s' % json.dumps(data, indent=2))
        if data.get('first_name'):
            user.nickname = data.get('first_name')
            if data.get('last_name'):
                user.full_name = '%s %s' % (data.get('first_name'), data.get('last_name'))
            user.save()

    # Create a session
    try:
        obj = SessionStore().model.objects.get(messenger_user_id=messenger_user_id)
        session = SessionStore(session_key=obj.session_key)
    except ObjectDoesNotExist:
        session = SessionStore()
        session['messenger_user_id'] = messenger_user_id
        session['inbox'] = {}
        session.create()
    session['user_id'] = user.id
    session.save()

    # Have we already processed this message?
    if 'message' in message:
        if message.get('message').get('mid') in session['inbox']:
            logger.info('We already processed this message!')
            return
        session['inbox'][message.get('message').get('mid')] = message
        session.save()

    # Process input transcript
    input_transcript = None
    if 'message' in message:
        input_transcript = message.get('message').get('text')
        if not input_transcript:  # Is this an attachment?
            if message.get('message').get('attachments'):
                input_transcript = message.get('message').get('attachments')[0].get('payload').get('url')
            elif message.get('message').get('quick_reply'):
                input_transcript = message.get('message').get('quick_reply').get('payload')
    elif 'postback' in message:
        if not message.get('postback').get('referral'):
            payload = json.loads(message.get('postback').get('payload'))
            if isinstance(payload, dict) and 'type' in payload and 'payload' in payload:
                process_postback(payload, user, session)
                return
            else:
                input_transcript = message.get('postback').get('payload')
            
    if not input_transcript:
        logger.critical('Failed to calculate input transcript for message\n%s', json.dumps(message, indent=2))
        return

    # Contact Lex
    lex_client = boto3.client('lex-runtime')
    response = lex_client.post_text(
        botName=settings.AWS_LEX_BOT_NAME,
        botAlias=settings.AWS_LEX_BOT_ALIAS,
        userId=str(user.messenger_user_id),
        inputText=input_transcript,
        sessionAttributes={
            'key': session.session_key
        }
    )
    try:
        logger.info('Lex response is\n%s' % json.dumps(response, indent=2))
    except TypeError:
        pass

    handle_lex_response(response, user, session)

    
class MessengerWebhookView(APIView):
    http_method_names = ['post', 'get']

    renderer_classes = (StaticHTMLRenderer, JSONRenderer)

    def get(self, request):
        mode = request.GET.get('hub.mode')
        verify_token = request.GET.get('hub.verify_token')
        challenge = request.GET.get('hub.challenge')
        if mode == 'subscribe' and verify_token == settings.MESSENGER_VERIFY_TOKEN:
            text = challenge
        else:
            text = 'Hello World!'
        return Response('%s' % text, content_type='text/plain')

    def post(self, request, format=None):
        # This should probably be done in SQS...:smile:
        logger.info('Processing request\n%s' % json.dumps(request.data, indent=2))
        for entry in request.data.get('entry'):
            for message in entry.get('messaging'):
                process_message(message)

        # Tell consumer to start up
        if settings.AWS_SQS_IS_ENABLED:
            client = boto3.client('lambda')
            payload = {
                'queue_name': QueueName.MESSENGER_WEBHOOKS.value
            }
            client.invoke(
                FunctionName=settings.AWS_LAMBDA_FUNCTION_NAME,
                InvocationType='Event',
                Payload=json.dumps(payload)
            )

        return Response({})
