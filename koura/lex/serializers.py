from __future__ import absolute_import

import logging

import stripe
from django.conf import settings
from gouda.clients.models import BraintreeWebhookNotification
from gouda.clients.models import StripeCard
from gouda.clients.models import StripeClient
from gouda.clients.models import StripeEvent
from gouda.clients.models import StripePlan
from rest_framework import serializers

logger = logging.getLogger(__name__)

STRIPE_SECRET_KEY = getattr(settings, 'STRIPE_SECRET_KEY')
stripe.api_key = STRIPE_SECRET_KEY


class BraintreeWebhookNotificationSerializer(serializers.ModelSerializer):
    bt_signature = serializers.CharField(source='signature', write_only=True)
    bt_payload = serializers.CharField(source='payload', trim_whitespace=False, write_only=True)

    class Meta:
        model = BraintreeWebhookNotification
        exclude = ('signature', 'payload', 'date_created', 'date_modified', 'timestamp', 'kind')


class StripeEventSerializer(serializers.Serializer):
    id = serializers.CharField(write_only=True)
    stripe_id = serializers.CharField(read_only=True)

    def create(self, validated_data):
        return StripeEvent.objects.create(stripe_id=validated_data.get('id'))


class StripeCouponSerializer(serializers.Serializer):
    amount_off = serializers.IntegerField(allow_null=True)
    created = serializers.IntegerField()
    currency = serializers.CharField(allow_null=True)
    duration = serializers.CharField()
    duration_in_months = serializers.IntegerField(allow_null=True)
    id = serializers.CharField()
    max_redemptions = serializers.IntegerField(allow_null=True)
    percent_off = serializers.IntegerField(allow_null=True)
    redeem_by = serializers.IntegerField(allow_null=True)
    times_redeemed = serializers.IntegerField(allow_null=True)
    valid = serializers.BooleanField()


class StripeCardSerializer(serializers.ModelSerializer):
    country = serializers.CharField(allow_null=True)
    name = serializers.CharField(allow_null=True)
    address_line1 = serializers.CharField(allow_null=True)
    address_line2 = serializers.CharField(allow_null=True)
    address_city = serializers.CharField(allow_null=True)
    address_state = serializers.CharField(allow_null=True)
    address_zip = serializers.CharField(allow_null=True)
    address_country = serializers.CharField(allow_null=True)
    cvc_check = serializers.CharField(allow_null=True)
    address_line1_check = serializers.CharField(allow_null=True)
    address_zip_check = serializers.CharField(allow_null=True)
    dynamic_last4 = serializers.CharField(allow_null=True)

    class Meta:
        model = StripeCard
        fields = [
            'last4',
            'brand',
            'funding',
            'exp_month',
            'exp_year',
            'country',
            'name',
            'address_line1',
            'address_line2',
            'address_city',
            'address_state',
            'address_zip',
            'address_country',
            'cvc_check',
            'address_line1_check',
            'address_zip_check',
            'dynamic_last4'
        ]


class StripeTokenSerializer(serializers.Serializer):
    card = StripeCardSerializer(write_only=True)
    coupon = StripeCouponSerializer(write_only=True, required=False)
    client_ip = serializers.CharField(write_only=True)
    plan_id = serializers.CharField(write_only=True)
    id = serializers.CharField(write_only=True)
    stripe_id = serializers.CharField(read_only=True)

    def create(self, validated_data):
        card_data = validated_data.pop('card')
        coupon_data = validated_data.pop('coupon', None)
        coupon_id = None
        if coupon_data:
            coupon_id = coupon_data.get('id') if coupon_data.get('valid') else None

        keys = card_data.keys()
        for key in keys:
            if card_data.get(key) is None:
                card_data.pop(key)

        # Grab user
        user = self.context.get('request').user

        # Use Stripe to build a client on their servers
        stripe_client = stripe.Client.create(
            source=validated_data.get('id'),
            plan=validated_data.get('plan_id'),
            email=user.email_address,
            coupon=coupon_id
        )
        stripe_client = StripeClient.objects.create(user=user, stripe_id=stripe_client.id)
        stripe_card = StripeCard.objects.create(stripe_client=stripe_client, **card_data)

        return stripe_client


class StripePlanSerializer(serializers.ModelSerializer):
    class Meta:
        model = StripePlan
        fields = ['interval',
                  'name',
                  'currency',
                  'stripe_id',
                  'statement_descriptor',
                  'amount',
                  'interval_count',
                  'trial_period_days']
