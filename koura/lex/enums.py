from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from enum import Enum, IntEnum


class FulfillmentState(Enum):
    FULFILLED = 'Fulfilled'
    FAILED = 'Failed'


class InvocationSource(Enum):
    DIALOG_CODE_HOOK = 'DialogCodeHook'
    FULFILLMENT_CODE_HOOK = 'FulfillmentCodeHook'


class DialogActionType(Enum):
    CLOSE = 'Close'
    DELEGATE = 'Delegate'
    ELICIT_SLOT = 'ElicitSlot'
    ELICIT_INTENT = 'ElicitIntent'


class ConfirmationStatus(Enum):
    NONE = 'None'
    CONFIRMED = 'Confirmed'
    DENIED = 'Denied'


class Intent(Enum):
    CREATE_BUSINESS = 'CreateBusiness'
    CREATE_CLIENT = 'CreateClient'
    CREATE_INVOICE = 'CreateInvoice'
    CREATE_INVOICE_WITH_TIME_SHEET = 'CreateInvoiceWithTimeSheet'
    CREATE_BANK_PAYOUT = 'CreateBankPayout'
    CREATE_TIME_SHEET = 'CreateTimeSheet'
    RETRIEVE_INVOICES = 'RetrieveInvoices'
    RETRIEVE_TIME_SHEETS = 'RetrieveTimeSheets'
    STOP_TIME_SHEET = 'StopTimeSheet'
    CANCEL = 'Cancel'
    HELP = 'Help'
    HELLO = 'Hello'


class Context(IntEnum):
    DEFAULT = 0
    ERROR = 1


class ContentType(IntEnum):
    PLAIN_TEXT = 0
    MESSENGER = 1


class Postback(Enum):
    START_TIMER = 'StartTimer'
    STOP_TIMER = 'StopTimer'
    CREATE_INVOICE = 'CreateInvoice'
    RETRIEVE_TIME_SHEETS = 'RetrieveTimeSheets'
    RETRIEVE_TIME_SHEET = 'RetrieveTimeSheet'
    RETRIEVE_INVOICES = 'RetrieveInvoices'
