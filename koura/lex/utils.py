from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import copy
import re
from django.utils import timezone


def verbose_timedelta(start_date, end_date=None):
    if end_date is None:
        end_date = timezone.now()
    delta = end_date - start_date
    seconds = int(abs(delta.total_seconds()))
    days = int(seconds / (60 * 60 * 24))
    hours = int(seconds / (60 * 60))
    minutes = int(seconds / 60)
    if days > 0:
        text = '%d %s ago' % (days, pluralize('day', days))
    elif hours > 0:
        text = '%d %s ago' % (hours, pluralize('hour', hours))
    elif minutes > 0:
        text = '%d %s ago' % (minutes, pluralize('minute', minutes))
    else:
        text = '%d %s ago' % (seconds, pluralize('second', seconds))
    return text


def pluralize(unit, amount):
    return '%s%s' % (unit, '' if amount == 1 else 's')


def to_snake_case(text):
    snake_case_text = copy.deepcopy(text)
    if snake_case_text == 'idleSessionTTLInSeconds':
        return 'idle_session_ttl_in_seconds'
    for substring in re.compile('[A-Z]').findall(text):
        snake_case_text = snake_case_text.replace(substring, '_' + substring[0].lower())
    return snake_case_text


def to_camel_case(obj, mapping=None):
    if isinstance(obj, str):
        camel_case_text = copy.deepcopy(obj)
        if camel_case_text == 'idle_session_ttl_in_seconds':
            return 'idleSessionTTLInSeconds'
        if mapping and camel_case_text in mapping:
            print('=======================================')
            return mapping.get(camel_case_text)
        for substring in re.compile('_[A-Za-z0-9]').findall(obj):
            camel_case_text = camel_case_text.replace(substring, substring[-1].upper())
        return camel_case_text
    elif isinstance(obj, dict):
        keys_to_remove = obj.keys()
        keys_to_keep = []
        temp_dict = copy.deepcopy(obj)
        for k, v in temp_dict.items():
            key_to_keep = to_camel_case(k)
            obj[key_to_keep] = v
            keys_to_keep.append(key_to_keep)
        for k in [k for k in keys_to_remove if k not in keys_to_keep]:
            del obj[k]
        return obj
