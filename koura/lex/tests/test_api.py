from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from importlib import import_module
from unittest import mock
from uuid import uuid4

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from koura.enums import View, Namespace
from koura.lex.enums import Postback, Intent as IntentName
from koura.urlresolvers import reverse_with_query
from koura.businesses.factories import BusinessFactory, TimeSheetFactory, TimeSheetEntryFactory, InvoiceFactory, LineItemFactory
from koura.businesses.forms import RetrieveTimeSheetsForm
from koura.businesses.models import TimeSheet, Invoice
from koura.users.factories import UserFactory
import json
from psycopg2.extras import DateTimeTZRange
from django.utils import timezone
from unittest import skip
from koura.pagination import Paginator
import random

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


class MessengerWebhookViewTestCase(APITestCase):
    view_name = '%s:%s' % (Namespace.LEX_API.value, View.MESSENGER_WEBHOOK.value)

    @classmethod
    def setUpClass(cls):
        super(MessengerWebhookViewTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(MessengerWebhookViewTestCase, cls).tearDownClass()

    def setUp(self):
        self.patchers = [
            mock.patch('boto3.client', mock.MagicMock(return_value=mock.MagicMock())),
            mock.patch('requests.post', mock.MagicMock(return_value={}))
        ]
        [p.start() for p in self.patchers]

        self.messenger_user_id = uuid4().hex
        self.data = {
            'object': 'page',
            'entry': [
                {
                    'id': 'PAGE_ID',
                    'time': 1458692752478,
                    'messaging': [
                        {
                            'sender': {
                                'id': self.messenger_user_id
                            },
                            'recipient': {
                                'id': 'PAGE_ID'
                            },
                            'timestamp': 1458692752478,
                            'message': {
                                'mid': 'mid.1457764197618:41d102a3e1ae206a38',
                                'text': 'hello, world!',
                                'quick_reply': {
                                    'payload': 'DEVELOPER_DEFINED_PAYLOAD'
                                }
                            }
                        }
                    ]
                }
            ]
        }

    def tearDown(self):
        [p.stop() for p in self.patchers]
        SessionStore().model.objects.all().delete()
        User.objects.all().delete()

    def test_verification(self):
        query_params = {
            'hub.mode': 'subscribe',
            'hub.challenge': '1234',
            'hub.verify_token': settings.MESSENGER_VERIFY_TOKEN
        }
        url = reverse_with_query(self.view_name, query_params=query_params)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(query_params.get('hub.challenge') in response.content.decode('utf-8'))

    def test_user_creation(self):
        data = self.data.copy()
        url = reverse(self.view_name)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(User.objects.all().count(), 1)
        self.assertTrue(User.objects.filter(messenger_user_id=self.messenger_user_id).exists())

    def test_session_creation(self):
        data = self.data.copy()
        url = reverse(self.view_name)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(SessionStore().model.objects.filter(messenger_user_id=self.messenger_user_id).exists())

    def test_webhook(self):
        data = self.data.copy()
        url = reverse(self.view_name)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
    def test_start_timer(self):
        user = UserFactory()
        business = BusinessFactory(user=user)
        time_sheet = TimeSheetFactory(business=business)
        data = {
            'object': 'page',
            'entry': [
                {
                    'id': 'PAGE_ID',
                    'time': 1458692752478,
                    'messaging': [
                        {
                            'sender': {
                                'id': user.messenger_user_id
                            },
                            'recipient': {
                                'id': 'PAGE_ID'
                            },
                            'timestamp': 1458692752478,
                            'postback': {
                                'payload': json.dumps({
                                    'type': Postback.START_TIMER.value,
                                    'payload': {
                                        'time_sheet': time_sheet.id
                                    }
                                }),
                            }
                        }
                    ]
                }
            ]
        }
        url = reverse(self.view_name)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(time_sheet.is_timing)
    
    def test_stop_timer(self):
        user = UserFactory()
        business = BusinessFactory(user=user)
        time_sheet = TimeSheetFactory(business=business)
        TimeSheetEntryFactory(
            time_sheet=time_sheet,
            time_range=DateTimeTZRange(lower=timezone.now())
        )
        data = {
            'object': 'page',
            'entry': [
                {
                    'id': 'PAGE_ID',
                    'time': 1458692752478,
                    'messaging': [
                        {
                            'sender': {
                                'id': user.messenger_user_id
                            },
                            'recipient': {
                                'id': 'PAGE_ID'
                            },
                            'timestamp': 1458692752478,
                            'postback': {
                                'payload': json.dumps({
                                    'type': Postback.STOP_TIMER.value,
                                    'payload': {
                                        'time_sheet': time_sheet.id
                                    }
                                }),
                            }
                        }
                    ]
                }
            ]
        }
        url = reverse(self.view_name)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(time_sheet.is_timing)

    def test_create_invoice_with_time_sheet(self):
        user = UserFactory()
        business = BusinessFactory(user=user)
        time_sheet = TimeSheetFactory(business=business)
        data = {
            'object': 'page',
            'entry': [
                {
                    'id': 'PAGE_ID',
                    'time': 1458692752478,
                    'messaging': [
                        {
                            'sender': {
                                'id': user.messenger_user_id
                            },
                            'recipient': {
                                'id': 'PAGE_ID'
                            },
                            'timestamp': 1458692752478,
                            'postback': {
                                'payload': json.dumps({
                                    'type': Postback.CREATE_INVOICE.value,
                                    'payload': {
                                        'time_sheet': time_sheet.id
                                    }
                                }),
                            }
                        }
                    ]
                }
            ]
        }
        url = reverse(self.view_name)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        obj = SessionStore().model.objects.get(messenger_user_id=user.messenger_user_id)
        session = SessionStore(session_key=obj.session_key)
        self.assertEqual(session['current_intent'], IntentName.CREATE_INVOICE_WITH_TIME_SHEET.value)

    def test_time_sheet_pagination(self):
        user = UserFactory()
        business = BusinessFactory(user=user)
        for i in range(50):
            TimeSheetFactory(business=business)
        queryset = TimeSheet.objects.filter(business=business).order_by('-date_created')
        paginator = Paginator(queryset, 10)
        next_page_token = RetrieveTimeSheetsForm.get_next_page_token(
            paginator=paginator,
            page_number=1,
            page_size=10,
            model_class='koura.businesses.models.TimeSheet',
            filter_kwargs={
                'business': business.id
            },
            ordering_args=[
                '-date_created'
            ]
        )
        data = {
            'object': 'page',
            'entry': [
                {
                    'id': 'PAGE_ID',
                    'time': 1458692752478,
                    'messaging': [
                        {
                            'sender': {
                                'id': user.messenger_user_id
                            },
                            'recipient': {
                                'id': 'PAGE_ID'
                            },
                            'timestamp': 1458692752478,
                            'postback': {
                                'payload': json.dumps({
                                    'type': Postback.RETRIEVE_TIME_SHEETS.value,
                                    'payload': {
                                        'next_page_token': next_page_token
                                    }
                                }),
                            }
                        }
                    ]
                }
            ]
        }
        url = reverse(self.view_name)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_time_sheet(self):
        user = UserFactory()
        business = BusinessFactory(user=user)
        time_sheet = TimeSheetFactory(business=business)
        TimeSheetEntryFactory(
            time_sheet=time_sheet,
            time_range=DateTimeTZRange(lower=timezone.now())
        )
        data = {
            'object': 'page',
            'entry': [
                {
                    'id': 'PAGE_ID',
                    'time': 1458692752478,
                    'messaging': [
                        {
                            'sender': {
                                'id': user.messenger_user_id
                            },
                            'recipient': {
                                'id': 'PAGE_ID'
                            },
                            'timestamp': 1458692752478,
                            'postback': {
                                'payload': json.dumps({
                                    'type': Postback.RETRIEVE_TIME_SHEET.value,
                                    'payload': {
                                        'time_sheet': time_sheet.id
                                    }
                                }),
                            }
                        }
                    ]
                }
            ]
        }
        url = reverse(self.view_name)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_invoices(self):
        user = UserFactory()
        business = BusinessFactory(user=user)
        for i in range(10):
            invoice = InvoiceFactory(business=business)
            for j in range(random.randint(1, 5)):
                LineItemFactory(invoice=invoice)
        paginator = Paginator(Invoice.objects.all().order_by('-date_created'), 4)
        next_page_token = paginator.get_page_token(2, 'koura.businesses.models.Invoice')
        data = {
            'object': 'page',
            'entry': [
                {
                    'id': 'PAGE_ID',
                    'time': 1458692752478,
                    'messaging': [
                        {
                            'sender': {
                                'id': user.messenger_user_id
                            },
                            'recipient': {
                                'id': 'PAGE_ID'
                            },
                            'timestamp': 1458692752478,
                            'postback': {
                                'payload': json.dumps({
                                    'type': Postback.RETRIEVE_INVOICES.value,
                                    'payload': {
                                        'next_page_token': next_page_token
                                    }
                                }),
                            }
                        }
                    ]
                }
            ]
        }
        url = reverse(self.view_name)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
