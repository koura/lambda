from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from importlib import import_module

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sessions.models import Session

from koura.businesses.factories import BusinessFactory
from koura.businesses.models import Client
from koura.lex import handler
from koura.lex.enums import InvocationSource, ContentType, Intent as IntentName
from koura.users.factories import UserFactory
from . import IntentTestCase

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


class CreateClientIntentTestCase(IntentTestCase):
    intent_name = IntentName.CREATE_CLIENT.value

    def setUp(self):
        self.maxDiff = None
        self.business = BusinessFactory()
        self.user = self.business.user
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.DIALOG_CODE_HOOK.value,
            'userId': uuid.uuid4().hex,
            'sessionAttributes': None,
            'bot': {
                'name': 'Koura',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'CreateClient',
                'slots': {
                    'emailAddress': None,
                    'fullName': None,
                },
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'Add client'
        }
        self.context = {
        }
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        Session.objects.all().delete()
        User.objects.all().delete()

    def test_init(self):
        event = self.event.copy()
        self._init_slots({
            'email_address': None,
            'full_name': None,
        })
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'emailAddress': None,
                    'fullName': None
                },
                'intentName': 'CreateClient',
                'slotToElicit': 'emailAddress'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_elicit_full_name(self):
        event = self.event.copy()
        event['inputTranscript'] = 'emeka@something.com'
        self.session['intents'] = {
            'CreateClient': {
                'num_events': 3
            }
        }
        self._init_slots({
            'email_address': event['inputTranscript'],
            'full_name': None,
        })
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'emailAddress': 'emeka@something.com',
                    'fullName': None
                },
                'intentName': 'CreateClient',
                'slotToElicit': 'fullName'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_email_address(self):
        event = self.event.copy()
        event['inputTranscript'] = 'emekasomethingcom'
        self.session['intents'] = {
            'CreateClient': {
                'num_events': 3
            }
        }
        self._init_slots({
            'email_address': event['inputTranscript'],
            'full_name': None,
        })
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'emailAddress': None,
                    'fullName': None
                },
                'intentName': 'CreateClient',
                'slotToElicit': 'emailAddress'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_duplicate_email_address(self):
        email_address = 'emeka@google.com'
        Client.objects.create(business=self.business, full_name='fadfdsfasdfasd', email_address=email_address)
        event = self.event.copy()
        event['inputTranscript'] = email_address
        self.session['intents'] = {
            'CreateClient': {
                'num_events': 3
            }
        }
        self._init_slots({
            'email_address': event['inputTranscript'],
            'full_name': None,
        })
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'emailAddress': None,
                    'fullName': None
                },
                'intentName': 'CreateClient',
                'slotToElicit': 'emailAddress'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_fulfillment(self):
        event = self.event.copy()
        event['inputTranscript'] = 'Chukwuemeka Ezekwe'
        self.session['intents'] = {
            'CreateClient': {
                'num_events': 5
            }
        }
        self._init_slots({
            'email_address': 'emeka@something.com',
            'full_name': event['inputTranscript'],
        })
        self.session.save()
        
        session_attributes = event.get('sessionAttributes').copy()
        expected_outbox = {
            ContentType.MESSENGER: [
                {'text': 'I have added Chukwuemeka Ezekwe to your account'},
                {'text': 'Invoices for this client will be sent to emeka@something.com'},
                {'text': 'You can send an invoice to your client by saying a phrase like \'Create invoice\''}
            ]
        }
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertDictEqual(expected_outbox, self.session['outbox'])
        self.assertTrue(Client.objects.all().count() == 1)
        self.assertTrue(Client.objects.filter(business=self.business, email_address='emeka@something.com').exists())
        self.assertDictNotContainsNull(response)

    def test_business_required(self):
        event = self.event.copy()
        user = UserFactory()
        session = SessionStore()
        session['user_id'] = user.id
        session.create()
        event['sessionAttributes'] = {
            'key': session.session_key
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        session = SessionStore(session_key=session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertTrue('outbox' in session)
