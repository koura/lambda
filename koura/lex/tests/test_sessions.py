# from __future__ import absolute_import
# from __future__ import division
# from __future__ import print_function
# from __future__ import unicode_literals
#
# import uuid
# from importlib import import_module
#
# from django.conf import settings
# from django.test import TestCase
#
# from koura.lex import handler
# from koura.lex_sessions.models import LexSession
#
# from unittest import skip
# SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
#
#
# @skip('Not needed')
# class LexSessionTestCase(TestCase):
#     def setUp(self):
#         self.event = {
#             "currentIntent": {
#                 "slots": {
#                     "AppointmentType": "whitening",
#                     "Date": "2030-11-08",
#                     "Time": "10:00"
#                 },
#                 "name": "CreateUser",
#                 "confirmationStatus": "None"
#             },
#             "bot": {
#                 "alias": "$LATEST",
#                 "version": "$LATEST",
#                 "name": "MakeAppointment"
#             },
#             "userId": uuid.uuid4().hex,
#             "invocationSource": "DialogCodeHook",
#             "outputDialogMode": "Text",
#             "messageVersion": "1.0",
#             "sessionAttributes": {}
#         }
#         self.context = {
#
#         }
#         self.assertEqual(LexSession.objects.all().count(), 0)
#
#     def tearDown(self):
#         LexSession.objects.all().delete()
#
#     def test_lex_session_creation(self):
#         handler(event=self.event, context=self.context)
#         self.assertTrue(LexSession.objects.filter(lex_user_id=self.event.get('userId')).exists())
#         self.assertEqual(LexSession.objects.all().count(), 1)
#
#     def test_lex_session_duplicates(self):
#         for i in range(10):
#             handler(event=self.event, context=self.context)
#         self.assertTrue(LexSession.objects.filter(lex_user_id=self.event.get('userId')).exists())
#         self.assertEqual(LexSession.objects.all().count(), 1)
