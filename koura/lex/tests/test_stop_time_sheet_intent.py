from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from importlib import import_module

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sessions.models import Session
from django.utils import timezone
from psycopg2.extras import DateTimeTZRange

from koura.businesses.factories import BusinessFactory, TimeSheetFactory, TimeSheetEntryFactory
from koura.lex import handler
from koura.lex.enums import InvocationSource, Intent as IntentName
from . import IntentTestCase

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


class StopTimeSheetIntentTestCase(IntentTestCase):
    intent_name = IntentName.STOP_TIME_SHEET.value

    def setUp(self):
        self.business = BusinessFactory()
        self.user = self.business.user
        self.time_sheet = TimeSheetFactory(business=self.business)
        for i in range(10):
            TimeSheetEntryFactory(time_sheet=self.time_sheet)
        TimeSheetEntryFactory(time_sheet=self.time_sheet, time_range=DateTimeTZRange(lower=timezone.now()))
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.FULFILLMENT_CODE_HOOK.value,
            'userId': uuid.uuid4().hex,
            'sessionAttributes': None,
            'bot': {
                'name': 'Koura',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'StopTimeSheet',
                'slots': {
                },
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'Stop timer'
        }
        self.context = {
        }
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        Session.objects.all().delete()
        User.objects.all().delete()

    def test_fulfillment(self):
        event = self.event.copy()
        self.session['intents'] = {
        }
        self.session.save()

        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        self.assertFalse(self.time_sheet.is_timing)

    def test_failed(self):
        self.time_sheet.stop_timer()
        event = self.event.copy()
        self.session['intents'] = {
        }
        self.session.save()

        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        self.assertFalse(self.time_sheet.is_timing)


