from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from importlib import import_module

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sessions.models import Session

from koura.businesses.factories import BusinessFactory, ClientFactory, TimeSheetFactory
from koura.businesses.models import Client, Business
from koura.lex import handler
from koura.lex.enums import InvocationSource, Intent as IntentName
from koura.users.factories import UserFactory
from . import IntentTestCase

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


class RetrieveTimeSheetsIntentTestCase(IntentTestCase):
    intent_name = IntentName.RETRIEVE_TIME_SHEETS.value
    
    @classmethod
    def setUpClass(cls):
        super(RetrieveTimeSheetsIntentTestCase, cls).setUpClass()
        business = BusinessFactory()
        client = ClientFactory(business=business)
        for i in range(20):
            TimeSheetFactory(business=business, client=client)

    @classmethod
    def tearDownClass(cls):
        super(RetrieveTimeSheetsIntentTestCase, cls).tearDownClass()
        User.objects.all().delete()

    def setUp(self):
        self.maxDiff = None
        self.business = Business.objects.all()[0]
        self.user = self.business.user
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.DIALOG_CODE_HOOK.value,
            'userId': uuid.uuid4().hex,
            'sessionAttributes': None,
            'bot': {
                'name': 'Koura',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'RetrieveTimeSheets',
                'slots': {
                },
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'Get mah time sheetz'
        }
        self.context = {
        }
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        Session.objects.all().delete()

    def test_init(self):
        event = self.event.copy()
        event['inputTranscript'] = 'View time sheets'
        self.session['intents'] = {
            'RetrieveTimeSheets': {
                'num_events': 0
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_fulfillment(self):
        client = Client.objects.all()[0]
        event = self.event.copy()
        event['inputTranscript'] = str(client.id)
        self.session['intents'] = {
            'RetrieveTimeSheets': {
                'num_events': 1,
                'slots': {
                    'query': client.full_name,
                    'client': None
                },
                'last_form_errors': {
                    'client': '',
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_business_required(self):
        event = self.event.copy()
        user = UserFactory()
        session = SessionStore()
        session['user_id'] = user.id
        session.create()
        event['sessionAttributes'] = {
            'key': session.session_key
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        session = SessionStore(session_key=session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertTrue('outbox' in session)
        user.delete()

    def test_time_sheets_required(self):
        event = self.event.copy()
        business = BusinessFactory()
        user = business.user
        session = SessionStore()
        session['user_id'] = user.id
        session.create()
        event['sessionAttributes'] = {
            'key': session.session_key
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        session = SessionStore(session_key=session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertTrue('outbox' in session)
        user.delete()
