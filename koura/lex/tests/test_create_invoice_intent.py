from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from importlib import import_module

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sessions.models import Session

from koura.businesses.factories import ClientFactory
from koura.businesses.models import Invoice, Client
from koura.lex import handler
from koura.lex.enums import InvocationSource, Intent as IntentName
from koura.users.factories import UserFactory
from . import IntentTestCase

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


class CreateInvoiceIntentTestCase(IntentTestCase):
    intent_name = IntentName.CREATE_INVOICE.value

    def setUp(self):
        self.maxDiff = None
        self.client = ClientFactory()
        self.business = self.client.business
        self.user = self.client.business.user
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.DIALOG_CODE_HOOK.value,
            'userId': uuid.uuid4().hex,
            'sessionAttributes': None,
            'bot': {
                'name': 'Koura',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'CreateInvoice',
                'slots': {
                    'lineItems': None,
                    'client': None,
                    'query': None,
                    'doConfirm': None
                },
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'Create invoice'
        }
        self.context = {
        }
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        Session.objects.all().delete()
        User.objects.all().delete()

    def test_init(self):
        event = self.event.copy()
        self.session['intents'] = {
            'CreateInvoice': {
                'num_events': 0,
                'slots': {
                    'lineItems': None,
                    'client': None,
                    'query': None,
                    'do_confirm': None,
                },
                'last_form_errors': {
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'lineItems': None,
                    'client': None,
                    'query': None,
                    'doConfirm': None,
                },
                'intentName': 'CreateInvoice',
                'slotToElicit': 'query'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_query(self):
        event = self.event.copy()
        event['inputTranscript'] = 'fcewaf4wfewfwa'
        self.session['intents'] = {
            'CreateInvoice': {
                'num_events': 2,
                'slots': {
                    'line_items': None,
                    'client': None,
                    'query': None,
                    'do_confirm': None,
                }
            }
        }
        self.session.save()

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'lineItems': None,
                    'client': None,
                    'query': None,
                    'doConfirm': None,
                },
                'intentName': 'CreateInvoice',
                'slotToElicit': 'query'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_client(self):
        event = self.event.copy()
        event['inputTranscript'] = self.client.id
        self.session['intents'] = {
            'CreateInvoice': {
                'num_events': 2,
                'slots': {
                    'line_items': None,
                    'client': None,
                    'query': self.client.email_address,
                    'do_confirm': None,
                },
                'last_form_errors': {
                    'line_items': '',
                    'client': '',
                    'do_confirm': '',
                }
            }
        }
        self.session.save()

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'lineItems': None,
                    'client': self.client.id,
                    'query': self.client.email_address,
                    'doConfirm': None,
                },
                'intentName': 'CreateInvoice',
                'slotToElicit': 'lineItems'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
    
    def test_invalid_client(self):
        event = self.event.copy()
        event['inputTranscript'] = 12345
        self.session['intents'] = {
            'CreateInvoice': {
                'num_events': 2,
                'slots': {
                    'line_items': None,
                    'client': None,
                    'query': self.client.email_address,
                    'do_confirm': None,
                },
                'last_form_errors': {
                    'line_items': '',
                    'client': '',
                    'do_confirm': '',
                }
            }
        }
        self.session.save()

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'lineItems': None,
                    'client': None,
                    'query': self.client.email_address,
                    'doConfirm': None,
                },
                'intentName': 'CreateInvoice',
                'slotToElicit': 'client'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_line_items(self):
        event = self.event.copy()
        event['inputTranscript'] = 'I write the codez $100\nAnd then some $250'
        self.session['intents'] = {
            'CreateInvoice': {
                'num_events': 2,
                'slots': {
                    'line_items': None,
                    'client': self.client.id,
                    'query': self.client.email_address,
                    'do_confirm': None,
                },
                'last_form_errors': {
                    'line_items': '',
                    'do_confirm': '',
                }
            }
        }
        self.session.save()

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'lineItems': event['inputTranscript'],
                    'client': self.client.id,
                    'query': self.client.email_address,
                    'doConfirm': None,
                },
                'intentName': 'CreateInvoice',
                'slotToElicit': 'doConfirm'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_line_items(self):
        event = self.event.copy()
        event['inputTranscript'] = 'dsafds0'
        self.session['intents'] = {
            'CreateInvoice': {
                'num_events': 2,
                'slots': {
                    'line_items': None,
                    'client': self.client.id,
                    'query': self.client.email_address,
                    'do_confirm': None,
                },
                'last_form_errors': {
                    'line_items': '',
                    'do_confirm': '',
                }
            }
        }
        self.session.save()

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'lineItems': None,
                    'client': self.client.id,
                    'query': self.client.email_address,
                    'doConfirm': None,
                },
                'intentName': 'CreateInvoice',
                'slotToElicit': 'lineItems'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_failure(self):
        event = self.event.copy()
        event['inputTranscript'] = 'No'
        self.session['intents'] = {
            'CreateInvoice': {
                'num_events': 5,
                'slots': {
                    'line_items': 'I write the codez $100',
                    'client': self.client.id,
                    'query': self.client.full_name,
                    'do_confirm': None
                },
                'last_form_errors': {
                    'do_confirm': ''
                }
            }
        }
        self.session.save()
        
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertFalse(Invoice.objects.all().exists())
        self.assertDictNotContainsNull(response)

    def test_fulfillment(self):
        event = self.event.copy()
        event['inputTranscript'] = 'Yes'
        self.session['intents'] = {
            'CreateInvoice': {
                'num_events': 5,
                'slots': {
                    'line_items': 'I write the codez $100',
                    'client': self.client.id,
                    'query': self.client.full_name,
                    'do_confirm': None
                },
                'last_form_errors': {
                    'do_confirm': ''
                }
            }
        }
        self.session.save()
        
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertTrue(Invoice.objects.all().count() == 1)
        self.assertTrue(Invoice.objects.filter(business=self.business, client=self.client).exists())
        self.assertDictNotContainsNull(response)

    def test_business_required(self):
        event = self.event.copy()
        user = UserFactory()
        session = SessionStore()
        session['user_id'] = user.id
        session.create()
        event['sessionAttributes'] = {
            'key': session.session_key
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        session = SessionStore(session_key=session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertTrue('outbox' in session)

    def test_clients_required(self):
        event = self.event.copy()
        Client.objects.all().delete()
        self.session['intents'] = {
            'CreateInvoice': {
                'num_events': 0,
                'slots': {
                    'line_items': None,
                    'client': None,
                    'query': None,
                    'do_confirm': None,
                },
                'last_form_errors': {
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
