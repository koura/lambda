from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from importlib import import_module

import random
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sessions.models import Session

from koura.businesses.factories import BusinessFactory, InvoiceFactory
from koura.businesses.models import Business
from koura.lex import handler
from koura.lex.enums import InvocationSource, Intent as IntentName
from koura.users.factories import UserFactory
from . import IntentTestCase

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


class RetrieveInvoicesIntentTestCase(IntentTestCase):
    intent_name = IntentName.RETRIEVE_INVOICES.value
    
    @classmethod
    def setUpClass(cls):
        super(RetrieveInvoicesIntentTestCase, cls).setUpClass()
        business = BusinessFactory()
        for i in range(50):
            InvoiceFactory(business=business, stripe_charge_id=random.choice(['', uuid.uuid4().hex]))

    @classmethod
    def tearDownClass(cls):
        super(RetrieveInvoicesIntentTestCase, cls).tearDownClass()
        User.objects.all().delete()

    def setUp(self):
        self.maxDiff = None
        self.business = Business.objects.all()[0]
        self.user = self.business.user
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.DIALOG_CODE_HOOK.value,
            'userId': uuid.uuid4().hex,
            'sessionAttributes': None,
            'bot': {
                'name': 'Koura',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'RetrieveInvoices',
                'slots': {
                    'filter': None,
                },
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'All'
        }
        self.context = {
        }
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        Session.objects.all().delete()

    def test_init(self):
        event = self.event.copy()
        event['inputTranscript'] = 'Show me invoices'
        self.session['intents'] = {
            'RetrieveInvoices': {
                'num_events': 0
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'filter': None,
                },
                'intentName': 'RetrieveInvoices',
                'slotToElicit': 'filter'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
    
    def test_invalid_filter(self):
        event = self.event.copy()
        event['inputTranscript'] = 'fjlwajlewa'
        self.session['intents'] = {
            'RetrieveInvoices': {
                'num_events': 1
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'filter': None,
                },
                'intentName': 'RetrieveInvoices',
                'slotToElicit': 'filter'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_lex_fulfillment(self):
        event = self.event.copy()
        event['inputTranscript'] = 'Show outstanding invoices'
        event['invocationSource'] = InvocationSource.FULFILLMENT_CODE_HOOK.value
        event['currentIntent'] = {
            'name': 'RetrieveInvoices',
            'slots': {
                'filter': 'outstanding',
            },
            'confirmationStatus': 'None'
        }
        self.session['intents'] = {
            'RetrieveInvoices': {
                'num_events': 0
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        
    def test_initial_fulfillment(self):
        event = self.event.copy()
        event['inputTranscript'] = 'Show outstanding invoices'
        event['currentIntent'] = {
            'name': 'RetrieveInvoices',
            'slots': {
                'filter': 'outstanding',
            },
            'confirmationStatus': 'None'
        }
        self.session['intents'] = {
            'RetrieveInvoices': {
                'num_events': 0
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_fulfillment(self):
        event = self.event.copy()
        event['inputTranscript'] = 'All'
        self.session['intents'] = {
            'RetrieveInvoices': {
                'num_events': 1,
                'slots': {
                    'filter': None,
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_business_required(self):
        event = self.event.copy()
        user = UserFactory()
        session = SessionStore()
        session['user_id'] = user.id
        session.create()
        event['sessionAttributes'] = {
            'key': session.session_key
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        session = SessionStore(session_key=session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertTrue('outbox' in session)
        user.delete()

    def test_invoices_required(self):
        event = self.event.copy()
        business = BusinessFactory()
        user = business.user
        session = SessionStore()
        session['user_id'] = user.id
        session.create()
        event['sessionAttributes'] = {
            'key': session.session_key
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        session = SessionStore(session_key=session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertTrue('outbox' in session)
        user.delete()

    def test_change_intent(self):
        event = self.event.copy()
        event['inputTranscript'] = 'All'
        self.session['current_intent'] = IntentName.CREATE_BUSINESS.value
        self.session['intents'] = {
            'RetrieveInvoices': {
                'num_events': 0,
                'slots': {
                    'filter': None,
                }
            },
            'CreateBusiness': {
                'num_events': 1,
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'street_address': '27 Treasure Cove Drive',
                    'locality': 'Spring',
                    'region': 'TX',
                    'postal_code': '21045',
                    'country': None,
                    'has_logo': None,
                    'logo_url': None,
                    'do_accept_terms_of_service': None,
                },
                'last_form_errors': {
                    'country': '',
                    'has_logo': '',
                    'logo_url': '',
                    'do_accept_terms_of_service': '',
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertEqual(len(self.session['intents'].keys()), 1)
        self.assertTrue('RetrieveInvoices' in self.session['intents'])
