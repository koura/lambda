from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from importlib import import_module

import factory
import stripe
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sessions.models import Session

from koura.businesses.forms import CreateBankPayoutForm
from koura.businesses.forms import CreateBusinessForm
from koura.lex import handler
from koura.lex.enums import InvocationSource, Intent as IntentName
from koura.users.factories import UserFactory
from . import IntentTestCase

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
stripe.api_key = settings.STRIPE_SECRET_KEY


class CreateBankPayoutIntentTestCase(IntentTestCase):
    intent_name = IntentName.CREATE_BANK_PAYOUT.value

    def setUp(self):
        self.maxDiff = None
        self.user = UserFactory()
        form = CreateBusinessForm(data={
            'user': self.user.id,
            'name': factory.Faker('company'),
            'has_logo': 'Yes',
            'logo_url': 'https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png',
            'do_accept_terms_of_service': 'Yes',

            'street_address': '221B Baker Street\nApartment #2',
            'locality': 'London',
            'region': 'California',
            'postal_code': '20444',
            'country': 'US',
        })
        self.assertTrue(form.is_valid())
        self.business = form.save()
        self.user = self.business.user
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session.create()

        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.DIALOG_CODE_HOOK.value,
            'userId': uuid.uuid4().hex,
            'sessionAttributes': None,
            'bot': {
                'name': 'Koura',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'CreateBankPayout',
                'slots': {
                    'dateOfBirth': None,
                    'taxId': None,
                    'accountNumber': None,
                    'routingNumber': None,
                    'ssnLastFour': None,
                },
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'I want to get paid!'
        }
        self.context = {
        }
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        Session.objects.all().delete()
        User.objects.all().delete()

    def test_init(self):
        event = self.event.copy()
        self.session['intents'] = {
            'CreateBankPayout': {
                'num_events': 0,
                'slots': {
                    'date_of_birth': None,
                    'tax_id': None,
                    'account_number': None,
                    'routing_number': None,
                    'ssn_last_four': None,
                }
            }
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'dateOfBirth': None,
                    'taxId': None,
                    'accountNumber': None,
                    'routingNumber': None,
                    'ssnLastFour': None,
                },
                'intentName': 'CreateBankPayout',
                'slotToElicit': 'dateOfBirth'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_date_of_birth(self):
        event = self.event.copy()
        event['inputTranscript'] = '8/23/86'
        self.session['intents'] = {
            'CreateBankPayout': {
                'num_events': 1,
                'slots': {
                    'date_of_birth': None,
                    'tax_id': None,
                    'account_number': None,
                    'routing_number': None,
                    'ssn_last_four': None,
                },
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'dateOfBirth': '8/23/86',
                    'taxId': None,
                    'accountNumber': None,
                    'routingNumber': None,
                    'ssnLastFour': None,
                },
                'intentName': 'CreateBankPayout',
                'slotToElicit': 'taxId'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_date_of_birth(self):
        event = self.event.copy()
        event['inputTranscript'] = '1986'
        self.session['intents'] = {
            'CreateBankPayout': {
                'num_events': 1,
                'slots': {
                    'date_of_birth': None,
                    'tax_id': None,
                    'account_number': None,
                    'routing_number': None,
                    'ssn_last_four': None,
                }
            }
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'dateOfBirth': None,
                    'taxId': None,
                    'accountNumber': None,
                    'routingNumber': None,
                    'ssnLastFour': None,
                },
                'intentName': 'CreateBankPayout',
                'slotToElicit': 'dateOfBirth'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
    
    def test_valid_tax_id(self):
        event = self.event.copy()
        event['inputTranscript'] = '123456789'
        self.session['intents'] = {
            'CreateBankPayout': {
                'num_events': 1,
                'slots': {
                    'date_of_birth': '8/23/86',
                    'tax_id': None,
                    'account_number': None,
                    'routing_number': None,
                    'ssn_last_four': None,
                },
                'last_form_errors': {
                    'tax_id': '',
                    'account_number': '',
                    'routing_number': '',
                    'ssn_last_four': '',
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'dateOfBirth': '8/23/86',
                    'taxId': event['inputTranscript'],
                    'accountNumber': None,
                    'routingNumber': None,
                    'ssnLastFour': None,
                },
                'intentName': 'CreateBankPayout',
                'slotToElicit': 'accountNumber'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_tax_id(self):
        event = self.event.copy()
        event['inputTranscript'] = '111'
        self.session['intents'] = {
            'CreateBankPayout': {
                'num_events': 1,
                'slots': {
                    'date_of_birth': '8/23/86',
                    'tax_id': None,
                    'account_number': None,
                    'routing_number': None,
                    'ssn_last_four': None,
                },
                'last_form_errors': {
                    'tax_id': '',
                    'account_number': '',
                    'routing_number': '',
                    'ssn_last_four': ''
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'dateOfBirth': '8/23/86',
                    'taxId': None,
                    'accountNumber': None,
                    'routingNumber': None,
                    'ssnLastFour': None,
                },
                'intentName': 'CreateBankPayout',
                'slotToElicit': 'taxId'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_account_number(self):
        event = self.event.copy()
        event['inputTranscript'] = '000123456789'
        self.session['intents'] = {
            'CreateBankPayout': {
                'num_events': 1,
                'slots': {
                    'date_of_birth': '8/23/86',
                    'tax_id': '123456789',
                    'account_number': None,
                    'routing_number': None,
                    'ssn_last_four': None,
                },
                'last_form_errors': {
                    'account_number': '',
                    'routing_number': '',
                    'ssn_last_four': '',
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'dateOfBirth': '8/23/86',
                    'taxId': '123456789',
                    'accountNumber': event['inputTranscript'],
                    'routingNumber': None,
                    'ssnLastFour': None,
                },
                'intentName': 'CreateBankPayout',
                'slotToElicit': 'routingNumber'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_routing_number(self):
        event = self.event.copy()
        event['inputTranscript'] = '110000000'
        self.session['intents'] = {
            'CreateBankPayout': {
                'num_events': 1,
                'slots': {
                    'date_of_birth': '8/23/86',
                    'tax_id': '123456789',
                    'account_number': '000123456789',
                    'routing_number': None,
                    'ssn_last_four': None,
                },
                'last_form_errors': {
                    'routing_number': '',
                    'ssn_last_four': '',
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'dateOfBirth': '8/23/86',
                    'taxId': '123456789',
                    'accountNumber': '000123456789',
                    'routingNumber': event['inputTranscript'],
                    'ssnLastFour': None,
                },
                'intentName': 'CreateBankPayout',
                'slotToElicit': 'ssnLastFour'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_routing_number(self):
        event = self.event.copy()
        event['inputTranscript'] = '1986'
        self.session['intents'] = {
            'CreateBankPayout': {
                'num_events': 1,
                'slots': {
                    'date_of_birth': '8/23/86',
                    'tax_id': '123456789',
                    'account_number': '000123456789',
                    'routing_number': None,
                    'ssn_last_four': None,
                },
                'last_form_errors': {
                    'routing_number': '',
                    'ssn_last_four': '',
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'dateOfBirth': '8/23/86',
                    'taxId': '123456789',
                    'accountNumber': '000123456789',
                    'routingNumber': None,
                    'ssnLastFour': None,
                },
                'intentName': 'CreateBankPayout',
                'slotToElicit': 'routingNumber'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_ssn_last_four(self):
        event = self.event.copy()
        event['inputTranscript'] = 'abcd'
        self.session['intents'] = {
            'CreateBankPayout': {
                'num_events': 1,
                'slots': {
                    'date_of_birth': '8/23/86',
                    'tax_id': '123456789',
                    'account_number': '000123456789',
                    'routing_number': '110000000',
                    'ssn_last_four': None,
                },
                'last_form_errors': {
                    'ssn_last_four': '',
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'dateOfBirth': '8/23/86',
                    'taxId': '123456789',
                    'accountNumber': '000123456789',
                    'routingNumber': '110000000',
                    'ssnLastFour': None,
                },
                'intentName': 'CreateBankPayout',
                'slotToElicit': 'ssnLastFour'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_fulfillment(self):
        event = self.event.copy()
        event['inputTranscript'] = '9999'
        self.session['intents'] = {
            'CreateBankPayout': {
                'num_events': 1,
                'slots': {
                    'date_of_birth': '8/23/86',
                    'tax_id': '123456789',
                    'account_number': '000123456789',
                    'routing_number': '110000000',
                    'ssn_last_four': None,
                },
                'last_form_errors': {
                    'ssn_last_four': ''
                }
            }
        }
        self.session.save()
        
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        stripe_account = stripe.Account.retrieve(self.business.stripe_account_id)
        self.assertIsNotNone(stripe_account)
        self.assertEqual(
            '110000000', 
            stripe_account.external_accounts.data[0].routing_number
        )
        self.assertDictNotContainsNull(response)

    def test_business_required(self):
        event = self.event.copy()
        user = UserFactory()
        session = SessionStore()
        session['user_id'] = user.id
        session.create()
        event['sessionAttributes'] = {
            'key': session.session_key
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        session = SessionStore(session_key=session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertTrue('outbox' in session)

    def test_no_bank_account_required(self):
        event = self.event.copy()
        self.session['intents'] = {
            'CreateBankPayout': {
                'num_events': 0,
                'slots': {
                    'date_of_birth': None,
                    'tax_id': None,
                    'account_number': None,
                    'routing_number': None,
                    'ssn_last_four': None,
                }
            }
        }
        self.session.save()
        
        # Create a bank account
        form = CreateBankPayoutForm(data={
            'business': self.business.id,
            'date_of_birth': '8/23/86',
            'tax_id': '11-1111111',
            'account_number': '000123456789',
            'routing_number': '110000000',
            'ssn_last_four': '1453',
        })
        form.submit()
        
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertTrue('outbox' in self.session)
