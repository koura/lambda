from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from importlib import import_module

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sessions.models import Session

from koura.businesses.factories import BusinessFactory
from koura.businesses.models import Business
from koura.lex import handler
from koura.lex.enums import InvocationSource, Intent as IntentName
from koura.users.factories import UserFactory
from . import IntentTestCase

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


class CreateBusinessIntentTestCase(IntentTestCase):
    intent_name = IntentName.CREATE_BUSINESS.value

    def setUp(self):
        self.maxDiff = None
        self.user = UserFactory()
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.DIALOG_CODE_HOOK.value,
            'userId': uuid.uuid4().hex,
            'sessionAttributes': None,
            'bot': {
                'name': 'Koura',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'CreateBusiness',
                'slots': {
                    'name': None,
                    'streetAddress': None,
                    'locality': None,
                    'region': None,
                    'postalCode': None,
                    'country': None,
                    'hasLogo': None,
                    'logoUrl': None,
                    'doAcceptTermsOfService': None,
                },
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'Join'
        }
        self.context = {
        }
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        Session.objects.all().delete()
        User.objects.all().delete()
        Business.objects.all().delete()

    def test_init(self):
        event = self.event.copy()
        self._init_slots({
            'name': None,
            'street_address': None,
            'locality': None,
            'region': None,
            'postal_code': None,
            'country': None,
            'has_logo': None,
            'logo_url': None,
            'do_accept_terms_of_service': None,
        })
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': event.get('currentIntent').get('slots'),
                'intentName': 'CreateBusiness',
                'slotToElicit': 'name'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_unassigned_name(self):
        event = self.event.copy()
        event['inputTranscript'] = 'ez3 Softworks LLC'
        self.session['intents'] = {
            'CreateBusiness': {
                'num_events': 1,
                'slots': {
                    'name': None,
                    'street_address': None,
                    'locality': None,
                    'region': None,
                    'postal_code': None,
                    'country': None,
                    'has_logo': None,
                    'logo_url': None,
                    'do_accept_terms_of_service': None,
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'name': event['inputTranscript'],
                    'streetAddress': None,
                    'locality': None,
                    'region': None,
                    'postalCode': None,
                    'country': None,
                    'hasLogo': None,
                    'logoUrl': None,
                    'doAcceptTermsOfService': None,
                },
                'intentName': 'CreateBusiness',
                'slotToElicit': 'streetAddress'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_street_address(self):
        event = self.event.copy()
        event['inputTranscript'] = '27 Treasure Cove Drive'
        self.session['intents'] = {
            'CreateBusiness': {
                'num_events': 1,
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'street_address': None,
                    'locality': None,
                    'region': None,
                    'postal_code': None,
                    'country': None,
                    'has_logo': None,
                    'logo_url': None,
                    'do_accept_terms_of_service': None,
                },
                'last_form_errors': {
                    'street_address': '',
                    'locality': '',
                    'region': '',
                    'postal_code': '',
                    'country': '',
                    'has_logo': '',
                    'logo_url': '',
                    'do_accept_terms_of_service': '',
                }
            }
        }
        self.session.save()

        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'streetAddress': event['inputTranscript'],
                    'locality': None,
                    'region': None,
                    'postalCode': None,
                    'country': None,
                    'hasLogo': None,
                    'logoUrl': None,
                    'doAcceptTermsOfService': None
                },
                'intentName': 'CreateBusiness',
                'slotToElicit': 'locality'
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertDictNotContainsNull(response)
    
    def test_invalid_street_address(self):
        event = self.event.copy()
        event['inputTranscript'] = '27 Treasure Cove Drive\nApt #5\nRoom #6'
        self.session['intents'] = {
            'CreateBusiness': {
                'num_events': 1,
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'street_address': None,
                    'locality': None,
                    'region': None,
                    'postal_code': None,
                    'country': None,
                    'has_logo': None,
                    'logo_url': None,
                    'do_accept_terms_of_service': None,
                },
                'last_form_errors': {
                    'street_address': '',
                    'locality': '',
                    'region': '',
                    'postal_code': '',
                    'country': '',
                    'has_logo': '',
                    'logo_url': '',
                    'do_accept_terms_of_service': '',
                }
            }
        }
        self.session.save()

        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'streetAddress': None,
                    'locality': None,
                    'region': None,
                    'postalCode': None,
                    'country': None,
                    'hasLogo': None,
                    'logoUrl': None,
                    'doAcceptTermsOfService': None
                },
                'intentName': 'CreateBusiness',
                'slotToElicit': 'streetAddress'
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertDictNotContainsNull(response)

    def test_valid_locality(self):
        event = self.event.copy()
        event['inputTranscript'] = 'Avocado'
        self.session['intents'] = {
            'CreateBusiness': {
                'num_events': 1,
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'street_address': '24 Moon River',
                    'locality': None,
                    'region': None,
                    'postal_code': None,
                    'country': None,
                    'has_logo': None,
                    'logo_url': None,
                    'do_accept_terms_of_service': None,
                },
                'last_form_errors': {
                    'locality': '',
                    'region': '',
                    'postal_code': '',
                    'country': '',
                    'has_logo': '',
                    'logo_url': '',
                    'do_accept_terms_of_service': '',
                }
            }
        }
        self.session.save()

        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'streetAddress': '24 Moon River',
                    'locality': event['inputTranscript'],
                    'region': None,
                    'postalCode': None,
                    'country': None,
                    'hasLogo': None,
                    'logoUrl': None,
                    'doAcceptTermsOfService': None
                },
                'intentName': 'CreateBusiness',
                'slotToElicit': 'region'
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertDictNotContainsNull(response)

    def test_valid_region(self):
        event = self.event.copy()
        event['inputTranscript'] = 'Yuck'
        self.session['intents'] = {
            'CreateBusiness': {
                'num_events': 1,
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'street_address': '24 Moon River',
                    'locality': 'Avocado',
                    'region': None,
                    'postal_code': None,
                    'country': None,
                    'has_logo': None,
                    'logo_url': None,
                    'do_accept_terms_of_service': None,
                },
                'last_form_errors': {
                    'region': '',
                    'postal_code': '',
                    'country': '',
                    'has_logo': '',
                    'logo_url': '',
                    'do_accept_terms_of_service': '',
                }
            }
        }
        self.session.save()

        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'streetAddress': '24 Moon River',
                    'locality': 'Avocado',
                    'region': event['inputTranscript'],
                    'postalCode': None,
                    'country': None,
                    'hasLogo': None,
                    'logoUrl': None,
                    'doAcceptTermsOfService': None
                },
                'intentName': 'CreateBusiness',
                'slotToElicit': 'postalCode'
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertDictNotContainsNull(response)

    def test_valid_country(self):
        event = self.event.copy()
        event['inputTranscript'] = 'US'
        self.session['intents'] = {
            'CreateBusiness': {
                'num_events': 1,
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'street_address': '27 Treasure Cove Drive',
                    'locality': 'Spring',
                    'region': 'TX',
                    'postal_code': '21045',
                    'country': None,
                    'has_logo': None,
                    'logo_url': None,
                    'do_accept_terms_of_service': None,
                },
                'last_form_errors': {
                    'country': '',
                    'has_logo': '',
                    'logo_url': '',
                    'do_accept_terms_of_service': '',
                }
            }
        }
        self.session.save()
        
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'streetAddress': '27 Treasure Cove Drive',
                    'locality': 'Spring',
                    'region': 'TX',
                    'postalCode': '21045',
                    'country': event['inputTranscript'],
                    'hasLogo': None,
                    'logoUrl': None,
                    'doAcceptTermsOfService': None
                },
                'intentName': 'CreateBusiness',
                'slotToElicit': 'hasLogo'
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertDictNotContainsNull(response)

    def test_valid_has_logo(self):
        event = self.event.copy()
        event['inputTranscript'] = 'Yes'
        self.session['intents'] = {
            'CreateBusiness': {
                'num_events': 2,
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'street_address': '27 Treasure Cove Drive',
                    'locality': 'Spring',
                    'region': 'TX',
                    'postal_code': '21045',
                    'country': 'US',
                    'has_logo': None,
                    'logo_url': None,
                    'do_accept_terms_of_service': None,
                },
                'last_form_errors': {
                    'has_logo': '',
                    'do_accept_terms_of_service': '',
                }
            }
        }
        self.session.save()

        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'streetAddress': '27 Treasure Cove Drive',
                    'locality': 'Spring',
                    'region': 'TX',
                    'postalCode': '21045',
                    'country': 'US',
                    'hasLogo': 'Yes',
                    'logoUrl': None,
                    'doAcceptTermsOfService': None,

                },
                'intentName': 'CreateBusiness',
                'slotToElicit': 'logoUrl'
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertDictNotContainsNull(response)

    def test_valid_has_logo_no(self):
        event = self.event.copy()
        event['inputTranscript'] = 'No'
        self.session['intents'] = {
            'CreateBusiness': {
                'num_events': 4,
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'street_address': '27 Treasure Cove Drive',
                    'locality': 'Spring',
                    'region': 'TX',
                    'postal_code': '21045',
                    'country': 'US',
                    'has_logo': None,
                    'logo_url': None,
                    'do_accept_terms_of_service': None,
                },
                'last_form_errors': {
                    'has_logo': '',
                    'do_accept_terms_of_service': '',
                }
            }
        }
        self.session.save()
        
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'streetAddress': '27 Treasure Cove Drive',
                    'locality': 'Spring',
                    'region': 'TX',
                    'postalCode': '21045',
                    'country': 'US',
                    'hasLogo': 'No',
                    'logoUrl': None,
                    'doAcceptTermsOfService': None,

                },
                'intentName': 'CreateBusiness',
                'slotToElicit': 'doAcceptTermsOfService'
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertDictNotContainsNull(response)

    def test_invalid_logo_url(self):
        event = self.event.copy()
        event['inputTranscript'] = 'fdsafdsdfsafdsfdsafdsafdsafdsaf'
        self.session['intents'] = {
            'CreateBusiness': {
                'num_events': 4,
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'street_address': '27 Treasure Cove Drive',
                    'locality': 'Spring',
                    'region': 'TX',
                    'postal_code': '21045',
                    'country': 'US',
                    'has_logo': 'Yes',
                    'logo_url': None,
                    'do_accept_terms_of_service': None,
                },
                'last_form_errors': {
                    'do_accept_terms_of_service': '',
                }
            }
        }
        self.session.save()
        
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'streetAddress': '27 Treasure Cove Drive',
                    'locality': 'Spring',
                    'region': 'TX',
                    'postalCode': '21045',
                    'country': 'US',
                    'hasLogo': 'Yes',
                    'logoUrl': None,
                    'doAcceptTermsOfService': None,

                },
                'intentName': 'CreateBusiness',
                'slotToElicit': 'logoUrl'
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertDictNotContainsNull(response)

    def test_fulfillment(self):
        event = self.event.copy()
        event['inputTranscript'] = 'Yes'
        self.session['intents'] = {
            'CreateBusiness': {
                'num_events': 5,
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'street_address': '221B Baker Street\nApartment #2',
                    'locality': 'London',
                    'region': 'California',
                    'postal_code': '20444',
                    'country': 'US',
                    'has_logo': 'Yes',
                    'logo_url': 'https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png',
                    'do_accept_terms_of_service': None,
                },
                'last_form_errors': {
                    'do_accept_terms_of_service': ''
                }
            }
        }
        self.session.save()

        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertTrue(Business.objects.all().count() == 1)
        self.assertTrue(Business.objects.filter(user=self.user).exists())
        self.assertDictNotContainsNull(response)

    def test_merge_name_from_lex(self):
        event = self.event.copy()
        event['inputTranscript'] = 'ez3 Softworks LLC'
        event['currentIntent']['slots'] = {
            'name': event['inputTranscript'],
            'streetAddress': None,
            'locality': None,
            'region': None,
            'postalCode': None,
            'country': None,
            'hasLogo': None,
            'logoUrl': None,
            'doAcceptTermsOfService': None,
        }
        self.session['intents'] = {
            'CreateBusiness': {
                'num_events': 1,
                'slots': {
                    'name': None,
                    'street_address': None,
                    'locality': None,
                    'region': None,
                    'postal_code': None,
                    'country': None,
                    'has_logo': None,
                    'logo_url': None,
                    'do_accept_terms_of_service': None,
                },
            }
        }
        self.session.save()

        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'streetAddress': None,
                    'locality': None,
                    'region': None,
                    'postalCode': None,
                    'country': None,
                    'hasLogo': None,
                    'logoUrl': None,
                    'doAcceptTermsOfService': None
                },
                'intentName': 'CreateBusiness',
                'slotToElicit': 'streetAddress'
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertDictNotContainsNull(response)

    def test_prevent_duplicate_signup(self):
        event = self.event.copy()
        business = BusinessFactory()
        session = SessionStore()
        session['user_id'] = business.user.id
        session.create()
        event['sessionAttributes'] = {
            'key': session.session_key
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        session = SessionStore(session_key=session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertTrue('outbox' in session)

    def test_multi_line_address(self):
        event = self.event.copy()
        event['inputTranscript'] = '123 Main Street\nBox #5'
        event['currentIntent']['slots'] = {
            'name': 'ez3Softworks LLC',
            'streetAddress': '123 Main Street Box #5',
            'locality': None,
            'region': None,
            'postalCode': None,
            'country': None,
            'hasLogo': None,
            'logoUrl': None,
            'doAcceptTermsOfService': None,
        }
        self.session['intents'] = {
            'CreateBusiness': {
                'num_events': 1,
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'street_address': None,
                    'locality': None,
                    'region': None,
                    'postal_code': None,
                    'country': None,
                    'has_logo': None,
                    'logo_url': None,
                    'do_accept_terms_of_service': None,
                },
                'last_form_errors': {
                    'street_address': '',
                    'locality': '',
                    'region': '',
                    'postal_code': '',
                    'country': '',
                    'has_logo': '',
                    'do_accept_terms_of_service': ''
                }

            }
        }
        self.session.save()

        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'name': 'ez3 Softworks LLC',
                    'streetAddress': '123 Main Street\nBox #5',
                    'locality': None,
                    'region': None,
                    'postalCode': None,
                    'country': None,
                    'hasLogo': None,
                    'logoUrl': None,
                    'doAcceptTermsOfService': None
                },
                'intentName': 'CreateBusiness',
                'slotToElicit': 'locality'
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertDictNotContainsNull(response)
