from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from importlib import import_module

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sessions.models import Session

from koura.businesses.factories import ClientFactory
from koura.businesses.models import Client, TimeSheet
from koura.lex import handler
from koura.lex.enums import InvocationSource, Intent as IntentName
from koura.users.factories import UserFactory
from . import IntentTestCase

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


class CreateTimeSheetIntentTestCase(IntentTestCase):
    intent_name = IntentName.CREATE_TIME_SHEET.value

    def setUp(self):
        self.maxDiff = None
        self.client = ClientFactory()
        self.business = self.client.business
        self.user = self.client.business.user
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.DIALOG_CODE_HOOK.value,
            'userId': uuid.uuid4().hex,
            'sessionAttributes': None,
            'bot': {
                'name': 'Koura',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'CreateTimeSheet',
                'slots': {
                    'description': None,
                    'hourlyRate': None,
                    'client': None,
                    'query': None,
                },
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'Create time sheet'
        }
        self.context = {
        }
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        Session.objects.all().delete()
        User.objects.all().delete()

    def test_init(self):
        event = self.event.copy()
        self.session['intents'] = {
            'CreateTimeSheet': {
                'num_events': 0,
                'slots': {
                    'description': None,
                    'hourly_rate': None,
                    'client': None,
                    'query': None,
                },
                'last_form_errors': {
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'description': None,
                    'hourlyRate': None,
                    'client': None,
                    'query': None,
                },
                'intentName': 'CreateTimeSheet',
                'slotToElicit': 'query'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_query(self):
        event = self.event.copy()
        event['inputTranscript'] = 'fcewaf4wfewfwa'
        self.session['intents'] = {
            'CreateTimeSheet': {
                'num_events': 2,
                'slots': {
                    'description': None,
                    'hourly_rate': None,
                    'client': None,
                    'query': None,
                },
                'last_form_errors': {
                    'description': '',
                    'hourly_rate': '',
                    'client': '',
                    'query': '',
                }
            }
        }
        self.session.save()

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'description': None,
                    'hourlyRate': None,
                    'client': None,
                    'query': None,
                },
                'intentName': 'CreateTimeSheet',
                'slotToElicit': 'query'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_client(self):
        event = self.event.copy()
        event['inputTranscript'] = self.client.id
        self.session['intents'] = {
            'CreateTimeSheet': {
                'num_events': 2,
                'slots': {
                    'description': None,
                    'hourly_rate': None,
                    'client': None,
                    'query': self.client.email_address,
                },
                'last_form_errors': {
                    'description': '',
                    'client': '',
                    'hourly_rate': '',
                }
            }
        }
        self.session.save()

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'client': self.client.id,
                    'query': self.client.email_address,
                    'description': None,
                    'hourlyRate': None,
                },
                'intentName': 'CreateTimeSheet',
                'slotToElicit': 'description'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
    
    def test_invalid_client(self):
        event = self.event.copy()
        event['inputTranscript'] = 1234512121
        self.session['intents'] = {
            'CreateTimeSheet': {
                'num_events': 2,
                'slots': {
                    'description': None,
                    'hourly_rate': None,
                    'client': None,
                    'query': self.client.email_address,
                },
                'last_form_errors': {
                    'description': '',
                    'client': '',
                    'hourly_rate': '',
                }
            }
        }
        self.session.save()

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'client': None,
                    'query': self.client.email_address,
                    'description': None,
                    'hourlyRate': None,
                },
                'intentName': 'CreateTimeSheet',
                'slotToElicit': 'client'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_description(self):
        event = self.event.copy()
        event['inputTranscript'] = 'I wrote some codez'
        self.session['intents'] = {
            'CreateTimeSheet': {
                'num_events': 2,
                'slots': {
                    'description': None,
                    'hourly_rate': None,
                    'client': self.client.id,
                    'query': self.client.email_address,
                },
                'last_form_errors': {
                    'description': '',
                    'hourly_rate': '',
                }
            }
        }
        self.session.save()

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'client': self.client.id,
                    'query': self.client.email_address,
                    'description': event['inputTranscript'],
                    'hourlyRate': None,
                },
                'intentName': 'CreateTimeSheet',
                'slotToElicit': 'hourlyRate'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
    
    def test_invalid_hourly_rate(self):
        event = self.event.copy()
        event['inputTranscript'] = '1 cup of coffee'
        self.session['intents'] = {
            'CreateTimeSheet': {
                'num_events': 2,
                'slots': {
                    'description': 'Code writing lol',
                    'hourly_rate': None,
                    'client': self.client.id,
                    'query': self.client.email_address,
                },
                'last_form_errors': {
                    'hourly_rate': '',
                }
            }
        }
        self.session.save()

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'client': self.client.id,
                    'query': self.client.email_address,
                    'description': 'Code writing lol',
                    'hourlyRate': None,
                },
                'intentName': 'CreateTimeSheet',
                'slotToElicit': 'hourlyRate'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_fulfillment(self):
        event = self.event.copy()
        event['inputTranscript'] = '$350'
        self.session['intents'] = {
            'CreateTimeSheet': {
                'num_events': 5,
                'slots': {
                    'description': 'I write elite codez',
                    'hourly_rate': None,
                    'client': self.client.id,
                    'query': self.client.full_name,
                },
                'last_form_errors': {
                    'hourly_rate': ''
                }
            }
        }
        self.session.save()
        
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertEqual(TimeSheet.objects.all().count(), 1)
        self.assertTrue(TimeSheet.objects.filter(business=self.business, client=self.client).exists())
        self.assertDictNotContainsNull(response)

    def test_business_required(self):
        event = self.event.copy()
        user = UserFactory()
        session = SessionStore()
        session['user_id'] = user.id
        session.create()
        event['sessionAttributes'] = {
            'key': session.session_key
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        session = SessionStore(session_key=session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertTrue('outbox' in session)

    def test_clients_required(self):
        event = self.event.copy()
        Client.objects.all().delete()
        self.session['intents'] = {
            'CreateTimeSheet': {
                'num_events': 0,
                'slots': {
                    'description': None,
                    'hourly_rate': None,
                    'client': None,
                    'query': None,
                },
                'last_form_errors': {
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
