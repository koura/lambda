from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from unittest import mock


def boto3_client(*args, **kwargs):
    client = mock.MagicMock()
    client.post_text = mock.MagicMock(return_value={})
    return client