from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from importlib import import_module

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sessions.models import Session

from koura.businesses.factories import ClientFactory, TimeSheetFactory, TimeSheetEntryFactory
from koura.businesses.models import Invoice, Client
from koura.lex import handler
from koura.lex.enums import InvocationSource, Intent as IntentName
from koura.users.factories import UserFactory
from . import IntentTestCase
from psycopg2.extras import DateTimeTZRange
from django.utils import timezone
from datetime import timedelta

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


class CreateInvoiceWithTimeSheetIntentTestCase(IntentTestCase):
    intent_name = IntentName.CREATE_INVOICE_WITH_TIME_SHEET.value

    def setUp(self):
        self.maxDiff = None
        self.client = ClientFactory()
        self.business = self.client.business
        self.time_sheet = TimeSheetFactory(business=self.business, client=self.client, hourly_rate=50 * 100)
        for i in range(10):
            lower = timezone.now() - timedelta(hours=i * 24 * 30)
            upper = timezone.now() - timedelta(hours=i * 24 * 30 - 5)
            TimeSheetEntryFactory(
                time_sheet=self.time_sheet,
                time_range=DateTimeTZRange(lower=lower, upper=upper)
            )
        self.user = self.client.business.user
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.DIALOG_CODE_HOOK.value,
            'userId': uuid.uuid4().hex,
            'sessionAttributes': None,
            'bot': {
                'name': 'Koura',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'CreateInvoiceWithTimeSheet',
                'slots': {
                    'lineItems': None,
                    'doConfirm': None,
                    'doElicitLineItems': None
                },
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'Create invoice with time sheet'
        }
        self.context = {
        }
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        Session.objects.all().delete()
        User.objects.all().delete()

    def test_init(self):
        event = self.event.copy()
        self.session['intents'] = {
            'CreateInvoiceWithTimeSheet': {
                'num_events': 0,
                'slots': {
                    'do_elicit_line_items': None,
                    'line_items': None,
                    'do_confirm': None,
                },
                'last_form_errors': {
                },
                'initial_data': {
                    'query': self.client.full_name,
                    'client': self.client.id,
                    'time_sheet': self.time_sheet.id,
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'doElicitLineItems': None,
                    'lineItems': None,
                    'doConfirm': None,
                },
                'intentName': 'CreateInvoiceWithTimeSheet',
                'slotToElicit': 'doElicitLineItems'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_elicit_line_items(self):
        event = self.event.copy()
        event['inputTranscript'] = 'Yes'
        self.session['intents'] = {
            'CreateInvoiceWithTimeSheet': {
                'num_events': 1,
                'slots': {
                    'do_elicit_line_items': None,
                    'line_items': None,
                    'do_confirm': None,
                },
                'last_form_errors': {
                },
                'initial_data': {
                    'query': self.client.full_name,
                    'client': self.client.id,
                    'time_sheet': self.time_sheet.id,
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'doElicitLineItems': 'Yes',
                    'lineItems': None,
                    'doConfirm': None,
                },
                'intentName': 'CreateInvoiceWithTimeSheet',
                'slotToElicit': 'lineItems'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_dont_elicit_line_items(self):
        event = self.event.copy()
        event['inputTranscript'] = 'No'
        self.session['intents'] = {
            'CreateInvoiceWithTimeSheet': {
                'num_events': 1,
                'slots': {
                    'do_elicit_line_items': None,
                    'line_items': None,
                    'do_confirm': None,
                },
                'last_form_errors': {
                },
                'initial_data': {
                    'query': self.client.full_name,
                    'client': self.client.id,
                    'time_sheet': self.time_sheet.id,
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'doElicitLineItems': 'No',
                    'lineItems': None,
                    'doConfirm': None,
                },
                'intentName': 'CreateInvoiceWithTimeSheet',
                'slotToElicit': 'doConfirm'
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_failure(self):
        event = self.event.copy()
        event['inputTranscript'] = 'No'
        self.session['intents'] = {
            'CreateInvoiceWithTimeSheet': {
                'num_events': 5,
                'slots': {
                    'do_elicit_line_items': 'Yes',
                    'line_items': 'I write the codez $100',
                    'do_confirm': None
                },
                'last_form_errors': {
                    'do_confirm': ''
                },
                'initial_data': {
                    'query': self.client.full_name,
                    'client': self.client.id,
                    'time_sheet': self.time_sheet.id,
                }
            }
        }
        self.session.save()
        
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertFalse(Invoice.objects.all().exists())
        self.assertDictNotContainsNull(response)

    def test_fulfillment(self):
        event = self.event.copy()
        event['inputTranscript'] = 'Yes'
        self.session['intents'] = {
            'CreateInvoiceWithTimeSheet': {
                'num_events': 5,
                'slots': {
                    'do_elicit_line_items': 'Yes',
                    'line_items': 'I write the codez $100',
                    'do_confirm': None
                },
                'last_form_errors': {
                    'do_confirm': ''
                },
                'initial_data': {
                    'query': self.client.full_name,
                    'client': self.client.id,
                    'time_sheet': self.time_sheet.id,
                }
            }
        }
        self.session.save()
        
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = handler(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertTrue('outbox' in self.session)
        self.assertTrue(Invoice.objects.all().count() == 1)
        self.assertTrue(Invoice.objects.filter(business=self.business, client=self.client).exists())
        self.assertDictNotContainsNull(response)

    def test_business_required(self):
        event = self.event.copy()
        user = UserFactory()
        session = SessionStore()
        session['user_id'] = user.id
        session.create()
        event['sessionAttributes'] = {
            'key': session.session_key
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        session = SessionStore(session_key=session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertTrue('outbox' in session)

    def test_clients_required(self):
        event = self.event.copy()
        Client.objects.all().delete()
        self.session['intents'] = {
            'CreateInvoiceWithTimeSheet': {
                'num_events': 0,
                'slots': {
                    'line_items': None,
                    'do_confirm': None,
                    'do_elicit_line_items': None
                },
                'last_form_errors': {
                },
                'initial_data': {
                    'query': self.client.full_name,
                    'client': self.client.id,
                    'time_sheet': self.time_sheet.id,
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_time_sheet_required(self):
        event = self.event.copy()
        Client.objects.all().delete()
        self.session['intents'] = {
            'CreateInvoiceWithTimeSheet': {
                'num_events': 0,
                'slots': {
                    'line_items': None,
                    'do_confirm': None,
                    'do_elicit_line_items': None
                },
                'last_form_errors': {
                },
                'initial_data': {
                    'query': self.client.full_name,
                    'client': self.client.id,
                }
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = handler(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
