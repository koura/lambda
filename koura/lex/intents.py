from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import logging

import boto3
import copy
import random
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from functools import reduce

from koura.businesses.forms import CreateBusinessForm, CreateClientForm, CreateInvoiceForm, CreateBankPayoutForm, \
    CreateTimeSheetForm, RetrieveInvoicesForm, RetrieveTimeSheetsForm
from koura.businesses.models import Client, Invoice, Business, TimeSheet, LineItem
from koura.businesses.serializers import LineItemSerializer
from koura.businesses.views import ConfirmInvoiceView
from koura.enums import View
from koura.lex.enums import Postback, DialogActionType, InvocationSource, FulfillmentState, ContentType, Context, \
    Intent as IntentName
from koura.lex.utils import to_camel_case, to_snake_case, pluralize, verbose_timedelta
from koura.urlresolvers import get_absolute_url, get_absolute_static_url

SUPPORT_EMAIL = getattr(settings, 'SUPPORT_EMAIL')
User = get_user_model()
logger = logging.getLogger(__name__)

COMMANDS = [
    'I want to link my business',
    'Add a new client',
    'Send an invoice',
    'Configure payout details',
    'Show invoices',
    'Add time sheet',
    'View time sheets',
]

def get_commands_text():
    return '\n'.join(['"%s"' % c for c in COMMANDS])

class Intent(object):
    intent_name = None
    messages = {
        'fulflled': None,
        'failed': None,
        'unauthentication_required': None,
        'authentication_required': None
    }
    is_unauthentication_required = False
    is_authentication_required = False

    @staticmethod
    def clean_response(response):
        return response
        # def clean(d):
        # data = response.copy()
        #     keys_to_remove = []
        #     for k, v in d.items():
        #         if k != 'slots' and isinstance(v, dict):
        #             clean(v)
        #         elif v is None:
        #             keys_to_remove.append(k)
        #     for k in keys_to_remove:
        #         del d[k]
        # clean(data)
        #
        # return data

    @staticmethod
    def is_authenticated(session):
        if 'user_id' in session:
            try:
                user = User.objects.get(id=session['user_id'])
                return user.is_active
            except ObjectDoesNotExist:
                pass
        return False

    @staticmethod
    def to_lex_key(obj):
        return to_camel_case(obj)

    @staticmethod
    def from_lex_key(obj):
        return to_snake_case(obj)

    def get_user(self):
        if 'user_id' in self.session:
            try:
                user = User.objects.get(id=self.session['user_id'])
                return user
            except ObjectDoesNotExist:
                pass
        return None

    def get_business(self):
        if 'user_id' in self.session:
            try:
                business = Business.objects.get(user__id=self.session['user_id'])
                return business
            except:
                pass
        return None

    def create_close_response(self, *, fulfillment_state, outbox=None):
        session_attributes = self.event.get('sessionAttributes').copy()

        if not outbox:
            if fulfillment_state == FulfillmentState.FULFILLED:
                message = self.get_fulfilled_message(ContentType.PLAIN_TEXT)
                self.session['outbox'] = {
                    ContentType.MESSENGER: self.get_fulfilled_message(ContentType.MESSENGER)
                }
            else:
                message = self.get_failed_message(ContentType.PLAIN_TEXT)
                self.session['outbox'] = {
                    ContentType.MESSENGER: self.get_failed_message(ContentType.MESSENGER)
                }
        else:
            message = outbox.get(ContentType.PLAIN_TEXT)
            self.session['outbox'] = outbox
        self.session.save()

        response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': DialogActionType.CLOSE.value,
                'fulfillmentState': fulfillment_state.value,
                'message': {
                    'contentType': 'PlainText',
                    'content': message
                }
            }
        }
        return self.clean_response(response)

    @staticmethod
    def get_boolean_message(content_type, text):
        messages = {
            ContentType.MESSENGER: {
                'text': text,
                'quick_replies': [
                    {
                        'content_type': 'text',
                        'title': 'Yes',
                        'payload': 'True'
                    },
                    {
                        'content_type': 'text',
                        'title': 'No',
                        'payload': 'False'
                    }
                ]
            }
        }
        return messages.get(content_type)

    @staticmethod
    def create_delegate_response(*, event):
        return Intent.clean_response({
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': DialogActionType.DELEGATE.value,
                'slots': event.get('currentIntent').get('slots')
            }
        })

    @staticmethod
    def create_elicit_intent_response(*, event, session):
        about = {
            'text': 'Try saying something like:\n%s' % get_commands_text()
        }
        gif_filename = random.choice(['what_%d.gif' % i for i in range(7)])
        session['outbox'] = {
            ContentType.MESSENGER: [
                {
                    'attachment': {
                        'type': 'image',
                        'payload': {
                            'url': get_absolute_static_url('gif/%s' % gif_filename),
                            "is_reusable": True,
                        }

                    }
                },
                {'text': 'I do not understand'},
                about
            ]
        }
        session.save()

        return Intent.clean_response({
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': DialogActionType.ELICIT_INTENT.value,
                'message': {
                    'contentType': 'PlainText',
                    'content': 'I do not understand'
                }
            }
        })

    def create_elicit_slot_response(self, *, slot_to_elicit, context=Context.DEFAULT):
        intent = self.event.get('currentIntent')
        session_attributes = self.event.get('sessionAttributes').copy()
        slots = copy.deepcopy(self.session['intents'][self.intent_name]['slots'])
        slots[slot_to_elicit] = None
        self.session['outbox'] = {
            ContentType.MESSENGER: self.get_slot_to_elicit_message(slot_to_elicit=slot_to_elicit, content_type=ContentType.MESSENGER,
                                                                   context=context)
        }
        self.session.save()
        return Intent.clean_response({
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': DialogActionType.ELICIT_SLOT.value,
                'slots': self.to_lex_key(slots),
                'intentName': intent.get('name'),
                'slotToElicit': self.to_lex_key(slot_to_elicit),
                'message': {
                    'contentType': 'PlainText',
                    'content': self.get_slot_to_elicit_message(slot_to_elicit=slot_to_elicit, content_type=ContentType.PLAIN_TEXT, context=context)
                }
            }
        })

    @staticmethod
    def get_open_id_token(user):
        client = boto3.client('cognito-identity')

        # Find the identity pool (so lazy...)
        response = client.list_identity_pools(MaxResults=60)
        identity_pool = [p for p in response.get('IdentityPools') if
                         p.get('IdentityPoolName') == settings.AWS_COGNITO_IDENTITY_POOL_NAME][0]
        # Get Open ID token
        kwargs = {
            'IdentityPoolId': identity_pool.get('IdentityPoolId'),
            'Logins': {
                settings.AWS_COGNITO_DEVELOPER_PROVIDER_NAME: str(user.id)
            }
        }
        if user.cognito_identity_id:
            kwargs['IdentityId'] = user.cognito_identity_id

        response = client.get_open_id_token_for_developer_identity(**kwargs)
        user.cognito_identity_id = response.get('IdentityId')
        user.save()
        return response.get('Token')

    def __init__(self, session):
        self.event = None
        self.session = session

    def get_slot_to_elicit_message(self, *, slot_to_elicit, content_type, context=Context.DEFAULT):
        if 'elicit_slot' in self.messages:
            if context in self.messages.get('elicit_slot'):
                if slot_to_elicit in self.messages.get('elicit_slot').get(context):
                    return self.messages.get('elicit_slot').get(context).get(slot_to_elicit).get(content_type)
        return 'Error!?!?!?'

    def get_failed_message(self, content_type):
        return self.messages.get('failed').get(content_type)

    def get_fulfilled_message(self, content_type):
        return self.messages.get('fulfilled').get(content_type)

    def get_unauthentication_required_message(self, content_type):
        return self.messages.get('unauthentication_required').get(content_type)

    def get_authentication_required_message(self, content_type):
        return self.messages.get('authentication_required').get(content_type)

    def process_fulfillment(self):
        raise NotImplementedError()

    def process_rejection(self):
        raise NotImplementedError()

    def process_dialog(self):
        raise NotImplementedError()

    def process_event(self, event):
        self.event = event
        if event.get('invocationSource') == InvocationSource.DIALOG_CODE_HOOK.value:
            return self.process_dialog()
        else:
            return self.process_fulfillment()


class FormIntent(Intent):
    intent_name = None
    form_class = None
    fields = []

    def __init__(self, session):
        super(FormIntent, self).__init__(session)
        if 'intents' not in self.session:
            self.session['intents'] = {}
        if self.intent_name not in self.session['intents']:
            self.session['intents'][self.intent_name] = {}
        if 'slots' not in self.session['intents'][self.intent_name]:
            self.session['intents'][self.intent_name]['slots'] = {}
            for field in self.fields:
                self.session['intents'][self.intent_name]['slots'][field] = None
        if 'last_form_errors' not in self.session['intents'][self.intent_name]:
            self.session['intents'][self.intent_name]['last_form_errors'] = {}
            for field in self.fields:
                self.session['intents'][self.intent_name]['last_form_errors'][field] = ''
        self.session.save()

    def get_initial_data(self):
        initial_data = {}
        if 'initial_data' in self.session['intents'][self.intent_name]:
            initial_data.update(self.session['intents'][self.intent_name]['initial_data'])
        return initial_data

    def form_valid(self, form):
        raise NotImplementedError

    def form_invalid(self, form):
        raise NotImplementedError

    def get_form_kwargs(self):
        return {}

    def create_elicit_slot_response(self, *, slot_to_elicit, context=Context.DEFAULT):
        intent = self.event.get('currentIntent').copy()
        session_attributes = self.event.get('sessionAttributes').copy()

        # Save messages to session
        self.session['outbox'] = {
            ContentType.MESSENGER: self.get_slot_to_elicit_message(
                slot_to_elicit=slot_to_elicit,
                content_type=ContentType.MESSENGER, context=context
            )
        }
        self.session.save()

        slots = copy.deepcopy(self.session['intents'][self.intent_name]['slots'])
        slots[slot_to_elicit] = None

        return self.clean_response({
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': DialogActionType.ELICIT_SLOT.value,
                'slots': self.to_lex_key(slots),
                'intentName': intent.get('name'),
                'slotToElicit': self.to_lex_key(slot_to_elicit),
                'message': {
                    'contentType': 'PlainText',
                    'content': self.get_slot_to_elicit_message(
                        slot_to_elicit=slot_to_elicit,
                        content_type=ContentType.PLAIN_TEXT,
                        context=context,
                    )
                }
            }
        })

    def process_dialog(self):
        # Check auth
        if self.is_authenticated(self.session) and self.is_unauthentication_required:
            return Intent.create_elicit_intent_response(
                event=self.event,
                session=self.session
            )

        if not self.is_authenticated(self.session) and self.is_authentication_required:
            return Intent.create_elicit_intent_response(
                event=self.event,
                session=self.session
            )

        # If this is the initial invocation for this intent let's ask for the first slot
        are_slots_empty = reduce(
            lambda x, y: x and y is None,
            self.event.get('currentIntent').get('slots').values(),
            True
        )
        logger.debug('are_slots_empty = %s' % are_slots_empty)
        if self.fields and self.session['intents'][self.event.get('currentIntent').get('name')]['num_events'] <= 1 and are_slots_empty:
            slot_to_elicit = self.fields[0]
            response = self.create_elicit_slot_response(
                slot_to_elicit=slot_to_elicit,
            )
            return response

        # Let's try merging Lex's slots with mine
        slots = self.session['intents'][self.intent_name]['slots']
        last_form_errors = self.session['intents'][self.intent_name].get('last_form_errors', {})
        for key, value in self.event.get('currentIntent').get('slots').items():
            if not value:
                continue
            snake_key = self.from_lex_key(key)
            if not slots.get(snake_key):
                slots[snake_key] = value
        self.session.save()

        # For some reason Lex doesn't capture garbage inputs...?
        # We'll just slide the input into an open slot
        input_transcript = self.event.get('inputTranscript')
        is_transcript_unassigned = True
        for field in self.fields:
            value = slots.get(field)
            if field in last_form_errors and value:
                if value in input_transcript:
                    is_transcript_unassigned = False
                    break
                elif '\n' in input_transcript and value in input_transcript.replace('\n', ' '):
                    is_transcript_unassigned = False
                    slots[field] = input_transcript
                    break
        logger.debug('is_transcript_unassigned = %s' % str(is_transcript_unassigned))
        logger.debug('last_form_errors = %s' % json.dumps(last_form_errors, indent=2))

        # Validate the form
        data = self.get_initial_data()
        for field in self.fields:
            data[field] = slots.get(field)
        form = self.form_class(**{**self.get_form_kwargs(), **{'data': data}})

        # Handle unassigned input transcript
        if is_transcript_unassigned:
            field_to_elicit = form.get_field_to_elicit()
            slots[field_to_elicit] = input_transcript
            self.session.save()
            data = self.get_initial_data()
            for field in self.fields:
                data[field] = slots.get(field)
            logger.debug('We are assigning the input transcript (%s) to %s', input_transcript, field_to_elicit)
            form = self.form_class(**{**self.get_form_kwargs(), **{'data': data}})

        # Clean out the slots based on form errors
        for error in form.errors:
            key = self.to_lex_key(error)
            if key in slots:
                slots[key] = None
        last_form_errors.clear()
        last_form_errors.update(form.errors)
        self.session.save()

        field_to_elicit = form.get_field_to_elicit()
        logger.debug('field_to_elicit = %s' % field_to_elicit)
        logger.debug('form.is_valid = %s' % str(form.is_valid()))
        logger.debug('data = \n%s' % json.dumps(data, indent=2))
        logger.debug('form.errors = \n%s' % json.dumps(form.errors, indent=2))
        if not form.is_valid():
            errors = form.errors
            if form.data.get(field_to_elicit) and field_to_elicit in errors:
                logger.debug('Invalid %s!' % field_to_elicit)
                response = self.create_elicit_slot_response(
                    slot_to_elicit=field_to_elicit,
                    context=Context.ERROR
                )
            else:  # Elicit the next slot
                response = self.create_elicit_slot_response(
                    slot_to_elicit=field_to_elicit
                )
        else:
            response = self.process_fulfillment(form)
        return response

    def process_fulfillment(self, form=None):
        slots = self.session['intents'][self.intent_name]['slots']
        if not form:
            data = self.get_initial_data()
            for field in self.fields:
                data[field] = slots.get(field)
            
            # Merge from Lex
            for key, value in self.event.get('currentIntent').get('slots').items():
                if not value:
                    continue
                snake_key = self.from_lex_key(key)
                if not data.get(snake_key):
                    data[snake_key] = value
            
            form = self.form_class(**{**self.get_form_kwargs(), **{'data': data}})

        # Clear out session data for intent
        del self.session.get('intents', {})[self.event.get('currentIntent').get('name')]
        self.session.save()

        if form.is_valid() and form.is_fulfilled():
            self.form_valid(form)
            return self.create_close_response(
                fulfillment_state=FulfillmentState.FULFILLED
            )
        else:
            logger.debug('form.errors = \n%s' % json.dumps(form.errors, indent=2))
            return self.create_close_response(
                fulfillment_state=FulfillmentState.FAILED,
            )


class CreateBusinessIntent(FormIntent):
    intent_name = IntentName.CREATE_BUSINESS.value
    fields = [
        'name',
        'street_address',
        'locality',
        'region',
        'postal_code',
        'country',
        'has_logo',
        'logo_url',
        'do_accept_terms_of_service'
    ]
    form_class = CreateBusinessForm
    messages = {
        'failed': {
            ContentType.PLAIN_TEXT: 'Uh oh. There was an error creating your account. Please contact %s' % SUPPORT_EMAIL,
            ContentType.MESSENGER: [
                {'text': 'I\'m sorry to hear you won\'t be joining 😞'},
                {'text': 'If you ever change your mind about signing up'},
                {'text': 'I\'ll be here 😊'},
            ]
        },
    }

    def process_dialog(self):
        if self.get_business():
            outbox = {
                ContentType.PLAIN_TEXT: 'You already have an account',
                ContentType.MESSENGER: [
                    {'text': 'It looks like you have already told me about your business'},
                    {'text': 'You can start adding clients to your account by saying "Add client"'},
                    {'text': 'Once you do that, you can send out invoices by saying "Send invoice"'},
                ]
            }
            return self.create_close_response(fulfillment_state=FulfillmentState.FAILED, outbox=outbox)
        return super(CreateBusinessIntent, self).process_dialog()

    def get_slot_to_elicit_message(self, *, slot_to_elicit, content_type, context=Context.DEFAULT):
        messages = {
            Context.DEFAULT: {
                'name': {
                    ContentType.PLAIN_TEXT: 'What is the name of your business?',
                    ContentType.MESSENGER: [
                        {'text': 'That should be no problem!'},
                        {'text': 'I just need you to answer a couple of questions'},
                        {'text': 'First, what is the name of your business?'},
                    ]
                },
                'street_address': {
                    ContentType.PLAIN_TEXT: 'What is your business\'s street address?',
                    ContentType.MESSENGER: [
                        {'text': 'What is the street address of your business?'},
                        {'text': 'You can use up to two lines'},
                    ]
                },
                'locality': {
                    ContentType.PLAIN_TEXT: 'What city is your business located in?',
                    ContentType.MESSENGER: [
                        {'text': 'Got it!'},
                        {'text': 'What city is your business located in?'},
                    ]
                },
                'region': {
                    ContentType.PLAIN_TEXT: 'Please tell me the state that your business is in',
                    ContentType.MESSENGER: [
                        {'text': 'Thanks!'},
                        {'text': 'Now go ahead and tell me the state or province of your business'},
                    ]
                },
                'postal_code': {
                    ContentType.PLAIN_TEXT: 'Please tell me the postal code that your business is in',
                    ContentType.MESSENGER: [
                        {'text': 'Great!'},
                        {'text': 'Now give me your business\'s postal code'},
                    ]
                },
                'country': {
                    ContentType.PLAIN_TEXT: 'What country?',
                    ContentType.MESSENGER: [
                        {'text': 'I also need you to tell me the name of the country where your business is located'},
                    ]
                },
                'has_logo': {
                    ContentType.PLAIN_TEXT: 'Would you like to upload your business logo? This logo will be used on invoices you send out',
                    ContentType.MESSENGER: Intent.get_boolean_message(
                        content_type=ContentType.MESSENGER,
                        text='Would you like to upload the logo of your business? This logo will be used on invoices that you send out'
                    )
                },
                'logo_url': {
                    ContentType.PLAIN_TEXT: 'Awesome! Please send me your logo',
                    ContentType.MESSENGER: [
                        {'text': 'Awesome!'},
                        {'text': 'Please send me your logo'},
                    ]
                },
                'do_accept_terms_of_service': {
                    ContentType.PLAIN_TEXT: 'Do you accept our terms of service? (%s)' % get_absolute_url(View.TERMS_OF_SERVICE.value),
                    ContentType.MESSENGER: [
                        {'text': 'Almost done 😇'},
                        {'text': 'I just need you to take a moment to read our Terms of Service Agreement'},
                        {
                            'attachment': {
                                'type': 'template',
                                'payload': {
                                    'template_type': 'generic',
                                    'elements': [
                                        {
                                            'title': 'Terms of Service',
                                            'image_url': get_absolute_static_url('img/terms_of_service.png'),
                                            'default_action': {
                                                "type": "web_url",
                                                "url": get_absolute_url(View.TERMS_OF_SERVICE.value),
                                            }
                                        }
                                    ]
                                }

                            }
                        },
                        {
                            'text': 'Do you accept our terms of service?',
                            'quick_replies': [
                                {
                                    'content_type': 'text',
                                    'title': 'Yes',
                                    'payload': 'True'
                                },
                                {
                                    'content_type': 'text',
                                    'title': 'No',
                                    'payload': 'False'
                                }
                            ]
                        },
                    ]

                }
            },
            Context.ERROR: {
                'do_accept_terms_of_service': {
                    ContentType.PLAIN_TEXT: 'Not valid',
                    ContentType.MESSENGER: [
                        {'text': 'I don\'t understand that'},
                        {'text': 'If you have read and accepted our terms of service say "Yes"'},
                        {'text': 'Otherwise say "No"'},
                        {
                            'attachment': {
                                'type': 'template',
                                'payload': {
                                    'template_type': 'generic',
                                    'elements': [
                                        {
                                            'title': 'Terms of Service',
                                            'image_url': get_absolute_static_url('img/terms_of_service.jpg'),
                                            'default_action': {
                                                "type": "web_url",
                                                "url": get_absolute_url(View.TERMS_OF_SERVICE.value),
                                            }
                                        }
                                    ]
                                }

                            }
                        },
                        {
                            'text': 'Do you accept our terms of service?',
                            'quick_replies': [
                                {
                                    'content_type': 'text',
                                    'title': 'Yes',
                                    'payload': 'True'
                                },
                                {
                                    'content_type': 'text',
                                    'title': 'No',
                                    'payload': 'False'
                                }
                            ]
                        },
                    ]
                },

                'name': {
                    ContentType.PLAIN_TEXT: 'Unfortunately, that name is invalid.',
                    ContentType.MESSENGER: [
                        {'text': 'Unfortunately, that name is invalid.'}
                    ]
                },
                'street_address': {
                    ContentType.PLAIN_TEXT: 'Unfortunately, that name is invalid.',
                    ContentType.MESSENGER: [
                        {'text': 'That does not look like a valid street address'},
                        {'text': 'Please enter another one'},
                    ]
                },
                'locality': {
                    ContentType.PLAIN_TEXT: 'Unfortunately, that name is invalid.',
                    ContentType.MESSENGER: [
                        {'text': 'That does not look like a valid city'},
                        {'text': 'Please enter another one'},
                    ]
                },
                'region': {
                    ContentType.PLAIN_TEXT: 'Unfortunately, that name is invalid.',
                    ContentType.MESSENGER: [
                        {'text': 'That does not look like a valid state or province'},
                        {'text': 'Please enter another one'},
                    ]
                },
                'postal_code': {
                    ContentType.PLAIN_TEXT: 'Unfortunately, that name is invalid.',
                    ContentType.MESSENGER: [
                        {'text': 'That does not look like a valid postal code'},
                        {'text': 'Please enter another one'},
                    ]
                },
                'country': {
                    ContentType.PLAIN_TEXT: 'Unfortunately, that name is invalid.',
                    ContentType.MESSENGER: [
                        {'text': 'That does not look like a valid country'},
                        {'text': 'Please enter another one'},
                    ]
                },
                'has_logo': {
                    ContentType.PLAIN_TEXT: 'Do you confirm?',
                    ContentType.MESSENGER: [
                        {'text': 'I don\'t understand that'},
                        {'text': 'If you would like to upload your business logo say "Yes"'},
                        {'text': 'Otherwise say "No"'},
                        Intent.get_boolean_message(
                            content_type=ContentType.MESSENGER,
                            text='Would you like to upload the logo of your business? This logo will be used on invoices that you send out'
                        )
                    ]
                },
                'logo_url': {
                    ContentType.PLAIN_TEXT: 'I couldn\'t process that. Please try sending another image',
                    ContentType.MESSENGER: [
                        {'text': 'Sorry'},
                        {'text': 'I couldn\'t process that'},
                        {'text': 'Please try sending another image'},
                    ]
                }
            }
        }
        return messages.get(context).get(slot_to_elicit).get(content_type)

    def get_initial_data(self):
        return {
            'user': self.session['user_id']
        }

    def form_invalid(self, form):
        pass

    def form_valid(self, form):
        # Create business in DB
        business = form.save()
        self.session['user_id'] = business.user.id
        self.session.save()
        logger.debug('Created business %s!' % business.name)
        logger.debug('Updated session\n%s' % json.dumps(self.session.load(), indent=2))

    def get_fulfilled_message(self, content_type):
        user = User.objects.get(id=self.session['user_id'])
        messages = {
            ContentType.PLAIN_TEXT: 'Thanks for joining the crew %s!' % user.nickname,
            ContentType.MESSENGER: [
                {'text': 'Thanks for joining the crew %s!' % user.nickname},
                {'text': 'You can begin adding your client\'s information to your account by saying a phrase like "Add client"'},
                {'text': 'Once you do that you can send an invoice to your client by saying a phrase like "Create invoice"'}
            ]
        }
        return messages.get(content_type)


class CreateClientIntent(FormIntent):
    intent_name = IntentName.CREATE_CLIENT.value
    form_class = CreateClientForm
    fields = [
        'email_address',
        'full_name'
    ]
    messages = {
    }

    def process_dialog(self):
        if not self.get_business():
            outbox = {
                ContentType.PLAIN_TEXT: 'Before you do that, I need you to give me information about your business',
                ContentType.MESSENGER: [
                    {'text': 'Before I can do that, I need you to give me information about your business'},
                    {'text': 'Try saying something like "Link my business" or "Join"'}
                ]
            }
            return self.create_close_response(fulfillment_state=FulfillmentState.FAILED, outbox=outbox)
        return super(CreateClientIntent, self).process_dialog()

    def get_initial_data(self):
        business = self.get_business()
        return {
            'business': business.id
        }

    def form_valid(self, form):
        # Create client in DB
        client = form.save()
        logger.debug('Created client %s <%s> for business %s!', client.full_name, client.email_address, client.business.name)

    def form_invalid(self, form):
        logger.critical('This should never happen lol')

    def get_fulfilled_message(self, content_type):
        client = Client.objects.filter(business__user__id=self.session['user_id']).order_by('-date_created')[0]
        messages = {
            ContentType.PLAIN_TEXT: 'I have added %s to your account' % client.full_name,
            ContentType.MESSENGER: [
                {'text': 'I have added %s to your account' % client.full_name},
                {'text': 'Invoices for this client will be sent to %s' % client.email_address},
                {'text': 'You can send an invoice to your client by saying a phrase like \'Create invoice\''}
            ]
        }
        return messages.get(content_type)

    def get_slot_to_elicit_message(self, *, slot_to_elicit, content_type, context=Context.DEFAULT):
        messages = {
            Context.DEFAULT: {
                'email_address': {
                    ContentType.PLAIN_TEXT: 'What is the client\'s email address',
                    ContentType.MESSENGER: [
                        {'text': 'Okay cool'},
                        {'text': 'I just need a couple bits of information'},
                        {'text': 'Please type in your client\'s email address'},
                    ]
                },
                'full_name': {
                    ContentType.PLAIN_TEXT: 'Now tell me your client\'s first and last name',
                    ContentType.MESSENGER: [
                        {'text': 'Now tell me your client\'s first and last name'},
                    ]
                },
            },
            Context.ERROR: {
                'email_address': {
                    ContentType.PLAIN_TEXT: 'That doesn\'t look like a valid email address. Please try entering another one.',
                    ContentType.MESSENGER: [
                        {'text': 'That doesn\'t look like a valid email address'},
                        {'text': 'Please try entering another one'}
                    ]
                },
                'full_name': {
                    ContentType.PLAIN_TEXT: 'Unfortunately, I cannot process that. The name you use must not contain any numbers or special characters.',
                    ContentType.MESSENGER: [
                        {'text': 'Unfortunately, I cannot process that'},
                        {'text': 'The name you use must not contain any numbers or special characters'},
                    ]
                }
            }
        }
        message = messages.get(context).get(slot_to_elicit).get(content_type)
        if slot_to_elicit == 'email_address' and context == Context.ERROR:
            input_transcript = self.event.get('inputTranscript')
            business = self.get_business()
            if Client.objects.filter(business=business, email_address=input_transcript).exists():
                duplicate_messages = {
                    ContentType.PLAIN_TEXT: 'That email address is already linked to one of your clients. Please try entering another one.',
                    ContentType.MESSENGER: [
                        {'text': 'That email address is already linked to one of your clients'},
                        {'text': 'Please try entering another one'}
                    ]
                }
                message = duplicate_messages.get(content_type)

        return message


class CreateInvoiceIntent(FormIntent):
    intent_name = IntentName.CREATE_INVOICE.value
    form_class = CreateInvoiceForm
    fields = [
        'query',
        'client',
        'line_items',
        'do_confirm',
    ]
    messages = {
        'failed': {
            ContentType.PLAIN_TEXT: 'Uh oh. There was an error creating your account. Please contact %s' % SUPPORT_EMAIL,
            ContentType.MESSENGER: [
                {'text': 'Okay, I cancelled the invoice'},
                {'text': 'Please let me know if you would like to create another'},
                {'text': 'Try saying something like "Send an invoice"'},
            ]
        },
    }

    def process_dialog(self):
        business = self.get_business()
        if not business:
            outbox = {
                ContentType.PLAIN_TEXT: 'Before you do that, I need you to give me information about your business',
                ContentType.MESSENGER: [
                    {'text': 'Before you do that, I need you to give me some information about your business'},
                    {'text': 'Try saying something like "Link my business" or "Join"'}
                ]
            }
            return self.create_close_response(fulfillment_state=FulfillmentState.FAILED, outbox=outbox)
        elif not Client.objects.filter(business=business).exists():
            outbox = {
                ContentType.PLAIN_TEXT: 'Before you do that, I need you to give me information about your business',
                ContentType.MESSENGER: [
                    {'text': 'It does not look like you have added clients to your account yet'},
                    {'text': 'Try saying something like "Add client"'}
                ]
            }
            return self.create_close_response(fulfillment_state=FulfillmentState.FAILED, outbox=outbox)

        return super(CreateInvoiceIntent, self).process_dialog()

    def get_initial_data(self):
        initial_data = super().get_initial_data()
        business = self.get_business()
        initial_data.update({
            'business': business.id,
            'do_elicit_line_items': 'Yes'
        })
        return initial_data

    def form_valid(self, form):
        # Create invoice in DB
        invoice = form.save()
        client = invoice.client
        business = invoice.business
        logger.debug('Sent invoice to %s from %s ($%.2f, $%.2f)', client.full_name, business.user.full_name, invoice.amount / 100, invoice.application_fee / 100)

    def form_invalid(self, form):
        pass

    def get_fulfilled_message(self, content_type):
        business = self.get_business()
        invoice = Invoice.objects.filter(business=business).order_by('-date_created')[0]
        client = invoice.client
        if business.has_external_accounts:
            messages = {
                ContentType.PLAIN_TEXT: 'Your invoice has been sent to %s at %s' % (client.full_name, client.email_address),
                ContentType.MESSENGER: [
                    {'text': 'Your invoice for $%.2f was sent to %s at %s' % (invoice.amount / 100, client.full_name, client.email_address)},
                    {'text': 'I will let you know when the payment is made'},
                ]
            }
        else:
            messages = {
                ContentType.PLAIN_TEXT: 'Your invoice has been sent to %s at %s' % (client.full_name, client.email_address),
                ContentType.MESSENGER: [
                    {'text': 'Your invoice for $%.2f was sent to %s at %s' % (invoice.amount / 100, client.full_name, client.email_address)},
                    {'text': 'It looks like you have not given me your payout information'},
                    {'text': '%s will be able to make a payment' % client.full_name},
                    {'text': 'But until you configure your payout I won\'t be able to transfer those funds to you'},
                    {'text': 'Try saying something like "Configure payout details"'},
                ]
            }
        return messages.get(content_type)

    def get_slot_to_elicit_message(self, *, slot_to_elicit, content_type, context=Context.DEFAULT):
        slots = self.session['intents'][self.intent_name]['slots']
        form = self.form_class(data={**slots, **self.get_initial_data()})
        form.is_valid()
        cleaned_data = form.cleaned_data
        business = cleaned_data.get('business')

        # Client options
        def get_client_messages(context):
            if not slots.get('query', None):
                return []

            # Build template
            query = cleaned_data.get('query')
            clients = Client.objects.filter(business=business).filter(
                Q(email_address__icontains=query) |
                Q(full_name__icontains=query)
            )[:10]
            elements = []
            for client in clients:
                elements.append({
                    'title': client.full_name,
                    'subtitle': client.email_address,
                    'buttons': [{
                        'type': 'postback',
                        'title': 'Select',
                        'payload': client.id
                    }]
                })

            template = {
                'attachment': {
                    'type': 'template',
                    'payload': {
                        'template_type': 'generic',
                        'elements': elements
                    }
                }
            }

            # Build messages
            messages = []

            if context == Context.DEFAULT:
                messages.extend([
                    {'text': 'Please select your client from one of the options below'},
                    template
                ])
            elif context == Context.ERROR:
                messages.extend([
                    {'text': 'I don\'t understand that'},
                    {'text': 'Please select your client from one of the options below'},
                    template
                ])

            return messages

        # Client
        client = Client(full_name='Optimus Prime', email_address='optimusprime@gmail.com', id=9000)
        if cleaned_data.get('client', None):
            client = cleaned_data.get('client')
            
        # Amount
        amount = 0
        if cleaned_data.get('line_items'):
            amount = reduce(lambda x, y: x + y.amount, cleaned_data.get('line_items'), 0)

        # Confirm invoice url
        line_items = cleaned_data.get('line_items')
        line_items = line_items if line_items else []
        logger.debug('line_items = %s' % line_items)
        confirm_invoice_url = get_absolute_url(url=ConfirmInvoiceView.get_url({
            'line_items': [LineItemSerializer(l).data for l in line_items],
            'business': business.id,
            'client': client.id,
        }))

        messages = {
            Context.DEFAULT: {
                'query': {
                    ContentType.PLAIN_TEXT: 'Which client would you like to send an invoice to?',
                    ContentType.MESSENGER: [
                        {'text': 'Which client would you like to send an invoice to?'},
                        {'text': 'You can give me the person\'s name or email address'},
                    ]
                },
                'client': {
                    ContentType.PLAIN_TEXT: 'What is the client\'s id?',
                    ContentType.MESSENGER: get_client_messages(Context.DEFAULT)
                },
                'line_items': {
                    ContentType.PLAIN_TEXT: 'Please give a description of the services you provided',
                    ContentType.MESSENGER: [
                        {'text': 'What line items should I put in the invoice?'},
                        {'text': 'You can enter in as many lines as you want'},
                        {'text': 'Just make sure each line has a description and dollar value like this:\nTransportation Charges $75.00\nSoftware Development $200.00\nExpenses (misc) $182.00'},
                    ]
                },
                'do_confirm': {
                    ContentType.PLAIN_TEXT: 'Do you confirm?',
                    ContentType.MESSENGER: [
                        {'text': '%s will receive an invoice for $%.2f' % (client.first_name, amount / 100)},
                        {'text': 'Please take a second to look over the invoice that I will send out'},
                        {
                            'attachment': {
                                'type': 'template',
                                'payload': {
                                    'template_type': 'generic',
                                    'elements': [
                                        {
                                            'title': 'Invoice',
                                            'image_url': get_absolute_static_url('img/logo.png'),
                                            'default_action': {
                                                "type": "web_url",
                                                "url": confirm_invoice_url,
                                            }
                                        }
                                    ]
                                }

                            }
                        },
                        Intent.get_boolean_message(
                            content_type=ContentType.MESSENGER,
                            text='Would you like me to send out this invoice?'
                        )
                    ]
                },
            },
            Context.ERROR: {
                'query': {
                    ContentType.PLAIN_TEXT: 'No match found',
                    ContentType.MESSENGER: [
                        {'text': 'I was not able to find any clients who match your query 😓'},
                        {'text': 'Please try another search term'},
                        {'text': 'You can also say "Cancel" to stop this process and add a new client to your account'},
                    ]
                },
                'client': {
                    ContentType.PLAIN_TEXT: 'Not valid',
                    ContentType.MESSENGER: get_client_messages(Context.ERROR)
                },
                'line_items': {
                    ContentType.PLAIN_TEXT: 'I don\'t understand that',
                    ContentType.MESSENGER: [
                        {'text': 'Unfortunately, I cannot understand that 😔'},
                        {'text': 'Please enter line items that match these examples:\nTransportation Charges $75.00\nSoftware Development $200.00\nExpenses (misc) $182.00'},
                    ]
                },
                'do_confirm': {
                    ContentType.PLAIN_TEXT: 'Do you confirm?',
                    ContentType.MESSENGER: [
                        {'text': 'I don\'t understand that 😕'},
                        {'text': 'If you would like to send out this invoice say "Yes"'},
                        {'text': 'Otherwise say "No"'},
                        Intent.get_boolean_message(
                            content_type=ContentType.MESSENGER,
                            text='Would you like me to send out this invoice?'
                        )
                    ]
                },
            }
        }
        message = messages.get(context).get(slot_to_elicit).get(content_type)
        return message


class CreateBankPayoutIntent(FormIntent):
    intent_name = IntentName.CREATE_BANK_PAYOUT.value
    form_class = CreateBankPayoutForm
    fields = [
        'date_of_birth',
        'tax_id',
        'account_number',
        'routing_number',
        'ssn_last_four',
    ]
    messages = {
    }

    def process_dialog(self):
        business = self.get_business()
        if not business:
            outbox = {
                ContentType.PLAIN_TEXT: 'Before you do that, I need you to give me information about your business',
                ContentType.MESSENGER: [
                    {'text': 'Before I can do that, I need you to give me information about your business'},
                    {'text': 'Try saying something like "Link my business" or "Join"'}
                ]
            }
            return self.create_close_response(fulfillment_state=FulfillmentState.FAILED, outbox=outbox)
        if business.has_external_accounts:
            outbox = {
                ContentType.PLAIN_TEXT: 'You already gave me your bank account information',
                ContentType.MESSENGER: [
                    {'text': 'It looks like you have already given me your payout information'},
                    {'text': 'If you would still like to make changes, you will have to contact %s' % settings.SUPPORT_EMAIL},
                ]
            }
            return self.create_close_response(fulfillment_state=FulfillmentState.FAILED, outbox=outbox)
        return super(CreateBankPayoutIntent, self).process_dialog()

    def get_initial_data(self):
        initial_data = super().get_initial_data()
        business = self.get_business()
        initial_data.update({
            'business': business.id
        })
        return initial_data

    def form_valid(self, form):
        # Create client in DB
        business = form.submit()
        logger.debug('Created bank payout for business %s!' % business.name)

    def form_invalid(self, form):
        logger.critical('This should never happen lol')

    def get_fulfilled_message(self, content_type):
        messages = {
            ContentType.PLAIN_TEXT: 'All done!',
            ContentType.MESSENGER: [
                {'text': 'Thanks!'},
                {'text': 'After we verify your identity, you will begin to receive payouts to the bank account you provided'},
                {'text': 'I\'ll let you know when you are issued a payout'},
            ]
        }
        return messages.get(content_type)

    def get_slot_to_elicit_message(self, *, slot_to_elicit, content_type, context=Context.DEFAULT):
        messages = {
            Context.DEFAULT: {
                'date_of_birth': {
                    ContentType.PLAIN_TEXT: 'What is your date of birth? 🎂',
                    ContentType.MESSENGER: [
                        {'text': 'Okay'},
                        {'text': 'I just need you to answer a few questions so that we can verify your identity'},
                        {'text': 'First, what is your date of birth?'},
                    ]
                },
                'tax_id': {
                    ContentType.PLAIN_TEXT: 'What is your tax id?',
                    ContentType.MESSENGER: [
                        {'text': 'Please tell me the tax ID or EIN that the IRS assigned to your company'},
                        {'text': 'This should be a 9 digit number'},
                    ]
                },
                'account_number': {
                    ContentType.PLAIN_TEXT: 'What bank account number?',
                    ContentType.MESSENGER: [
                        {'text': 'What is the account number of the bank where you would like payouts deposited?'},
                    ]
                },
                'routing_number': {
                    ContentType.PLAIN_TEXT: 'What routing number?',
                    ContentType.MESSENGER: [
                        {'text': 'What is the routing number for that bank account?'},
                    ]
                },
                'ssn_last_four': {
                    ContentType.PLAIN_TEXT: 'Last four digits of SSN?',
                    ContentType.MESSENGER: [
                        {'text': 'Final question'},
                        {'text': 'What are the last 4 digits of your social security number?'},
                    ]
                },
            },
            Context.ERROR: {
                'date_of_birth': {
                    ContentType.PLAIN_TEXT: 'That doesn\'t look like a date of birth. Please try entering another one.',
                    ContentType.MESSENGER: [
                        {'text': 'That doesn\'t look like a valid birth date'},
                        {'text': 'Try saying something like "8/23/1986"'}
                    ]
                },
                'tax_id': {
                    ContentType.PLAIN_TEXT: 'Unfortunately, I cannot process that. The name you use must not contain any numbers or special characters.',
                    ContentType.MESSENGER: [
                        {'text': 'Unfortunately, I cannot process that'},
                        {'text': 'Please try entering a valid tax ID'},
                    ]
                },
                'account_number': {
                    ContentType.PLAIN_TEXT: 'Unfortunately, I cannot process that.',
                    ContentType.MESSENGER: [
                        {'text': 'I don\'t understand that'},
                        {'text': 'Please try entering a valid bank account number'},
                    ]
                },
                'routing_number': {
                    ContentType.PLAIN_TEXT: 'Unfortunately, I cannot process that.',
                    ContentType.MESSENGER: [
                        {'text': 'That doesn\'t look like a valid routing number'},
                        {'text': 'Please try entering another one'},
                    ]
                },
                'ssn_last_four': {
                    ContentType.PLAIN_TEXT: 'Unfortunately, I cannot process that.',
                    ContentType.MESSENGER: [
                        {'text': 'That doesn\'t make sense'},
                        {'text': 'Please tell me the last 4 digits of your social security number'},
                    ]
                }
            }
        }
        message = messages.get(context).get(slot_to_elicit).get(content_type)
        return message

        
class RetrieveInvoicesIntent(FormIntent):
    intent_name = IntentName.RETRIEVE_INVOICES.value
    form_class = RetrieveInvoicesForm
    fields = [
        'filter',
    ]
    page_size = 4
    messages = {}
    paginator = None
    page_number = None
    filter = None
    invoices = None
    next_page_token = None

    @staticmethod
    def get_facebook_elements(invoices, is_list_view):
        elements = []
        for invoice in invoices:
            did_pay = invoice.stripe_charge_id != ''
            verb = 'Paid' if did_pay else 'Owes'
            client = invoice.client
            date_created = ' \u2022 Invoice was sent %s' % verbose_timedelta(invoice.date_created) if not did_pay else ''
            elements.append({
                'title': client.full_name,
                'subtitle': '%s $%.2f%s' % (verb, invoice.amount / 100, date_created),
            })
        return elements

    def process_dialog(self):
        business = self.get_business()
        if not business:
            outbox = {
                ContentType.PLAIN_TEXT: 'Before you do that, I need you to give me information about your business',
                ContentType.MESSENGER: [
                    {'text': 'Before I can do that, I need you to give me information about your business'},
                    {'text': 'Try saying something like "Link my business" or "Join"'}
                ]
            }
            return self.create_close_response(fulfillment_state=FulfillmentState.FAILED, outbox=outbox)
        if not Invoice.objects.filter(business=business).exists():
            outbox = {
                ContentType.PLAIN_TEXT: 'Before you do that, I need you to give me information about your business',
                ContentType.MESSENGER: [
                    {'text': 'It does not look like you have any invoices'},
                    {'text': 'Try saying something like "Send invoice"'}
                ]
            }
            return self.create_close_response(fulfillment_state=FulfillmentState.FAILED, outbox=outbox)
        return super(RetrieveInvoicesIntent, self).process_dialog()

    def get_form_kwargs(self):
        form_kwargs = super(RetrieveInvoicesIntent, self).get_form_kwargs()
        form_kwargs['page_size'] = self.page_size
        return form_kwargs
        
    def get_initial_data(self):
        initial_data = super().get_initial_data()
        business = self.get_business()
        initial_data.update({
            'business': business.id
        })
        return initial_data

    def form_valid(self, form):
        # Create client in DB
        business = self.get_business()
        (self.invoices, self.next_page_token) = form.retrieve()
        self.filter = form.cleaned_data.get('filter')
        logger.debug('Retrieved %d invoices for business %s!' % (len(self.invoices), business.name))

    def form_invalid(self, form):
        logger.critical('This should never happen lol')

    def get_fulfilled_message(self, content_type):
        if len(self.invoices) > 0:
            is_list_view = bool(self.next_page_token) or len(self.invoices) >= 2
            elements = self.get_facebook_elements(self.invoices, is_list_view)
            payload = {
                'template_type': 'list' if is_list_view else 'generic',
                'elements': elements
            }
            if is_list_view:
                payload['top_element_style'] = 'compact'
                if self.next_page_token:
                    payload['buttons'] = [{
                        'title': 'View More',
                        'type': 'postback',
                        'payload': json.dumps({
                            'type': Postback.RETRIEVE_INVOICES.value,
                            'payload': {
                                'next_page_token': self.next_page_token
                            }
                        })
                    }]

            messages = {
                ContentType.PLAIN_TEXT: 'I retrieved your invoices!',
                ContentType.MESSENGER: [{
                    'attachment': {
                        'type': 'template',
                        'payload': payload
                    }
                }]
            }
        else:
            messages = {
                ContentType.PLAIN_TEXT: 'I could not find any invoices matching that query',
                ContentType.MESSENGER: [
                    {'text': 'Sorry'},
                    {'text': 'It looks like there are no invoices that match your query'},
                ]
            }
        return messages.get(content_type)

    def get_slot_to_elicit_message(self, *, slot_to_elicit, content_type, context=Context.DEFAULT):
        invoices = Invoice.objects.filter(business=self.get_business())
        num_invoices = invoices.count()
        total_paid_amount = reduce(lambda x, y: x + y.amount, invoices.exclude(stripe_charge_id=''), 0)
        total_unpaid_amount = reduce(lambda x, y: x + y.amount, invoices.filter(stripe_charge_id=''), 0)
        messages = {
            Context.DEFAULT: {
                'filter': {
                    ContentType.PLAIN_TEXT: 'Which filters?',
                    ContentType.MESSENGER: [
                        {'text': 'You have a total of %d %s for your business' % (num_invoices, pluralize('invoice', num_invoices))},
                        {'text': 'I see $%.2f worth of paid invoices' % (total_paid_amount / 100, )},
                        {'text': 'And $%.2f of outstanding payments' % (total_unpaid_amount / 100, )},
                        {
                            'text': 'Which invoices would you like to take a closer look at?',
                            'quick_replies': [
                                {
                                    'content_type': 'text',
                                    'title': 'All',
                                    'payload': 'all'
                                },
                                {
                                    'content_type': 'text',
                                    'title': 'Paid',
                                    'payload': 'paid'
                                },
                                {
                                    'content_type': 'text',
                                    'title': 'Past Due',
                                    'payload': 'unpaid'
                                }
                            ]
                        }
                    ]
                }
            },
            Context.ERROR: {
                'filter': {
                    ContentType.PLAIN_TEXT: 'Unfortunately, I cannot process that.',
                    ContentType.MESSENGER: [
                        {'text': 'That doesn\'t make sense'},
                        {
                            'text': 'Which invoices would you like to take a closer look at?',
                            'quick_replies': [
                                {
                                    'content_type': 'text',
                                    'title': 'All',
                                    'payload': 'all'
                                },
                                {
                                    'content_type': 'text',
                                    'title': 'Paid',
                                    'payload': 'paid'
                                },
                                {
                                    'content_type': 'text',
                                    'title': 'Past Due',
                                    'payload': 'unpaid'
                                }
                            ]
                        }
                    ]
                }
            }
        }
        message = messages.get(context).get(slot_to_elicit).get(content_type)
        return message  

        
class CancelIntent(Intent):
    intent_name = IntentName.CANCEL.value
    messages = {
        'fulfilled': {
            ContentType.PLAIN_TEXT: 'Stopped',
            ContentType.MESSENGER: [
                {'text': 'Okay okay'},
                {'text': 'I will stop'},
                {'text': 'Let me know if there is anything else I can do for you'},
            ]
        }
    }

    def process_fulfillment(self):
        self.session.pop('intents')
        self.session.save()
        return self.create_close_response(
            fulfillment_state=FulfillmentState.FULFILLED
        )


class HelpIntent(Intent):
    intent_name = IntentName.HELP.value
    messages = {
        'fulfilled': {
            ContentType.PLAIN_TEXT: 'Stopped',
            ContentType.MESSENGER: [
                {'text': 'Of course I can give you a hint 😉️'},
                {'text': 'Try saying something like:\n%s' % get_commands_text()},
                {'text': 'You can also say something like "Cancel" or "Quit" if you need me to stop 🛑  doing anything'}
            ]
        }
    }

    def process_fulfillment(self):
        self.session.pop('intents')
        self.session.save()
        return self.create_close_response(
            fulfillment_state=FulfillmentState.FULFILLED
        )

        
class HelloIntent(Intent):
    intent_name = IntentName.HELLO.value

    def get_fulfilled_message(self, content_type):
        user = self.get_user()
        business = self.get_business()
        messages = {
            ContentType.PLAIN_TEXT: 'Hello!',
            ContentType.MESSENGER: [
                {'text': '👋 Hello %s 😊' % (user.nickname if user.nickname else '')},
                {'text': 'I\'m Koura'},
                {
                    'attachment': {
                        'type': 'template',
                        'payload': {
                            'template_type': 'generic',
                            'elements': [
                                {
                                    'title': 'kōura',
                                    'subtitle': 'I love 😍 your name, but what does it mean?',
                                    'image_url': get_absolute_static_url('img/dictionary.jpg'),
                                    'default_action': {
                                        'type': 'web_url',
                                        'url': 'https://translate.google.com/?um=1&ie=UTF-8&hl=en&client=tw-ob#mi/en/koura',
                                    }
                                }
                            ]
                        }

                    }
                },
                {'text': 'My job is to help you out with running your business'},
                {'text': 'I generate invoices for your clients and keep track of which clients have paid'},
                {'text': 'I can also keep track of the hours you spend doing work for a given client'},
                {'text': 'To get started'},
                {'text': 'Try saying something like:\n%s' % get_commands_text()},
                {'text': 'You can also say something like "Cancel" or "Quit" if you need me to stop 🛑 doing anything'},
                {'text': 'Say "Help" if you ever need a refresher on my commands'}
            ]
        }
        if not business:
            messages.get(ContentType.MESSENGER).extend([
                {'text': 'It looks like you have not told me about your business 🏢'},
                {'text': 'You can say something like "Link my business" to get started'}
            ])
        return messages.get(content_type)

    def process_fulfillment(self):
        self.session.pop('intents')
        self.session.save()
        return self.create_close_response(
            fulfillment_state=FulfillmentState.FULFILLED
        )

 
class CreateTimeSheetIntent(FormIntent):
    intent_name = IntentName.CREATE_TIME_SHEET.value
    form_class = CreateTimeSheetForm
    fields = [
        'query',
        'client',
        'description',
        'hourly_rate',
    ]
    messages = {
    }

    def process_dialog(self):
        business = self.get_business()
        if not business:
            outbox = {
                ContentType.PLAIN_TEXT: 'Before you do that, I need you to give me information about your business',
                ContentType.MESSENGER: [
                    {'text': 'Before you do that, I need you to give me some information about your business'},
                    {'text': 'Try saying something like "Link my business" or "Join"'}
                ]
            }
            return self.create_close_response(fulfillment_state=FulfillmentState.FAILED, outbox=outbox)
        elif not Client.objects.filter(business=business).exists():
            outbox = {
                ContentType.PLAIN_TEXT: 'Before you do that, I need you to give me information about your business',
                ContentType.MESSENGER: [
                    {'text': 'It does not look like you have added clients to your account yet'},
                    {'text': 'Try saying something like "Add client"'}
                ]
            }
            return self.create_close_response(fulfillment_state=FulfillmentState.FAILED, outbox=outbox)

        return super(CreateTimeSheetIntent, self).process_dialog()

    def get_initial_data(self):
        initial_data = super().get_initial_data()
        business = self.get_business()
        initial_data.update({
            'business': business.id
        })
        return initial_data

    def form_valid(self, form):
        # Create time sheet in DB
        time_sheet = form.save()
        client = time_sheet.client
        business = time_sheet.business
        logger.debug('Created time sheet to %s for %s @ rate of $%.2f', client.full_name, business.user.full_name, time_sheet.hourly_rate)

    def form_invalid(self, form):
        pass

    def get_fulfilled_message(self, content_type):
        time_sheet = TimeSheet.objects.filter(business__user__id=self.session['user_id']).order_by('-date_created')[0]
        client = time_sheet.client
        messages = {
            ContentType.PLAIN_TEXT: 'Your time sheet for %s has been created' % (client.full_name),
            ContentType.MESSENGER: [
                {'text': 'Done!'},
                {'text': 'I created a time sheet for %s' % client.full_name},
                {'text': 'You can start logging your hours by saying "Start tracking" or "Stop tracking"'},
                {'text': 'Say "Submit time sheet" when you are ready to turn your time sheet into an invoice'},
            ]
        }
        return messages.get(content_type)

    def get_slot_to_elicit_message(self, *, slot_to_elicit, content_type, context=Context.DEFAULT):
        slots = self.session['intents'][self.intent_name]['slots']
        form = self.form_class(data={**slots, **self.get_initial_data()})
        form.is_valid()
        cleaned_data = form.cleaned_data
        business = cleaned_data.get('business')

        # Client options
        def get_client_messages(context):
            if not slots.get('query', None):
                return []

            # Build messages
            messages = []
            if context == Context.DEFAULT:
                messages.extend([
                    {'text': 'Please select your client from one of the options below'},
                ])
            elif context == Context.ERROR:
                messages.extend([
                    {'text': 'I don\'t understand that'},
                    {'text': 'Please select your client from one of the options below'},
                ])

            # Build elements
            query = cleaned_data.get('query')
            clients = Client.objects.filter(business=business).filter(
                Q(email_address__icontains=query) |
                Q(full_name__icontains=query)
            )[:10]

            elements = []
            for client in clients:
                elements.append({
                    'title': client.full_name,
                    'subtitle': client.email_address,
                    'buttons': [{
                        'type': 'postback',
                        'title': 'Select',
                        'payload': client.id
                    }]
                })

            messages.append({
                'attachment': {
                    'type': 'template',
                    'payload': {
                        'template_type': 'generic',
                        'elements': elements
                    }
                }
            })
            return messages

        # Client
        client = Client(full_name='Optimus Prime', email_address='optimusprime@gmail.com', id=9000)
        if cleaned_data.get('client', None):
            client = cleaned_data.get('client')
            
        messages = {
            Context.DEFAULT: {
                'query': {
                    ContentType.PLAIN_TEXT: 'Which client would you like to send an invoice to?',
                    ContentType.MESSENGER: [
                        {'text': 'Which client would you like to charge hours to?'},
                        {'text': 'You can give me the person\'s name or email address'},
                    ]
                },
                'client': {
                    ContentType.PLAIN_TEXT: 'What is the client\'s id?',
                    ContentType.MESSENGER: get_client_messages(Context.DEFAULT)
                },
                'description': {
                    ContentType.PLAIN_TEXT: 'Please give a description of the services you provided',
                    ContentType.MESSENGER: [
                        {'text': 'Please give a description of the services you are providing'},
                        {'text': 'This information will be added as a line item to %s\'s invoice' % client.first_name},
                    ]
                },
                'hourly_rate': {
                    ContentType.PLAIN_TEXT: 'What hourly rate?',
                    ContentType.MESSENGER: [
                        {'text': 'Last question'},
                        {'text': 'What is the hourly rate for this time sheet?'},
                    ]
                },
            },
            Context.ERROR: {
                'query': {
                    ContentType.PLAIN_TEXT: 'No match found',
                    ContentType.MESSENGER: [
                        {'text': 'I was not able to find any clients who match your query 😓'},
                        {'text': 'Please try another search term'},
                        {'text': 'You can also say "Cancel" to stop this process and add a new client to your account'},
                    ]
                },
                'client': {
                    ContentType.PLAIN_TEXT: 'Not valid',
                    ContentType.MESSENGER: get_client_messages(Context.ERROR)
                },
                'hourly_rate': {
                    ContentType.PLAIN_TEXT: 'I don\'t understand that',
                    ContentType.MESSENGER: [
                        {'text': 'Unfortunately, I cannot understand that'},
                        {'text': 'Please enter in a valid dollar amount'},
                    ]
                },
            }
        }
        message = messages.get(context).get(slot_to_elicit).get(content_type)
        return message


class RetrieveTimeSheetsIntent(FormIntent):
    intent_name = IntentName.RETRIEVE_TIME_SHEETS.value
    form_class = RetrieveTimeSheetsForm
    fields = []
    messages = {}
    time_sheets = None
    next_page_token = None

    def process_dialog(self):
        business = self.get_business()
        if not business:
            outbox = {
                ContentType.PLAIN_TEXT: 'Before you do that, I need you to give me information about your business',
                ContentType.MESSENGER: [
                    {'text': 'Before you do that, I need you to give me some information about your business'},
                    {'text': 'Try saying something like "Link my business" or "Join"'}
                ]
            }
            return self.create_close_response(fulfillment_state=FulfillmentState.FAILED, outbox=outbox)
        elif not TimeSheet.objects.filter(business=business).exists():
            outbox = {
                ContentType.PLAIN_TEXT: 'Before you do that, I need you to give me information about your business',
                ContentType.MESSENGER: [
                    {'text': 'It does not look like you have added time sheets to your account yet'},
                    {'text': 'Try saying something like "Create time sheet"'}
                ]
            }
            return self.create_close_response(fulfillment_state=FulfillmentState.FAILED, outbox=outbox)

        return super(RetrieveTimeSheetsIntent, self).process_dialog()
    
    def get_form_kwargs(self):
        form_kwargs = super(RetrieveTimeSheetsIntent, self).get_form_kwargs()
        form_kwargs['page_size'] = 4
        return form_kwargs

    def get_initial_data(self):
        initial_data = super().get_initial_data()
        business = self.get_business()
        initial_data.update({
            'business': business.id
        })
        return initial_data

    def form_valid(self, form):
        business = self.get_business()
        (self.time_sheets, self.next_page_token) = form.retrieve()
        logger.debug('Retrieved %d time sheets for business %s!' % (len(self.time_sheets), business.name))

    def form_invalid(self, form):
        logger.critical('This should NEVER happen...')
        pass

    def get_fulfilled_message(self, content_type):
        if len(self.time_sheets) > 0:
            is_list_view = bool(self.next_page_token) or len(self.time_sheets) >= 2
            elements = self.get_facebook_elements(self.time_sheets, is_list_view)
            payload = {
                'template_type': 'list' if is_list_view else 'generic',
                'elements': elements
            }
            if is_list_view:
                payload['top_element_style'] = 'compact'
                if self.next_page_token:
                    payload['buttons'] = [{
                        'title': 'View More',
                        'type': 'postback',
                        'payload': json.dumps({
                            'type': Postback.RETRIEVE_TIME_SHEETS.value,
                            'payload': {
                                'next_page_token': self.next_page_token
                            }
                        })
                    }]

            messages = {
                ContentType.PLAIN_TEXT: 'I retrieved your time sheets!',
                ContentType.MESSENGER: [{
                    'attachment': {
                        'type': 'template',
                        'payload': payload
                    }
                }]
            }
        else:
            messages = {
                ContentType.PLAIN_TEXT: 'I could not find any time sheets for that client',
                ContentType.MESSENGER: [
                    {'text': 'Sorry'},
                    {'text': 'It looks like there are no time sheets for that client'},
                ]
            }
        return messages.get(content_type)

    @staticmethod
    def get_facebook_elements(time_sheets, is_list_view):
        elements = []
        running_time_sheet = None
        for time_sheet in time_sheets:
            if time_sheet.is_timing:
                running_time_sheet = time_sheet
                break
        for time_sheet in time_sheets:
            buttons = []
            if is_list_view:
                buttons.append({
                    'type': 'postback',
                    'title': 'Select',
                    'payload': json.dumps({
                        'type': Postback.RETRIEVE_TIME_SHEET.value,
                        'payload': {
                            'time_sheet': time_sheet.id
                        }
                    })
                })
            else:
                if time_sheet.is_timing:
                    buttons.append({
                        'type': 'postback',
                        'title': 'Stop Timer',
                        'payload': json.dumps({
                            'type': Postback.STOP_TIMER.value,
                            'payload': {
                                'time_sheet': time_sheet.id
                            }
                        })
                    })
                elif running_time_sheet is None:
                    buttons.append({
                        'type': 'postback',
                        'title': 'Start Timer',
                        'payload': json.dumps({
                            'type': Postback.START_TIMER.value,
                            'payload': {
                                'time_sheet': time_sheet.id
                            }
                        })
                    })
                if time_sheet.is_invoiceable:
                    buttons.append({
                        'type': 'postback',
                        'title': 'Create Invoice',
                        'payload': json.dumps({
                            'type': Postback.CREATE_INVOICE.value,
                            'payload': {
                                'time_sheet': time_sheet.id
                            }
                        })
                    })
            rounded_hours = time_sheet.rounded_hours
            elements.append({
                'title': time_sheet.client.full_name,
                'subtitle': '%s \u2022 %.1f %s \u2022 $%.0f/hr%s' % (
                    time_sheet.description,
                    rounded_hours,
                    pluralize('hour', rounded_hours),
                    time_sheet.hourly_rate / 100,
                    ' \u2022 RUNNING' if time_sheet.is_timing else ''
                ),
                'buttons': buttons
            })
        return elements


class CreateInvoiceWithTimeSheetIntent(CreateInvoiceIntent):
    intent_name = IntentName.CREATE_INVOICE_WITH_TIME_SHEET.value
    fields = [
        'do_elicit_line_items',
        'line_items',
        'do_confirm',
    ]

    def get_slot_to_elicit_message(self, *, slot_to_elicit, content_type, context=Context.DEFAULT):
        slots = self.session['intents'][self.intent_name]['slots']
        form = self.form_class(data={**slots, **self.get_initial_data()})
        form.is_valid()
        cleaned_data = form.cleaned_data
        business = cleaned_data.get('business')
        client = cleaned_data.get('client')
        time_sheet = cleaned_data.get('time_sheet')
        line_items = cleaned_data.get('line_items')

        # Amount
        amount = 0
        if time_sheet:
            if line_items:
                for line_item in line_items:
                    line_item.order += 1
            else:
                line_items = list()
            line_items.insert(0, LineItem(
                description=time_sheet.description,
                amount=time_sheet.amount,
                order=0,
                time_sheet=time_sheet
            ))
        logger.debug('line_items = %s', line_items)
        if line_items:
            amount = reduce(lambda x, y: x + y.amount, line_items, 0)

        # Confirm invoice url
        line_items = line_items if line_items else []
        confirm_invoice_url = get_absolute_url(url=ConfirmInvoiceView.get_url({
            'line_items': [LineItemSerializer(l).data for l in line_items],
            'business': business.id,
            'client': client.id,
            'has_time_sheet': True
        }))

        messages = {
            Context.DEFAULT: {
                'do_elicit_line_items': {
                    ContentType.PLAIN_TEXT: 'Would you like to add more line items to this invoice?',
                    ContentType.MESSENGER: [
                        {'text': 'Based on the hours you logged, %s will receive an invoice for $%.2f' % (client.full_name, amount / 100)},
                        Intent.get_boolean_message(
                            content_type=ContentType.MESSENGER,
                            text='Would you like to add more line items to this invoice?'
                        )
                    ]
                },
                'line_items': {
                    ContentType.PLAIN_TEXT: 'Please give a description of the services you provided',
                    ContentType.MESSENGER: [
                        {'text': 'What extra line items should I put in the invoice?'},
                        {'text': 'You can enter in as many lines as you want'},
                        {
                            'text': 'Just make sure each line has a description and dollar value like this:\nTransportation Charges $75.00\nSoftware Development $200.00\nExpenses (misc) $182.00'},
                    ]
                },
                'do_confirm': {
                    ContentType.PLAIN_TEXT: 'Do you confirm?',
                    ContentType.MESSENGER: [
                        {'text': '%s will receive an invoice for $%.2f' % (client.first_name, amount / 100)},
                        {'text': 'Please take a second to look over the invoice that I will send out'},
                        {
                            'attachment': {
                                'type': 'template',
                                'payload': {
                                    'template_type': 'generic',
                                    'elements': [
                                        {
                                            'title': 'Invoice',
                                            'image_url': get_absolute_static_url('img/logo.png'),
                                            'default_action': {
                                                "type": "web_url",
                                                "url": confirm_invoice_url,
                                            }
                                        }
                                    ]
                                }

                            }
                        },
                        Intent.get_boolean_message(
                            content_type=ContentType.MESSENGER,
                            text='Would you like me to send out this invoice?'
                        )
                    ]
                },
            },
            Context.ERROR: {
                'do_elicit_line_items': {
                    ContentType.PLAIN_TEXT: 'Do you confirm?',
                    ContentType.MESSENGER: [
                        {'text': 'I don\'t understand that 😕'},
                        {'text': 'If you would like to add more line items say "Yes"'},
                        {'text': 'Otherwise say "No"'},
                        Intent.get_boolean_message(
                            content_type=ContentType.MESSENGER,
                            text='Would you like to add more line items to this invoice?'
                        )
                    ]
                },
                'line_items': {
                    ContentType.PLAIN_TEXT: 'I don\'t understand that',
                    ContentType.MESSENGER: [
                        {'text': 'Unfortunately, I cannot understand that 😔'},
                        {
                            'text': 'Please enter line items that match these examples:\nTransportation Charges $75.00\nSoftware Development $200.00\nExpenses (misc) $182.00'},
                    ]
                },
                'do_confirm': {
                    ContentType.PLAIN_TEXT: 'Do you confirm?',
                    ContentType.MESSENGER: [
                        {'text': 'I don\'t understand that 😕'},
                        {'text': 'If you would like to send out this invoice say "Yes"'},
                        {'text': 'Otherwise say "No"'},
                        Intent.get_boolean_message(
                            content_type=ContentType.MESSENGER,
                            text='Would you like me to send out this invoice?'
                        )
                    ]
                },
            }
        }
        message = messages.get(context).get(slot_to_elicit).get(content_type)
        return message


class StopTimeSheetIntent(Intent):
    intent_name = IntentName.STOP_TIME_SHEET.value
    stopped_time_sheet = None

    def get_failed_message(self, content_type):
        business = self.get_business()
        if TimeSheet.objects.filter(business=business).exists():
            messages = {
                ContentType.PLAIN_TEXT: 'It looks like all of your timers are already stopped',
                ContentType.MESSENGER: [
                    {'text': 'It looks like all of your timers are already stopped'},
                ]
            }
        else:
            messages = {
                ContentType.PLAIN_TEXT: 'You don\'t have any time sheets',
                ContentType.MESSENGER: [
                    {'text': 'It looks like you don\'t have any time sheets'},
                    {'text': 'Try saying something like "Create time sheet"'},
                ]
            }
        return messages.get(content_type)

    def get_fulfilled_message(self, content_type):
        client = self.stopped_time_sheet.client
        messages = {
            ContentType.PLAIN_TEXT: 'I have stopped your timer',
            ContentType.MESSENGER: [
                {'text': 'I have stopped your timer'},
                {'text': 'So far you have %.1f hours logged for the work you are doing for %s' % (self.stopped_time_sheet.rounded_hours, client.full_name)},
                {
                    'attachment': {
                        'type': 'template',
                        'payload': {
                            'template_type': 'generic',
                            'elements': RetrieveTimeSheetsIntent.get_facebook_elements([self.stopped_time_sheet], False),
                        }
                    }
                }
            ]
        }
        return messages.get(content_type)

    def process_fulfillment(self):
        self.session.pop('intents')
        self.session.save()

        # Get running timer
        business = self.get_business()
        for time_sheet in TimeSheet.objects.filter(business=business):
            if time_sheet.is_timing:
                time_sheet.stop_timer()
                self.stopped_time_sheet = time_sheet
                break
        return self.create_close_response(
            fulfillment_state=FulfillmentState.FULFILLED if self.stopped_time_sheet is not None else FulfillmentState.FAILED
        )