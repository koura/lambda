from __future__ import absolute_import

from django.core import cache
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        cache.clear()
