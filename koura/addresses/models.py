from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.contrib.gis.db import models


class AbstractModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Address(AbstractModel):
    street_address_1 = models.CharField(max_length=512, blank=False)
    street_address_2 = models.CharField(max_length=512, blank=True)
    locality = models.CharField(max_length=512, blank=False)
    region = models.CharField(max_length=512, blank=False)
    country = models.CharField(max_length=512, blank=False)
    postal_code = models.CharField(max_length=512, blank=False)

