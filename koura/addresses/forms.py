from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django import forms

from koura.addresses.models import Address


class AddressForm(forms.ModelForm):
    class Meta:
        model = Address
        fields = [
            'street_address_1',
            'street_address_2',
            'locality',
            'region',
            'country',
            'postal_code'
        ]
