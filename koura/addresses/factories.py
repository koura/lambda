from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import factory

from koura.addresses.models import Address


class AddressFactory(factory.DjangoModelFactory):
    street_address_1 = factory.Faker('street_address')
    street_address_2 = factory.Faker('secondary_address')
    locality = factory.Faker('city')
    region = factory.Faker('state')
    country = 'US'
    postal_code = factory.Faker('zipcode')

    class Meta:
        model = Address
