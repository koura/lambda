from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.test import TestCase

from koura.addresses.forms import AddressForm
from koura.addresses.models import Address


class AddressFormTestCase(TestCase):
    def setUp(self):
        self.data = {
            'street_address_1': '221B Baker Street',
            'locality': 'Stockton',
            'region': 'California',
            'country': 'US',
            'postal_code': '12334'
        }

    def tearDown(self):
        Address.objects.all().delete()

    def test_init(self):
        form = AddressForm()
        self.assertIsNotNone(form)

    def test_create_address(self):
        data = self.data.copy()
        form = AddressForm(data=data)
        self.assertTrue(form.is_valid())
        address = form.save()
        self.assertIsNotNone(address)
        self.assertEqual(Address.objects.all().count(), 1)
        for key, value in data.items():
            self.assertEqual(value, getattr(address, key))

    def test_street_name_2(self):
        data = self.data.copy()
        data['street_address_2'] = 'SMC 2076'
        form = AddressForm(data=data)
        self.assertTrue(form.is_valid())
        address = form.save()
        self.assertIsNotNone(address)
        self.assertEqual(Address.objects.all().count(), 1)
        for key, value in data.items():
            self.assertEqual(value, getattr(address, key))
