from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import ast
import json
import logging

import boto3
import copy
import re
import sys
import yaml
from yamlreader import yaml_load


logging.basicConfig(level=logging.INFO, format='%(message)s')
logger = logging.getLogger()

# Initialize client
LEX_CLIENT = boto3.client('lex-models')
LAMBDA_CLIENT = boto3.client('lambda')
MESSAGE_VERSION = '1.0'


def get_last_checksum(name, resource_type):
    next_token = None
    versions = []

    functions = {
        'slotTypes': {
            'get_versions': LEX_CLIENT.get_slot_type_versions,
            'get': LEX_CLIENT.get_slot_type
        },
        'intents': {
            'get_versions': LEX_CLIENT.get_intent_versions,
            'get': LEX_CLIENT.get_intent
        },
        'bots': {
            'get_versions': LEX_CLIENT.get_bot_versions,
            'get': LEX_CLIENT.get_bot
        }
    }
    while True:
        if next_token:
            response = functions.get(resource_type).get('get_versions')(name=name, nextToken=next_token, maxResults=50)
        else:
            response = functions.get(resource_type).get('get_versions')(name=name, maxResults=50)
        for data in response.get(resource_type, []):
            versions.append(data.get('version'))
        logger.debug(response)
        next_token = response.get('nextToken')
        if not next_token:
            break
    if not versions:
        return None
    if resource_type == 'bots':
        kwargs = {
            'name': name,
            'versionOrAlias': '$LATEST'
        }
    else:
        kwargs = {
            'name': name,
            'version': '$LATEST'
        }
    response = functions.get(resource_type).get('get')(**kwargs)
    return response.get('checksum')


def create_slot_types(config):
    slot_types = {}
    for key, data in config.get('slot_types', {}).items():
        if 'AMAZON' in key:
            continue
        last_checksum = get_last_checksum(data.get('name'), 'slotTypes')
        enumeration_values = [{'value': x} for x in data.get('enumeration_values')]
        kwargs = {
            'name': data.get('name'),
            'enumerationValues': enumeration_values,
        }
        if data.get('description'):
            kwargs['description'] = data.get('description')
        if last_checksum:
            kwargs['checksum'] = last_checksum
        response = LEX_CLIENT.put_slot_type(**kwargs)
        logger.debug(response)

        # Slot type version
        response = LEX_CLIENT.create_slot_type_version(name=data.get('name'))
        logger.debug(response)
        slot_types[key] = {
            'name': response.get('name'),
            'version': response.get('version')
        }

    return slot_types


def get_code_hook(data):
    response = LAMBDA_CLIENT.get_function(FunctionName=data.get('name'))
    return {
        'uri': response.get('Configuration').get('FunctionArn'),
        'messageVersion': MESSAGE_VERSION
    }


# Yuck...
def get_camel_case(text):
    camel_case_text = copy.deepcopy(text)
    if camel_case_text == 'idle_session_ttl_in_seconds':
        return 'idleSessionTTLInSeconds'
    for substring in re.compile('_[A-Za-z]').findall(text):
        camel_case_text = camel_case_text.replace(substring, substring[-1].upper())
    return camel_case_text


def clean_payload(root, slot_types=None, intents=None):
    payload = copy.deepcopy(root)
    stack = [payload]
    while len(stack) > 0:
        x = stack.pop()
        y = {}
        keys_to_remove = []
        for key, value in x.items():
            if not value:
                keys_to_remove.append(key)
                continue
            if key in ['messages']:
                y[key] = [{'content_type': 'PlainText', 'content': m} for m in value]
            elif key == 'child_directed':
                y[key] = ast.literal_eval(value.capitalize())
            elif key == 'slot_type' and 'AMAZON' not in value:
                y[key] = slot_types.get(value).get('name')
                y['slot_type_version'] = slot_types.get(value).get('version')
            elif key == 'intents':
                new_values = []
                for intent_key in value:
                    new_values.append({
                        'intentName': intents.get(intent_key).get('name'),
                        'intentVersion': intents.get(intent_key).get('version'),
                    })
                y[key] = new_values
            elif key in ['code_hook', 'dialog_code_hook']:
                y[key] = get_code_hook(value)
            elif isinstance(value, dict):
                stack.append(value)
            elif isinstance(value, list):
                for v in value:
                    if isinstance(v, dict):
                        stack.append(v)
        for key, value in y.items():
            x[key] = value
        for key in keys_to_remove:
            del x[key]
        old_keys = x.keys()
        for key in old_keys:
            value = x.get(key)
            del x[key]
            x[get_camel_case(key)] = value

    return to_camel_case(payload)


def to_camel_case(root):
    payload = copy.deepcopy(root)
    stack = [payload]
    while len(stack) > 0:
        x = stack.pop()
        old_keys = x.keys()

        for key, value in x.items():
            if isinstance(value, dict):
                stack.append(value)
            if isinstance(value, list):
                for v in value:
                    if isinstance(v, dict):
                        stack.append(v)
        for key in old_keys:
            value = x.get(key)
            del x[key]
            x[get_camel_case(key)] = value
    return payload


def create_intents(config, slot_types):
    intents = {}
    for key, data in config.get('intents', {}).items():
        last_checksum = get_last_checksum(data.get('name'), 'intents')

        # Build payload
        data['checksum'] = last_checksum
        payload = clean_payload(data, slot_types)

        # Create intent
        response = LEX_CLIENT.put_intent(**payload)

        # Slot type version
        response = LEX_CLIENT.create_intent_version(name=data.get('name'))
        logger.debug(response)

        intents[key] = {
            'name': response.get('name'),
            'version': response.get('version')
        }

    return intents


def create_bots(config, intents, slot_types):
    bots = {}
    for key, data in config.get('bots', {}).items():
        last_checksum = get_last_checksum(data.get('name'), 'bots')

        # Build payload
        data['checksum'] = last_checksum
        payload = clean_payload(data, slot_types, intents)
        # payload['processBehavior'] = 'BUILD'
        logger.debug(json.dumps(payload, indent=2, sort_keys=True))

        # Create bot
        response = LEX_CLIENT.put_bot(**payload)
        logger.info(response)

        # Add a version
        response = LEX_CLIENT.create_bot_version(name=data.get('name'))
        logger.info(response)

        bots[key] = {
            'name': response.get('name'),
            'version': response.get('version')
        }

    return bots


config = yaml_load('*.yaml')

# Start creating slot types
slot_types = create_slot_types(config)
logger.info('Loaded %d slot types\n%s' % (len(slot_types), '\n'.join([s.get('name') for s in slot_types.values()])))

# Create intents
intents = create_intents(config, slot_types)
logger.info('Loaded %d intents\n%s' % (len(intents), '\n'.join([s.get('name') for s in intents.values()])))

# Create bots
bots = create_bots(config, intents, slot_types)
logger.info('Loaded %d bots\n%s' % (len(bots), '\n'.join([s.get('name') for s in bots.values()])))
