from faker import Faker
import yaml

email_addresses = set()
fake = Faker()

while len(email_addresses) < 10000:
    email_addresses.add(fake.email())

with open('email_addresses.yaml', 'w') as yaml_file:
    data = {
        'slot_types': {
            'email_address': {
                'name': 'EmailAddress',
                'enumeration_values': list(email_addresses)
            }
        }
    }
    yaml_file.write(yaml.dump(data, default_flow_style=False))
