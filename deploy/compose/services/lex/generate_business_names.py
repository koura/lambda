from faker import Faker
import yaml

business_names = set()
fake = Faker()

business_names.add('ez3 Softworks LLC')
while len(business_names) < 10000:
    business_names.add(fake.company())

with open('business_names.yaml', 'w') as yaml_file:
    data = {
        'slot_types': {
            'business_name': {
                'name': 'BusinessName',
                'enumeration_values': list(business_names)
            }
        }
    }
    yaml_file.write(yaml.dump(data, default_flow_style=False))
