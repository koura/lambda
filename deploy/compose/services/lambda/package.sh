#!/bin/bash

# Create virtualenv
rm -rfv /var/task/bin /var/task/lib /var/task/include
virtualenv /var/task/ -p /usr/bin/python3
source /var/task/bin/activate

pip install -r /var/task/requirements.txt
cp --preserve=links -fv /var/task/deploy/compose/services/lambda/lib/* /var/task/lib/
cd /var/task/lib
rm -rfv libgeos.so
rm -rfv libgeos_c.so.1
rm -rfv libgeos_c.so

cd /var/task
rm -rfv local/
zappa package production

mv /var/task/handler*.zip /var/zappa/handler.zip
mv /var/task/koura*.zip /var/zappa/koura_current_project.zip
