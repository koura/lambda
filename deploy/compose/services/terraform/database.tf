resource "aws_security_group" "database" {
  name = "${var.app_name}-${var.environment}-database"
  description = "Firewall for database"
  vpc_id = "${aws_vpc.default.id}"
  ingress {
    from_port = "${var.database_port}"
    to_port = "${var.database_port}"
    protocol = "tcp"
    security_groups = ["${aws_security_group.lambda.id}"]
  }
  tags {
    Name = "${var.app_name}-${var.environment}-database"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}

resource "aws_db_subnet_group" "default" {
  subnet_ids = ["${aws_subnet.private.*.id}"]
}

resource "aws_db_instance" "default" {
  availability_zone    = "${var.aws_availability_zone}"
  allocated_storage    = "${var.database_allocated_storage}"
  storage_type         = "${var.database_storage_type}"
  engine               = "${var.database_engine}"
  instance_class       = "${var.database_instance_class}"
  name                 = "${var.app_name}"
  username             = "${var.database_user}"
  password             = "${var.database_password}"
  port                 = "${var.database_port}"
  vpc_security_group_ids = ["${aws_security_group.database.id}"]
  db_subnet_group_name   = "${aws_db_subnet_group.default.name}"

  tags {
    Name = "${var.app_name}-${var.environment}"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}