data "aws_iam_policy_document" "lambda_execution" {
  statement {
    actions = [
      "s3:*"
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.default.id}",
      "arn:aws:s3:::${aws_s3_bucket.default.id}/*",
    ]
  }
  statement {
    actions = [
      "ses:*"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "sqs:*"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "lex:PostText",
      "lex:PostContent"
    ]
    resources = [
      "arn:aws:lex:${var.aws_region}:${data.aws_caller_identity.current.account_id}:bot:${var.lex_bot_name}:${var.lex_bot_alias}"
    ]
  }
  statement {
    actions = [
      "rds:*"
    ]
    resources = [
      "arn:aws:rds:::${aws_db_instance.default.id}"
    ]
  }
  statement {
    actions = [
      "logs:*"
    ]
    resources = [
      "arn:aws:logs:*:*:*"
    ]
  }
  statement {
    actions = [
      "lambda:InvokeFunction"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "ec2:AttachNetworkInterface",
      "ec2:CreateNetworkInterface",
      "ec2:DeleteNetworkInterface",
      "ec2:DescribeInstances",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DetachNetworkInterface",
      "ec2:ModifyNetworkInterfaceAttribute",
      "ec2:ResetNetworkInterfaceAttribute"
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_role_policy" "lambda_execution" {
  name = "${var.app_name}-${var.environment}-LambdaExecutionPolicy"
  role = "${aws_iam_role.lambda.id}"
  policy = "${data.aws_iam_policy_document.lambda_execution.json}"
}

data "aws_iam_policy_document" "lambda_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = [
        "ec2.amazonaws.com",
        "lambda.amazonaws.com",
        "events.amazonaws.com",
        "apigateway.amazonaws.com",
        "lex.amazonaws.com",
        "ses.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_role" "lambda" {
  name = "${var.app_name}-${var.environment}-LambdaExecutionRole"
  assume_role_policy = "${data.aws_iam_policy_document.lambda_assume_role.json}"
}

resource "aws_security_group" "lambda" {
  name = "${var.app_name}-${var.environment}-lambda"
  description = "Firewall for lambda"
  vpc_id = "${aws_vpc.default.id}"

  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.app_name}-${var.environment}-lambda"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }

  depends_on = ["aws_vpc.default"]
}

resource "aws_s3_bucket_object" "lambda_handler" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "${var.lambda_handler_s3_key}"
  source = "${var.lambda_handler_file_path}"
  etag   = "${md5(file(var.lambda_handler_file_path))}"
}

resource "aws_s3_bucket_object" "lambda_project" {
  bucket = "${aws_s3_bucket.default.id}"
  key = "${var.lambda_project_s3_key}"
  source = "${var.lambda_project_file_path}"
  etag   = "${md5(file(var.lambda_project_file_path))}"
}

resource "aws_lambda_function" "default" {
  s3_bucket = "${aws_s3_bucket.default.id}"
  s3_key = "${var.lambda_handler_s3_key}"
  source_code_hash = "${base64sha256(file(var.lambda_handler_file_path))}"
  function_name    = "${var.app_name}-${var.environment}"
  role             = "${aws_iam_role.lambda.arn}"
  handler          = "${var.lambda_handler}"
  runtime          = "${var.lambda_runtime}"
  memory_size = "${var.lambda_memory_size}"
  timeout = "${var.lambda_timeout}"
  vpc_config {
    security_group_ids = ["${aws_security_group.lambda.id}"]
    subnet_ids = ["${aws_subnet.private.*.id}"]
  }
  environment {
    variables = {
      DJANGO_SECRET_KEY = "${var.django_secret_key}"
      POSTGRES_NAME = "${aws_db_instance.default.name}"
      POSTGRES_USER = "${aws_db_instance.default.username}"
      POSTGRES_PASSWORD = "${aws_db_instance.default.password}"
      POSTGRES_HOST = "${aws_db_instance.default.address}"
      POSTGRES_PORT = "${aws_db_instance.default.port}"
      REDIS_HOST = "${aws_elasticache_cluster.default.cache_nodes.0.address}"
      REDIS_PORT = "${aws_elasticache_cluster.default.cache_nodes.0.port}"
      CACHE_BACKEND = "${var.lambda_cache_backend}"
      EMAIL_BACKEND = "${var.lambda_email_backend}"
      DEBUG = "${var.debug}"
      LD_LIBRARY_PATH = "${var.lambda_ld_library_path}"
      GDAL_LIBRARY_PATH = "${var.lambda_gdal_library_path}"
      GEOS_LIBRARY_PATH = "${var.lambda_geos_library_path}"
      DEFAULT_FILE_STORAGE = "${var.lambda_default_file_storage}"
      AWS_STORAGE_BUCKET_NAME = "${aws_s3_bucket.default.id}"
      AWS_CLOUDFRONT_DOMAIN = "${aws_cloudfront_distribution.default.domain_name}"
      STATICFILES_STORAGE = "${var.lambda_staticfiles_storage}"
      STATICFILES_LOCATION = "${var.lambda_staticfiles_location}"
      AWS_COGNITO_IDENTITY_POOL_NAME = "${aws_cognito_identity_pool.default.identity_pool_name}"
      AWS_COGNITO_DEVELOPER_PROVIDER_NAME = "${aws_cognito_identity_pool.default.developer_provider_name}"
      SITE_DOMAIN = "${var.site_domain}"
      SITE_NAME = "${var.site_name}"
      SITE_DISPLAY_NAME = "${var.site_display_name}"
      FACEBOOK_PUBLIC_KEY = "${var.facebook_public_key}"
      FACEBOOK_SECRET_KEY = "${var.facebook_secret_key}"
      STRIPE_PUBLIC_KEY = "${var.stripe_public_key}"
      STRIPE_SECRET_KEY = "${var.stripe_secret_key}"
      MESSENGER_VERIFY_TOKEN = "${var.messenger_verify_token}"
      MESSENGER_PAGE_TOKEN = "${var.messenger_page_token}"
      AWS_LEX_BOT_NAME = "${var.lex_bot_name}"
      AWS_LEX_BOT_ALIAS = "${var.lex_bot_alias}"
      AWS_SQS_IS_ENABLED = "${var.sqs_is_enabled}"
//      PYTHONPATH = "${var.lambda_python_path}"
    }
  }
  tags {
    Project = "${var.project_name}"
  }


  depends_on = ["aws_s3_bucket_object.lambda_handler"]
}