//data "aws_iam_policy_document" "s3_private_bucket" {
//  statement {
//    principals {
//      identifiers = [
//        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root",
//        "${aws_iam_role.lambda.arn}",
//      ]
//      type = "AWS"
//    }
//    actions = [
//      "s3:*"
//    ]
//    resources = [
//      "arn:aws:s3:::${var.s3_private_bucket}",
//      "arn:aws:s3:::${var.s3_private_bucket}/*",
//    ]
//  }
//  statement {
//    principals {
//      identifiers = [
//        "*",
//      ]
//      type = "*"
//    }
//    actions = [
//      "s3:*"
//    ]
//    resources = [
//      "arn:aws:s3:::${var.s3_private_bucket}",
//      "arn:aws:s3:::${var.s3_private_bucket}/*",
//    ]
//    condition {
//      test = "StringEquals"
//      values = ["${aws_vpc.default.id}"]
//      variable = "aws:sourceVpc"
//    }
//  }
//}

data "aws_iam_policy_document" "s3_bucket" {
  statement {
    principals {
      identifiers = ["${aws_cloudfront_origin_access_identity.default.s3_canonical_user_id}"]
      type = "CanonicalUser"
    }
    actions = [
      "s3:GetObject",
    ]
    resources = [
      "arn:aws:s3:::${var.s3_bucket_name}/*",
    ]
  }
  
  statement {
    principals {
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root",
        "${aws_iam_role.lambda.arn}",
      ]
      type = "AWS"
    }
    actions = [
      "s3:*"
    ]
    resources = [
      "arn:aws:s3:::${var.s3_bucket_name}",
      "arn:aws:s3:::${var.s3_bucket_name}/*",
    ]
  }
  statement {
    principals {
      identifiers = [
        "*",
      ]
      type = "*"
    }
    actions = [
      "s3:*"
    ]
    resources = [
      "arn:aws:s3:::${var.s3_bucket_name}",
      "arn:aws:s3:::${var.s3_bucket_name}/*",
    ]
    condition {
      test = "StringEquals"
      values = ["${aws_vpc.default.id}"]
      variable = "aws:sourceVpc"
    }
  }
}


//resource "aws_s3_bucket" "private" {
//  bucket = "${var.s3_private_bucket}"
//  acl = "private"
//  policy = "${data.aws_iam_policy_document.s3_private_bucket.json}"
//  tags {
//    Name = "${var.app_name}"
//    Environment = "${var.environment}"
//    Project = "${var.project_name}"
//  }
//}

resource "aws_s3_bucket" "default" {
  bucket = "${var.app_name}-${var.environment}"
  acl    = "private"
  policy = "${data.aws_iam_policy_document.s3_bucket.json}"
  cors_rule {
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
    allowed_headers = ["Authorization"]
  }
  tags {
    Name = "${var.app_name}"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}