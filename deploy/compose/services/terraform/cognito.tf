data "aws_iam_policy_document" "cognito_assume_role" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    principals {
      type = "Federated"
      identifiers = [
        "cognito-identity.amazonaws.com",
      ]
    }
    condition {
      test = "StringEquals"
      variable ="cognito-identity.amazonaws.com:aud"
      values = [
        "${aws_cognito_identity_pool.default.id}"
      ]
    }
    condition {
      test = "ForAnyValue:StringLike"
      variable ="cognito-identity.amazonaws.com:amr"
      values = [
        "authenticated"
      ]
    }
  }
}


data "aws_iam_policy_document" "cognito_unauthenticated_user" {
  statement {
    actions = [
      "lex:PostText",
      "lex:PostContent"
    ]
    resources = [
      "arn:aws:lex:${var.aws_region}:${data.aws_caller_identity.current.account_id}:bot:${var.lex_bot_name}:*",
    ]
  }
  statement {
    actions = [
        "mobileanalytics:PutEvents",
        "cognito-sync:*",
        "cognito-identity:*"
    ],
    resources = [
      "*"
    ]
  }
}

data "aws_iam_policy_document" "cognito_authenticated_user" {
  statement {
    actions = [
      "lex:PostText",
      "lex:PostContent"
    ]
    resources = [
      "arn:aws:lex:${var.aws_region}:${data.aws_caller_identity.current.account_id}:bot:${var.lex_bot_name}:*",
    ]
  }
  statement {
    actions = [
        "mobileanalytics:PutEvents",
        "cognito-sync:*",
        "cognito-identity:*"
    ],
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_role" "cognito_unauthenticated_user" {
  name = "${var.app_name}-${var.environment}-CognitoUnauthenticatedUserRole"
  assume_role_policy = "${data.aws_iam_policy_document.cognito_assume_role.json}"
}

resource "aws_iam_role_policy" "cognito_unauthenticated_user" {
  name = "${var.app_name}-${var.environment}-CognitoUnauthenticatedUserPolicy"
  role = "${aws_iam_role.cognito_unauthenticated_user.id}"
  policy = "${data.aws_iam_policy_document.cognito_unauthenticated_user.json}"
}

resource "aws_iam_role" "cognito_authenticated_user" {
  name = "${var.app_name}-${var.environment}-CognitoAuthenticatedUserRole"
  assume_role_policy = "${data.aws_iam_policy_document.cognito_assume_role.json}"
}

resource "aws_iam_role_policy" "cognito_authenticated_user" {
  name = "${var.app_name}-${var.environment}-CognitoAuthenticatedUserPolicy"
  role = "${aws_iam_role.cognito_authenticated_user.id}"
  policy = "${data.aws_iam_policy_document.cognito_authenticated_user.json}"
}


resource "aws_cognito_identity_pool" "default" {
  identity_pool_name = "${var.cognito_identity_pool_name}"
  allow_unauthenticated_identities = true
  developer_provider_name = "${var.cognito_developer_provider_name}"
}
