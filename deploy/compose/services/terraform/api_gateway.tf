data "aws_acm_certificate" "default" {
  domain   = "${var.site_domain}"
  statuses = ["ISSUED"]
}

resource "aws_api_gateway_domain_name" "default" {
  domain_name = "${var.site_domain}"
  certificate_arn = "${data.aws_acm_certificate.default.arn}"
}

resource "aws_api_gateway_rest_api" "default" {
  name        = "${var.api_name}"
  description = ""
  binary_media_types = ["*/*"]
  depends_on = [
    "aws_lambda_function.default"
  ]
}

resource "aws_api_gateway_resource" "child" {
  parent_id = "${aws_api_gateway_rest_api.default.root_resource_id}"
  path_part = "{proxy+}"
  rest_api_id = "${aws_api_gateway_rest_api.default.id}"
}

resource "aws_api_gateway_method" "parent" {
  rest_api_id   = "${aws_api_gateway_rest_api.default.id}"
  resource_id   = "${aws_api_gateway_rest_api.default.root_resource_id}"
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_method" "child" {
  rest_api_id   = "${aws_api_gateway_rest_api.default.id}"
  resource_id   = "${aws_api_gateway_resource.child.id}"
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_lambda_permission" "api_gateway" {
  statement_id = "AllowExecutionFromAPIGateway"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.default.function_name}"
  principal = "apigateway.amazonaws.com"
}

resource "aws_lambda_permission" "api_gateway_method" {
  statement_id = "AllowExecutionFromAPIGatewayMethod"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.default.function_name}"
  principal = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:${var.aws_region}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.default.id}/*/*/"
}

resource "aws_api_gateway_integration" "parent" {
  rest_api_id             = "${aws_api_gateway_rest_api.default.id}"
  resource_id             = "${aws_api_gateway_rest_api.default.root_resource_id}"
  http_method             = "${aws_api_gateway_method.parent.http_method}"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "arn:aws:apigateway:${var.aws_region}:lambda:path/2015-03-31/functions/${aws_lambda_function.default.arn}/invocations"

  depends_on = [
    "aws_lambda_permission.api_gateway"
  ]
}

resource "aws_api_gateway_integration" "child" {
  rest_api_id             = "${aws_api_gateway_rest_api.default.id}"
  resource_id             = "${aws_api_gateway_resource.child.id}"
  http_method             = "${aws_api_gateway_method.child.http_method}"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "arn:aws:apigateway:${var.aws_region}:lambda:path/2015-03-31/functions/${aws_lambda_function.default.arn}/invocations"

  depends_on = [
    "aws_lambda_permission.api_gateway"
  ]
}

resource "aws_api_gateway_deployment" "default" {
  rest_api_id = "${aws_api_gateway_rest_api.default.id}"
  stage_name  = "${var.environment}"
  depends_on = [
    "aws_api_gateway_integration.parent"
  ]
}

resource "aws_api_gateway_base_path_mapping" "test" {
  api_id      = "${aws_api_gateway_rest_api.default.id}"
  stage_name  = "${aws_api_gateway_deployment.default.stage_name}"
  domain_name = "${aws_api_gateway_domain_name.default.domain_name}"
}

output "invoke_url" {
  value = "${aws_api_gateway_deployment.default.invoke_url}"
}

