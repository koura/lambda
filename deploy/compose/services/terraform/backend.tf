terraform {
  backend "s3" {
    bucket = "koura-production"
    key    = "terraform/terraform.tfstate"
    region = "us-east-1"
  }
}