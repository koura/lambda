variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "django_secret_key" {}
variable "facebook_public_key" {}
variable "facebook_secret_key" {}
variable "stripe_public_key" {}
variable "stripe_secret_key" {}
variable "messenger_verify_token" {}
variable "messenger_page_token" {}
variable "environment" {}
variable "debug" {}

variable "aws_region" {
  default = "us-east-1"
}
variable "aws_availability_zone" {
  default = "us-east-1a"
}
variable "aws_private_availability_zones" {
  type = "list"
  default = [
    "us-east-1a",
    "us-east-1b",
    "us-east-1c"
  ]
}

variable "site_domain" {
}
variable "site_name" {
}
variable "site_display_name" {
}
variable "api_name" {
  default = "Koura"
}
variable "app_name" {
  default = "koura"
}
variable "project_name" {
  default = "Koura"
}
variable "s3_bucket_name" {}


variable "cache_node_type" {
  default = "cache.t2.micro"
}
variable "cache_port" {
  default = 6379
}
variable "cache_engine" {
  default = "redis"
}
variable "cache_cluster_id" {
  default = "koura"
}
variable "cache_num_cache_nodes" {
  default = 1
}
variable "cache_parameter_group_name" {
  default = "default.redis3.2"
}

variable "database_user" {
  default = "postgres"
}
variable "database_password" {
  default = "c38bab650dcb46c59e0541fa0bc23b96"
}
variable "database_instance_class" {
  default = "db.t2.micro"
}
variable "database_engine" {
  default = "postgres"
}
variable "database_allocated_storage" {
  default = 20
}
variable "database_storage_type" {
  default = "gp2"
}
variable "database_port" {
  default = 5432
}

variable "vpc_cidr" {
  description = "CIDR for the whole VPC"
  default = "10.0.0.0/16"
}
variable "public_subnet_cidr" {
  description = "CIDR for the Public Subnet"
  default = "10.0.0.0/24"
}
variable "private_subnet_cidrs" {
  description = "CIDR for the Private Subnet"
  type = "list"
  default = [
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24",
  ]
}

variable "lambda_runtime" {
  default = "python3.6"
}
variable "lambda_handler" {
  default = "handler.lambda_handler"
}
variable "lambda_memory_size" {
  default = 1024
}
variable "lambda_timeout" {
  default = 300
}
variable "lambda_handler_s3_key" {
  default = "zappa/handler.zip"
}
variable "lambda_project_s3_key" {
  default = "zappa/koura_current_project.zip"
}
variable "lambda_handler_file_path" {
  default = "../../handler.zip"
}
variable "lambda_project_file_path" {
  default = "../../koura_current_project.zip"
}
variable "lambda_nltk_data_path" {
  default = "/tmp/koura/deploy/compose/services/lambda/nltk_data/"
}
variable "lambda_ld_library_path" {
  default = "/var/lang/lib:/lib64:/usr/lib64:/var/runtime:/var/runtime/lib:/var/task:/var/task/lib:/tmp/koura/lib"
}
variable "lambda_gdal_library_path" {
  default = "/tmp/koura/lib/libgdal.so"
}
variable "lambda_geos_library_path" {
  default = "/tmp/koura/lib/libgeos_c.so"
}
variable "lambda_python_path" {
  default = "/var/runtime:/tmp/koura:/tmp/koura/koura:/tmp/koura/koura/koura"
}
variable "lambda_default_file_storage" {
  default = "koura.storages.S3MediaStorage"
}
variable "lambda_staticfiles_storage" {
  default = "koura.storages.S3StaticStorage"
}
variable "lambda_cache_backend" {
  default = "django_redis.cache.RedisCache"
}
variable "lambda_email_backend" {
  default = "django_amazon_ses.backends.boto.EmailBackend"
}
variable "lambda_staticfiles_location" {}

variable "lex_bot_name" {}
variable "lex_bot_alias" {}

variable "cognito_identity_pool_name" {}
variable "cognito_developer_provider_name" {}

variable "cname_records" {
  type = "map"
  default = {
    "r77mqxlcfaqeoy5mygk7xokot4bqfxik._domainkey.getkoura.com" = "r77mqxlcfaqeoy5mygk7xokot4bqfxik.dkim.amazonses.com"
    "l4p6ycu62yzako74e6hjyk6mqaazhwaf._domainkey.getkoura.com" = "l4p6ycu62yzako74e6hjyk6mqaazhwaf.dkim.amazonses.com"
    "tmkxkw347qjrkr4y7nsbnwtybnouwqhg._domainkey.getkoura.com" = "tmkxkw347qjrkr4y7nsbnwtybnouwqhg.dkim.amazonses.com"
    "autodiscover.getkoura.com" = "autodiscover.mail.us-east-1.awsapps.com"
  }
}

variable "mx_records" {
  type = "map"
  default = {
    "getkoura.com" = "10 inbound-smtp.us-east-1.amazonaws.com"
  }
}

variable "sqs_is_enabled" {
  default = "True"
}