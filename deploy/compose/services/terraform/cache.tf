resource "aws_security_group" "cache" {
  name = "${var.app_name}-${var.environment}-cache"
  description = "Firewall for cache"
  vpc_id = "${aws_vpc.default.id}"
  ingress {
    from_port = "${var.cache_port}"
    to_port = "${var.cache_port}"
    protocol = "tcp"
    security_groups = ["${aws_security_group.lambda.id}"]
  }
  tags {
    Name = "${var.app_name}-${var.environment}-cache"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}

resource "aws_elasticache_subnet_group" "default" {
  name = "${var.app_name}-${var.environment}-cache"
  subnet_ids = ["${aws_subnet.private.*.id}"]

}

resource "aws_elasticache_cluster" "default" {
  cluster_id           = "${var.cache_cluster_id}"
  engine               = "${var.cache_engine}"
  node_type            = "${var.cache_node_type}"
  port                 = "${var.cache_port}"
  num_cache_nodes      = "${var.cache_num_cache_nodes}"
  parameter_group_name = "${var.cache_parameter_group_name}"
  security_group_ids = ["${aws_security_group.cache.id}"]
  subnet_group_name = "${aws_elasticache_subnet_group.default.name}"
  tags {
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}
