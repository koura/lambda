resource "aws_cloudfront_origin_access_identity" "default" {
  comment = "Koura"
}

resource "aws_cloudfront_distribution" "default" {
  origin {
    domain_name = "${aws_s3_bucket.default.bucket_domain_name}"
    origin_id   = "${aws_s3_bucket.default.bucket_domain_name}"

    s3_origin_config {
      origin_access_identity = "${aws_cloudfront_origin_access_identity.default.cloudfront_access_identity_path}"
    }
  }

  enabled = true

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "${aws_s3_bucket.default.bucket_domain_name}"
    compress = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
    
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 31557600
    max_ttl                = 31557600
  }

  price_class = "PriceClass_All"

  viewer_certificate {
    cloudfront_default_certificate = true
  }
  
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  
  tags {
    Name = "${var.app_name}"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }

}

output "cloudfront_domain_name" {
  value = "${aws_cloudfront_distribution.default.domain_name}"
}

