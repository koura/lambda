resource "aws_vpc" "default" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = false
  enable_dns_support = true
  tags {
    Name = "${var.app_name}-${var.environment}"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}

/*
  Private Subnets
*/
resource "aws_subnet" "private" {
  count = "${length(var.aws_private_availability_zones)}"
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "${element(var.private_subnet_cidrs, count.index)}"
  availability_zone = "${element(var.aws_private_availability_zones, count.index)}"
  tags {
    Name = "${var.app_name}-${var.environment}-${element(var.aws_private_availability_zones, count.index)}-private"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}
resource "aws_route_table" "private" {
  count = "${length(var.aws_private_availability_zones)}"
  vpc_id = "${aws_vpc.default.id}"
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.default.id}"
  }

  tags {
    Name = "Private Subnet"
    Project = "${var.project_name}"
  }
}

resource "aws_route_table_association" "private" {
  count = "${length(var.aws_private_availability_zones)}"
  subnet_id = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private.*.id, count.index)}"
}

# Public Internet
resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"
  tags {
    Name = "${var.app_name}-${var.environment}"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}

resource "aws_eip" "nat" {
  vpc = true
}

resource "aws_nat_gateway" "default" {
  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = "${aws_subnet.public.id}"
  depends_on = ["aws_internet_gateway.default"]
}

resource "aws_subnet" "public" {
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "${var.public_subnet_cidr}"
  availability_zone = "${var.aws_availability_zone}"
  map_public_ip_on_launch = true
  tags {
    Name = "${var.app_name}-${var.environment}-public"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.default.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
  }
  tags {
    Name = "${var.app_name}-${var.environment}"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id = "${aws_subnet.public.id}"
  route_table_id = "${aws_route_table.public.id}"
}
